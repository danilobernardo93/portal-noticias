<?php

namespace App\Controllers;

use SimpleXMLElement;

class Pagina extends Inicio
{
	protected $limite;
	protected $postModel;
	protected $newsModel;
	protected $usuarioModel;
	protected $revistaModel;
	protected $enqueteModel;
	protected $conteudoModel;
	protected $categoriaModel;
	protected $comentarioModel;

	public function __construct()
	{
		$this->limite = 10;
		$this->sessao = session();
		$this->postModel = new \App\Models\PostModel();
		$this->newsModel = new \App\Models\NewsModel();
		$this->usuarioModel = new \App\Models\UserModel();
		$this->enqueteModel = new \App\Models\EnqueteModel();
		$this->revistaModel  = new \App\Models\RevistaModel();
		$this->conteudoModel = new \App\Models\ConteudoModel();
		$this->categoriaModel = new \App\Models\CategoriaModel();
		$this->comentarioModel = new \App\Models\ComentarioModel();
	}

	public function index()
	{
		// $destaque    =  $this->categoriaModel->getCategoriasDesstaque(['destaque_cat_id' => 1]);
		$destaque    = json_decode($this->categoriaModel->getCategoriasDesstaque(['destaque_id' => 1])->categorias);
		$data['seo'] = $this->seoBasicPage(null, ['', '', '', '']);

		$data['menu']  = $this->getCategorias();
		$data['links'] = $this->arquivos();

		$data['enquete'] = $this->enqueteModel->getEnquete(['enquete_status' => 1]);

		$data['maisLidas'] = $this->postModel->getPostMaisVisto(['post_ativo' => 1, 'post_rascunho' => 0], 5);
		$data['rescentes'] = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0], 5);
		$data['categoriaDestaque1']  = $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $destaque->categoria1], 5);
		$data['categoriaDestaque2'] = $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $destaque->categoria2], 5);
		$data['categoriaDestaque3'] = $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $destaque->categoria3], 5);
		$data['categoriaDestaque4'] = $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $destaque->categoria4], 5);
		$data['categoriaDestaque5'] = $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $destaque->categoria5], 5);
		$data['categoriaDestaque6'] = $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $destaque->categoria6], 5);
		$data['categoriaDestaque7'] = $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $destaque->categoria7], 5);
		$data['categoriaDestaque8'] = $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $destaque->categoria8], 5);

		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas('home');
		return view('portal/views/home', $data);
	}

	public function enquete()
	{
		if ($this->request->getPost('action') == 'formEnquete') {
			$alternativa = $this->request->getPost('alternativa');
			$enquete     = $this->enqueteModel->getAlternativaEnquete(['enquete_alternativa_id' => $alternativa]);
			$numVotos    = $enquete->enquete_alternativa_num;
			if ($this->enqueteModel->updateAlternativaEnquete(['enquete_alternativa_id' => $alternativa], ['enquete_alternativa_num' => ($numVotos + 1)])) {
				return true;
			}
		}
	}

	public function sobre()
	{

		$data['rescentes'] = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0, 'user_nivel_id' => 2], 3);
		$data['sobreRodape'] = json_decode($this->conteudoModel->find(5)->conteudo);
		$data['redesSociais'] = json_decode($this->conteudoModel->find(6)->conteudo);
		$data['sobre'] = json_decode($this->conteudoModel->find(1)->conteudo);
		$data['sobre2'] = json_decode($this->conteudoModel->find(2)->conteudo);
		$data['news'] = json_decode($this->conteudoModel->find(3)->conteudo);
		$data['imagens'] = $this->revistaModel->findAll();

		$data['menu']  = $this->getCategorias();
		$data['seo']   = $this->seoBasicPage(null, ['', '', '', '']);
		$data['links'] = $this->arquivos('sobre');

		$data['propagandas'] = $this->propagandas();
		return view('sobre', $data);
	}

	public function contato()
	{
		$data['rescentes'] = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0, 'user_nivel_id' => 2], 3);
		$data['sobreRodape'] = json_decode($this->conteudoModel->find(5)->conteudo);
		$data['redesSociais'] = json_decode($this->conteudoModel->find(6)->conteudo);
		$data['contato'] = json_decode($this->conteudoModel->find(4)->conteudo);

		$data['menu'] = $this->getCategorias();
		$data['title'] = 'Contato';
		$data['keywords'] = 'Lorena, Lorena r7, Lorena a fazenda, lorena contato';
		$data['ogIMG'] = '';
		$data['description'] = 'Entre em contato conosco.';
		$data['links'] = $this->arquivos('contato');


		$this->enviaEmail();

		return view('contato', $data);
	}

	public function postagem($uri)
	{
		if (!$post = $this->postModel->getPost($uri, ['post_rascunho' => 0])) {
			return redirect()->to(base_url());
		}

		$this->postModel->update($post->post_id, ['post_views' => ($post->post_views + 1)]);


		$data['post'] =  $post;
		$data['seo']  = $this->seoBasicPage(null, [$post->post_titulo, $post->post_resumo, $post->keywords, 'public/portal/img/postagens/' . $post->post_img]);

		$data['rescentes'] 	  = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0], 5);
		$data['maisLidas']    = $this->postModel->getPostMaisVisto(['post_ativo' => 1, 'post_rascunho' => 0], 5);
		$data['comentarios']  = $this->postModel->getComentarios($post->post_id);

		$data['menu']  = $this->getCategorias();
		$data['links'] = $this->arquivos();

		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas();

		if ($this->request->getPost('action') == 'Comentar') {
			$dados['comentario_nome'] = $this->request->getPost('nome');
			$dados['comentario_email'] = $this->request->getPost('email');
			$dados['comentario_assunto'] = $this->request->getPost('assunto');
			$dados['comentario_texto'] = $this->request->getPost('comentario');
			$dados['comentario_post_id'] = $post->post_id;

			if ($this->comentarioModel->insert($dados)) {
				$maisUm = $post->comentarios + 1;
				$this->postModel->update($post->post_id, ['comentarios' => $maisUm]);
				$data['msg'] = 'Comentário enviado com sucesso';
			}
		}

		return view('portal/views/postagem', $data);
	}

	public function postagemPreview($uri)
	{
		if (!isset($_SESSION['user']['logged'])) {
			return redirect()->to(base_url());
		}
		if (!$post = $this->postModel->getPost($uri, ['post_rascunho' => 1])) {
			return redirect()->to(base_url());
		}


		$data['sobreRodape'] = json_decode($this->conteudoModel->find(5)->conteudo);
		$data['redesSociais'] = json_decode($this->conteudoModel->find(6)->conteudo);

		$data['rescentes'] = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0, 'user_nivel_id' => 2], 3);
		$data['maisLidas'] = $this->postModel->getPostMaisVisto(['post_ativo' => 1, 'post_rascunho' => 0, 'user_nivel_id' => 2], 3);
		$data['post'] =  $post;

		$data['menu'] = $this->getCategorias();
		$data['title'] = $post->post_titulo;
		$data['keywords'] = $post->keywords;
		$data['ogIMG'] = base_url('public/portal/img/postagens/' . $post->post_img);
		$data['description'] = $post->post_resumo;
		$data['links'] = $this->arquivos('postagem');

		if ($this->request->getPost('action') == 'Comentar') {
			$dados['comentario_nome'] = $this->request->getPost('nome');
			$dados['comentario_email'] = $this->request->getPost('email');
			$dados['comentario_assunto'] = $this->request->getPost('assunto');
			$dados['comentario_texto'] = $this->request->getPost('comentario');
			$dados['comentario_post_id'] = $post->post_id;

			if ($this->comentarioModel->insert($dados)) {
				$maisUm = $post->comentarios + 1;
				$this->postModel->update($post->post_id, ['comentarios' => $maisUm]);
				$data['msg'] = 'Comentário enviado com sucesso';
			}
		}

		return view('postagem-preview', $data);
	}

	public function categoria($uri, $page = 1)
	{
		if (!$categoria = $this->categoriaModel->getCategoriaUri($uri)) {
			// return redirect()->to(base_url());
		}

		$inicio = ($page * $this->limite) - $this->limite;
		$posts 	= $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0,  'post_categoria' => $categoria->categoria_id], $this->limite, $inicio);
		$numRegistro = count($this->postModel->numRegistro(['post_ativo' => 1, 'post_rascunho' => 0,  'post_categoria' => $categoria->categoria_id]));
		$data['rescentes'] = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0], 5);
		$data['maisLidas'] = $this->postModel->getPostMaisVisto(['post_ativo' => 1, 'post_rascunho' => 0], 5);

		$data['menu'] = $this->getCategorias();

		$data['pageCurrent'] = $page;
		$data['numPages'] = ceil($numRegistro / $this->limite);
		$data['posts'] = $posts;
		$data['categoria'] = $categoria;
		$data['seo'] = $this->seoBasicPage();
		$data['links'] = $this->arquivos('home');

		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas();

		return view('portal/views/postagens', $data);
	}

	public function subCategoria($subcategoria, $page = 1)
	{
		if (!$subCategoria = $this->categoriaModel->getSubCategoriaUri($subcategoria)) {
			return redirect()->to(base_url());
		}

		$data['menu'] = $this->getCategorias();

		$inicio = ($page * $this->limite) - $this->limite;
		$posts 	= $this->postModel->getPostsCategoria(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $subCategoria->subcategoria_categoria_id], $this->limite, $inicio);
		$numRegistro = count($this->postModel->numRegistro(['post_ativo' => 1, 'post_rascunho' => 0, 'post_categoria' => $subCategoria->subcategoria_categoria_id]));
		$data['rescentes']    = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0], 5);
		$data['maisLidas']    = $this->postModel->getPostMaisVisto(['post_ativo' => 1, 'post_rascunho' => 0], 5);

		$data['pageCurrent']  = $page;
		$data['numPages']     = ceil($numRegistro / $this->limite);

		$data['posts']     	  = $posts;
		$data['subcategoria'] = $subCategoria;

		$data['seo']  = $this->seoBasicPage(null, ['', '', '', '']);
		$data['links'] 		  = $this->arquivos('home');

		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas();

		return view('portal/views/postagens', $data);
	}

	public function busca($busca, $page = 1)
	{

		$busca = str_replace("-", " ", $busca);
		$inicio = ($page * $this->limite) - $this->limite;
		$numRegistro = count($this->postModel->numRegistro(['post_ativo' => 1, 'post_rascunho' => 0, 'user_nivel_id' => 2], $busca));
		$posts = $this->postModel->getBuscaPosts($busca, $this->limite, $inicio);
		$data['busca'] = $busca;
		$data['rescentes'] = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0, 'user_nivel_id' => 2], 3);

		$data['menu'] = $this->getCategorias();
		$data['pageCurrent'] = $page;
		$data['numPages'] = ceil($numRegistro / $this->limite);
		$data['posts'] = $posts;
		$data['title'] = 'Resultado da pesquisa';
		$data['keywords'] = '';
		$data['ogIMG'] = '';
		$data['description'] = '';
		$data['links'] = $this->arquivos('home');

		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas();
		return view('busca', $data);
	}

	public function recentes($page = 1)
	{
		$inicio = ($page * $this->limite) - $this->limite;
		$numRegistro = count($this->postModel->numRegistro(['post_ativo' => 1, 'post_rascunho' => 0, 'user_nivel_id' => 2]));
		$posts = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0, 'user_nivel_id' => 2], $this->limite, $inicio);
		$data['rescentes'] = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0, 'user_nivel_id' => 2], 3);


		$data['menu'] = $this->getCategorias();
		$data['pageCurrent'] = $page;
		$data['numPages'] = ceil($numRegistro / $this->limite);
		$data['posts'] = $posts;
		$data['title'] = 'Fique por dentro das notícias mais rescentes';
		$data['keywords'] = 'Fique por dentro das notícias mais rescentes';
		$data['ogIMG'] = '';
		$data['description'] = 'Fique por dentro das notícias mais rescentes';
		$data['links'] = $this->arquivos('home');

		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas();

		return view('recentes', $data);
	}

	public function colunistas($page = 1)
	{
		$inicio = ($page * $this->limite) - $this->limite;
		$colunistas  = $this->usuarioModel->getUsuarios(['user_nivel_id' => 3], $this->limite, $inicio);
		$numRegistro = count($this->usuarioModel->getUsuarios(['user_nivel_id' => 3]));
		$data['numPages']     = ceil($numRegistro / $this->limite);
		$data['pageCurrent']  = $page;

		// $data['rescentes']    = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0], 5);
		// $data['maisLidas']    = $this->postModel->getPostMaisVisto(['post_ativo' => 1, 'post_rascunho' => 0], 5);

		$data['menu']        = $this->getCategorias();
		$data['colunistas']  = $colunistas;

		$data['seo']   = $this->seoBasicPage(null, ['', '', '', '']);
		$data['links'] = $this->arquivos('colunistas');

		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas();

		return view('portal/views/colunistas', $data);
	}

	public function colunista($uri, $page = 1)
	{
		if (!$colunista = $this->usuarioModel->getUsuarios(['user_uri' => $uri], 1, 0)) {
			return redirect()->to(base_url('colunistas'));
		}

		$inicio = ($page * $this->limite) - $this->limite;
		$numRegistro = count($this->postModel->numRegistro(['post_ativo' => 1, 'post_rascunho' => 0, 'post_user_id' => $colunista[0]->user_id]));

		$data['posts'] 		  = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0, 'post_user_id' => $colunista[0]->user_id], $this->limite, $inicio);
		$data['rescentes']    = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0, 'post_user_id' => $colunista[0]->user_id], 5);
		$data['maisLidas']    = $this->postModel->getPostMaisVisto(['post_ativo' => 1, 'post_rascunho' => 0, 'post_user_id' => $colunista[0]->user_id], 5);


		$data['seo'] 		 = $this->seoBasicPage(null, [$colunista[0]->user_nome, $colunista[0]->user_texto, $colunista[0]->user_nome, '']);
		$data['menu'] 		 = $data['menu']  = $this->getCategorias();
		$data['links'] 		 = $this->arquivos('colunistas');
		$data['numPages'] 	 = ceil($numRegistro / $this->limite);
		$data['colunista']   = $colunista[0];
		$data['pageCurrent'] = $page;

		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas();

		return view('portal/views/colunista', $data);
	}

	public function politicaDePrivacidade()
	{

		$data['seo'] 	= $this->seoBasicPage(null, ['Políticas de Privacidade | Vale Finanças', 'Políticas de Privacidade | Vale Finanças', 'Políticas de Privacidade,Vale Finanças']);
		$data['menu'] 	= $data['menu']  = $this->getCategorias();
		$data['links'] 	= $this->arquivos('colunistas');


		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas();

		return view('portal/views/politica-privacidade', $data);
	}
	public function termosDeUso()
	{

		$data['seo'] 	= $this->seoBasicPage(null, ['Termos de Uso | Vale Finanças', 'Termos de Uso | Vale Finanças', 'Termos de Uso,Vale Finanças']);
		$data['menu'] 	= $data['menu']  = $this->getCategorias();
		$data['links'] 	= $this->arquivos('colunistas');


		$data['dadosFixos']  = $this->dadosFixos();
		$data['propagandas'] = $this->propagandas();

		return view('portal/views/termos-de-uso', $data);
	}

	public function rss()
	{

		// setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');

		$postagens = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0], 10);

		header("content-type:application/rss+xml; charset=iso-8859-1");
		echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
		echo '<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/">' . PHP_EOL;
		echo '<channel>' . PHP_EOL;
		echo '<title>Feed Lorena Louisy</title>' . PHP_EOL;
		echo '<link>' . base_url() . '</link>' . PHP_EOL;
		echo '<description>Feed Lorena Louisy</description>' . PHP_EOL;

		foreach ($postagens as $post) {
			$dataCompleta = explode(" ", $post->post_updated_at);
			$data = explode("-", $dataCompleta[0]);
			$hora = explode(":", $dataCompleta[1]);
			echo '<item>' . PHP_EOL;
			echo '<title>' . $post->post_titulo . '</title>' . PHP_EOL;
			echo '<link>' . base_url('post/' . $post->post_uri) . '</link>' . PHP_EOL;
			echo '<guid>' . base_url('post/' . $post->post_uri) . '</guid>' . PHP_EOL;
			echo '<pubDate>' . strftime("%a, %d %b %Y %H:%M:%S", mktime($hora[0], $hora[1], $hora[2], $data[1], $data[2], $data[0])) . ' +0200</pubDate>' . PHP_EOL;
			echo '<dc:creator>' . $post->user_nome . '</dc:creator>' . PHP_EOL;
			echo '<dc:coverage><![CDATA[ ]]></dc:coverage>' . PHP_EOL;
			echo '<category>' . PHP_EOL;
			echo '<![CDATA[  ' . $post->categoria_nome . ' ]]>' . PHP_EOL;
			echo '</category>' . PHP_EOL;
			echo '<description>' . PHP_EOL;
			echo ($post->post_resumo) . PHP_EOL;
			echo '</description>' . PHP_EOL;
			echo '<content:encoded>' . PHP_EOL;
			echo '<![CDATA[<img src="' . base_url('public/portal/img/postagens/' . $post->post_img) . '" alt="' . str_replace('"', "", $post->post_titulo) . '"/>]]>' . PHP_EOL;
			echo '</content:encoded>' . PHP_EOL;
			echo '</item>' . PHP_EOL;
		}

		echo '</channel>' . PHP_EOL;
		echo '</rss>';
	}

	private function enviaEmail()
	{
		if ($this->request->getPost('action') == 'Enviar Mensagem') {
			$email = \Config\Services::email();

			$config['protocol'] = 'smtp';
			// $config['mailPath'] = '/usr/sbin/sendmail';
			$config['charset']  = 'utf-8'; #iso-8859-1
			$config['mailType'] = 'html';
			$config['wordWrap'] = true;
			$config['SMTPHost'] = 'mail.lorenalouisy.com';
			$config['SMTPUser'] = 'site@lorenalouisy.com';
			$config['SMTPPass'] = 'JP4UqRd5S#*3';
			$config['SMTPPort'] = 587;

			$email->initialize($config);



			$dados['nome'] = $this->request->getPost('nome');
			$dados['email'] = $this->request->getPost('email');
			$dados['assunto'] = $this->request->getPost('assunto');
			$dados['telefone'] = $this->request->getPost('telefone');
			$dados['mensagem'] = $this->request->getPost('mensagem');

			$teste = view('incl/layout-email/layout-1', $dados);

			$email->setFrom('site@lorenalouisy.com', 'Mensagem do site');
			$email->setTo($dados['email']);
			$email->setSubject($dados['assunto']);
			$email->setMessage($teste);

			if ($email->send()) {
				echo '<script>window.alert("E-mail enviado com sucesso");window.location=window.location;</script>';
			} else {
				echo '<script>window.alert("Erro ao enviar email, tente novamente mais tarde");window.location=window.location;</script>';
			}
		}
	}

	public function getCategorias()
	{
		$categorias    = $this->categoriaModel->findAll();
		$subCategorias = $this->categoriaModel->findSubcategorias();
		$categoriasID  = [];
		$menu = [];

		foreach ($categorias as $linha) {
			$menu[$linha->categoria_id] = ['itemMenu' => $linha, 'subMenu' => []];
			$categoriasID[$linha->categoria_id] = true;
		}

		foreach ($subCategorias as $linha) {
			if (in_array($linha->subcategoria_categoria_id, $categoriasID)) {
				$menu[$linha->subcategoria_categoria_id]['subMenu'][] = $linha;
			}
		}

		return $this->opcoesMenu($menu);
	}

	/**
	 * @param array $array[[],[]]
	 * @return array
	 */
	private function opcoesMenu($array)
	{
		$novoArray = ['topo' => [], 'rodape' => []];

		foreach ($array as $linha) {
			if ($linha['itemMenu']->menu == 1) {
				$novoArray['topo'][] = $linha;
			}

			if ($linha['itemMenu']->rodape == 1) {
				$novoArray['rodape'][] = $linha;
			}
		}
		return $novoArray;
	}

	public function salvaEmailNews()
	{
		if ($this->request->getMethod() == 'post') {
			if ($email = $this->request->getPost('email')) {
				if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
					return $this->newsModel->insert(['newsletter_email' => $email]) ? 'Email cadastrado com sucesso' : 'Erro ao adicionar email';
				} else {
					return 'Email inválido';
				}
			}
		}
	}

	/**
	 * @param null|string $pagina
	 * @return arry
	 */
	private function arquivos($pagina = 'default')
	{
		switch ($pagina) {
			case 'home':
				$arquivos['css'] = [];
				$arquivos['js']  = [];
				return $arquivos;
				break;

			case 'sobre':
				$arquivos['css'] = [];
				$arquivos['js']  = [];
				return $arquivos;
				break;

			case 'postagem':
				$arquivos['css'] = [];
				$arquivos['js']  = [];
				return $arquivos;
				break;

			case 'colunistas':
				$arquivos['css'] = [];
				$arquivos['js']  = [];
				return $arquivos;
				break;

			default:
				$arquivos['css'] = [];
				$arquivos['js']  = [];
				return $arquivos;
				break;
		}
	}

	/**
	 * @param null|string $page
	 * @param [title,description,keywords,img] $params
	 * @return array
	 */
	private function seoBasicPage($page = null, $params = [])
	{
		$padrao = [
			'Vale Finanças',
			'Um portal de notícias para você ficar atualizado nos assuntos mais importantes na area da economia',
			'Vale finanças, portal de noticias economia, blog economia',
			base_url('public/portal/img/vf-vermelho.png')
		];
		switch ($page) {
			default:
				return [
					'title' => !empty($params[0]) ? $params[0] : $padrao[0],
					'description' => !empty($params[1]) ? $params[1] : $padrao[1],
					'keywords' => !empty($params[2]) ? $params[2] : $padrao[2],
					'ogIMG' => base_url(!empty($params[3]) ? $params[3] : $padrao[3])
				];
				break;
		}
	}

	/**
	 * @return array
	 */
	private function dadosFixos()
	{
		$data['sobre'] = ''; //json_decode($this->conteudoModel->find(1)->conteudo);
		$data['news']  = ''; //json_decode($this->conteudoModel->find(3)->conteudo);
		$data['sobreRodape']  = ''; //json_decode($this->conteudoModel->find(5)->conteudo);
		$data['redesSociais'] = ''; //json_decode($this->conteudoModel->find(6)->conteudo);

		return $data;
	}

	/**
	 * @param null|string $page
	 * @return array
	 */
	private function propagandas($page = null)
	{
		$padrao = [
			'header01' => [
				'link' => '#',
				'title' => 'Anuncio header 01',
				'image' => base_url('public/portal/img/anuncio/anuncio-728x90.jpg'),
			],
			'sideBar01' => [
				'link' => '#',
				'title' => 'Anuncio sidebar',
				'image' => base_url('public/portal/img/anuncio/anuncio-300x250.jpg'),
			],
			'sideBar02' => [
				'link' => '#',
				'title' => 'Anuncio sidebar',
				'image' => base_url('public/portal/img/anuncio/anuncio-300x250.jpg'),
			]
		];

		switch ($page) {
			case 'home':
				$adicionais = [
					'body01' => [
						'link' => '#',
						'title' => 'Anuncio body 01',
						'image' => base_url('public/portal/img/anuncio/anuncio-970x90.jpg'),
					],
					'body02' => [
						'link' => '#',
						'title' => 'Anuncio body 02',
						'image' => base_url('public/portal/img/anuncio/anuncio-728x90.jpg'),
					]
				];

				return array_merge($padrao, $adicionais);
				break;

			default:
				return $padrao;
				break;
		}
	}


	public function lerFeed($link = 'https://www.spacemoney.com.br/ultimas-noticias/feed/')
	{

		$url = $link;
		$feeds = file_get_contents($url);
		$rss = simplexml_load_string($feeds);

		$i = 1;

		foreach ($rss->channel->item as $entry) {
			if ($i <= 10) {
				$keyWords = explode(" ", str_replace([",", ":", "+", "-"], "", $entry->title));
				$data['post_titulo']   	 = $entry->title;
				$data['keywords']      	 = implode(",", $keyWords);
				$data['post_uri']      	 = mb_url_title($entry->title);
				// echo ($entry->description);die();
				$data['post_texto']    	 = $this->trocaTexto(strip_tags($entry->description));
				$data['post_resumo']   	 = $entry->description;
				$data['post_ativo']    	 = 1;
				$data['post_rascunho']   = 0;
				$data['post_updated_at'] = date("Y-m-d H:i:s");
				$data['post_user_id'] 	 = 1;

				if ($categoria = $this->categoriaModel->getCategoriaUri(mb_url_title($entry->category))) {
					$data['post_categoria'] = $categoria->categoria_id;
				} else {
					$data['post_categoria'] = $this->categoriaModel->insert(
						[
							"categoria_nome" => $entry->category,
							"categoria_uri" => mb_url_title($entry->category),
							"menu" => 1
						]
					);
				}

				try {
					$this->postModel->insert($data);
				} catch (\Throwable $th) {
					//throw $th;
				}
			}
			else{
				exit;
			}
			$i++;
		}
	}

	public function trocaTexto($texto)
	{
		$curl = curl_init();
		$dados = json_encode([
			'language' => 'pt',
			'strength' => '3',
			'text' => $texto
		]);

		// var_dump($dados);die();

		curl_setopt_array($curl, [
			CURLOPT_URL => "https://rewriter-paraphraser-text-changer-multi-language.p.rapidapi.com/rewrite",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $dados,
			CURLOPT_HTTPHEADER => [
				"content-type: application/json",
				"x-rapidapi-host: rewriter-paraphraser-text-changer-multi-language.p.rapidapi.com",
				"x-rapidapi-key: acc5d89a14msha788ed14136f1f0p1c1fdajsnf2a8a28c76f0"
			],
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return false;
		} else {
			$result = json_decode($response);
			return isset($result->rewrite) ? $result->rewrite : 'texto muito grande  - fazer manual';
		}
	}
}
