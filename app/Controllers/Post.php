<?php

namespace App\Controllers;

class Post extends Inicio
{
    protected $postModel;
    protected $conteudoModel;

    public function __construct()
    {
        $this->postModel = new \App\Models\PostModel();
        $this->categoriaModel = new \App\Models\CategoriaModel();
        $this->conteudoModel = new \App\Models\ConteudoModel();
        $this->sessao = session();
    }

    public function index($page = 1)
    {
        nivelAcessoLogin(getNivelAcesso(), ['1', '2', '3']);
        $arrayOpcoes = ['post_user_id' => $_SESSION['user']['user_id']];
        if (getNivelAcesso() == 1) {
            $arrayOpcoes = [];
        }
        $limite      = 20;
        $numRegistro = count($this->postModel->getPosts($arrayOpcoes));
        $numPages    = ceil($numRegistro / $limite);

        $data['title']       = 'Lista de postagem';
        $data['description'] = 'Lista de postagem';
        $data['links']       = $this->arquivos('novo');
        $data['pageCurrent'] = $page;
        $data['numPages']    = $numPages;

        $inicio            = ($page * $limite) - $limite;
        $data['postagens'] = $this->postModel->getPosts($arrayOpcoes, $limite, $inicio);
        $data['paginas']   = $this->conteudoModel->getConteudos([], 'pagina');

        return view('painel/postagens', $data);
    }

    public function rascunho($page = 1)
    {
        nivelAcessoLogin(getNivelAcesso(), ['1', '2', '3']);

        $arrayOpcoes = ['post_rascunho' => 1, 'post_user_id' => $_SESSION['user']['user_id']];
        if (getNivelAcesso() == 1) {
            $arrayOpcoes = ['post_rascunho' => 1];
        }

        $data['title']       = 'Lista de postagem em rascunho';
        $data['description'] = 'Lista de postagem em rascunho';
        $data['links']       = $this->arquivos('novo');
        $data['pageCurrent'] = $page;
        $data['postagens']   = $this->postModel->getPosts($arrayOpcoes, 50);
        $data['paginas']     = $this->conteudoModel->getConteudos([], 'pagina');

        return view('painel/postagens-rascunho', $data);
    }

    public function novo()
    {
        nivelAcessoLogin(getNivelAcesso(), ['1', '2', '3']);
        $data['paginas']     = $this->conteudoModel->getConteudos([], 'pagina');
        $data['title']       = 'Adicionar postagem';
        $data['description'] = 'Adicionar postagem';
        $data['links']       = $this->arquivos('novo');
        $data['categorias']  = $this->categoriaModel->findAll();


        $data['msg'] = $this->verificaAction();

        return view('painel/postagem-adicionar', $data);
    }

    public function editar($id)
    {
        nivelAcessoLogin(getNivelAcesso(), ['1', '2', '3']);
        $arrayOpcoes = ['post_id' => $id, 'post_user_id' => $_SESSION['user']['user_id']];
        if (getNivelAcesso() == 1) {
            $arrayOpcoes = ['post_id' => $id];
        }

        if (!$post = $this->postModel->postGetWhere($arrayOpcoes, 1)) {
            return redirect()->to(base_url('painel/post'));
        }

        $data['paginas'] = $this->conteudoModel->getConteudos([], 'pagina');
        $data['title'] = 'Editar postagem';
        $data['description'] = 'Editar postagem';
        $data['links'] = $this->arquivos('novo');
        $data['categorias'] = $this->categoriaModel->findAll();
        $data['post'] = $post[0];

        $data['msg'] = $this->verificaAction($id);

        return view('painel/postagem-editar', $data);
    }

    public function deletar()
    {
        if ($this->request->getMethod() == 'post') {
            return $this->postModel->delete($this->request->getPost('id'));
        }
    }

    private function salvar($acao, $id = null)
    {
        $data['post_titulo']    = $this->request->getPost('post_titulo');
        $data['keywords']       = $this->request->getPost('keywords');
        $data['post_uri']       = mb_url_title($data['post_titulo']);
        $data['post_texto']     = $this->request->getPost('conteudo');
        $data['post_resumo']     = $this->request->getPost('resumo');
        $data['post_ativo']     = $this->request->getPost('post_ativo') ? 1 : 0;
        $data['post_rascunho']     = $this->request->getPost('post_rascunho') ? 1 : 0;
        $data['post_updated_at'] = date("Y-m-d H:i:s");

        $data['post_categoria'] = $this->request->getPost('categoria');

        switch ($acao) {
            case 'insert':
                $data['post_user_id']   = $_SESSION['user']['user_id'];
                if ($ID = $this->postModel->insert($data)) {
                    $this->createSiteMap();
                    if ($this->uploadIMG($ID)) {
                        header("Location:" . base_url('painel/post/' . $ID));
                        die();
                    }
                    return 'Erro ao inserir imagem';
                } else {
                    var_dump($this->postModel->errors());
                }
                break;

            case 'update':
                if ($this->postModel->update($id, $data)) {
                    $this->createSiteMap();
                    if ($this->uploadIMG($id)) {
                        header("Location:" . base_url('painel/post/' . $id));
                        die();
                    }
                }
                return 'Erro ao atualizar';

                break;
        }
    }

    public function createSiteMap()
    {
        $helpers = array('funcoes', 'xml', 'text');
        $dateCurrent = explode(" ", date("Y-m-d H:m:i"));

        $postagens = $this->postModel->getPostsResncentes(['post_ativo' => 1, 'post_rascunho' => 0], 100);
        // Receberá todos os dados do XML
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
        // A raiz do meu documento XML
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . PHP_EOL;

        $xml .= '<url><loc>' . base_url('/') . '</loc><lastmod>' . $dateCurrent[0] . 'T' . $dateCurrent[1] . '+00:00</lastmod><priority>1.00</priority></url>';
        foreach ($postagens as $post) {
            $dataCompleta = explode(" ", $post->post_updated_at);
            $data = explode("-", $dataCompleta[0]);
            $hora = explode(":", $dataCompleta[1]);
            $xml .= '<url>' . PHP_EOL;
            $xml .= '<loc>' . base_url('post/' . $post->post_uri) . '</loc>' . PHP_EOL;
            $xml .= '<lastmod>' . $data[0] . '-' . $data[1] . '-' . $data[2] . 'T' . $hora[0] . ':' . $hora[1] . ':' . $hora[2] . '+00:00</lastmod>' . PHP_EOL;
            $xml .= '<priority>0.80</priority>' . PHP_EOL;
            $xml .= '</url>' . PHP_EOL;
        }

        $xml .= '</urlset>';

        // Escreve o arquivo
        $fp = fopen('sitemap.xml', 'w+');
        fwrite($fp, $xml);
        fclose($fp);
        echo $xml;
    }

    private function uploadIMG($id)
    {
        if ($file = $this->request->getFile('imagem')) {


            $dir          = './public/portal/img/postagens';
            $ext          = $file->getExtension();
            $imgName      =  'post_' . $id . '.' . $ext;
            $extPermitida = ['png', 'jpg', 'jpeg', 'webp'];
            $caminho = 'public/portal/img/postagens/' . $imgName;

            if (in_array($file->getExtension(), $extPermitida)) {
                $image = \Config\Services::image();
                $image = $image->withFile($file);
                $imagemCrop = $image->crop($_POST['input_w'], $_POST['input_h'], $_POST['input_x1'], $_POST['input_y1']);

                if ($imagemCrop->save($caminho)) {
                    $img = 'public/portal/img/postagens/' . $imgName;
                    $thumb = 'public/portal/img/postagens/thumb-' . $imgName;
                    // echo $img;die();
                    $image = \Config\Services::image()
                        ->withFile($img)
                        ->fit(280, 162, 'center')
                        ->save($thumb);
                    return $this->postModel->update($id, ['post_img' => $imgName]);


                    // $img = 'public/portal/img/postagens/' . $imgName;
                    // $thumb = 'public/portal/img/postagens/thumb-' . $imgName;
                    // // echo $img;die();
                    // $image = \Config\Services::image()
                    //     ->withFile($img)
                    //     ->crop($_POST['input_w'], $_POST['input_h'], $_POST['input_x1'], $_POST['input_y1'])
                    //     ->save($thumb);
                    // return $this->postModel->update($id, ['post_img' => $imgName]);

                }
            }

            return true;
        }
    }

    private function verificaAction($id = null)
    {
        if ($action = $this->request->getPost('action')) {
            switch ($action) {
                case 'Adicionar postagem':
                    return $this->salvar('insert');
                    break;

                case 'Editar postagem':
                    return $this->salvar('update', $id);
                    break;

                default:
                    # code...
                    break;
            }
        }
    }

    private function arquivos($pagina)
    {
        switch ($pagina) {
            case 'novo':
                $arquivos['css'] = ['public/painel/css/post.css'];
                $arquivos['js']  = ['public/painel/js/post.js'];
                $arquivos['jsOut'] = ['https://cdn.tiny.cloud/1/aceqk9g9djcidvs2u21pkdnz2ucovo4jc42evwnbk3loom8g/tinymce/5/tinymce.min.js'];
                return $arquivos;
                break;

            default:
                $arquivos['css'] = [];
                $arquivos['js']  = [];
                $arquivos['jsOut'] = [];
                return $arquivos;
                break;
        }
    }
}
