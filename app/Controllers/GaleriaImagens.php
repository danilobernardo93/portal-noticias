<?php

namespace App\Controllers;

use App\Models\ConteudoModel;

class GaleriaImagens extends Inicio
{

    protected $conteudoModel;
    protected $galeriaImagensModel;

    public function __construct()
    {
        $this->sessao = session();
        $this->conteudoModel  = new \App\Models\ConteudoModel();
        $this->galeriaImagensModel  = new \App\Models\GaleriaImagensModel();
    }

    public function index($page = 1)
    {
        nivelAcessoLogin(getNivelAcesso(), [1, 2, 3], base_url('painel'));
        $data['links'] = $this->arquivos('galeria-imagens');
        $data['title'] = 'Galeria de imagens';
        $data['description'] = 'Galeria de imagens';
        $data['paginas'] = $this->conteudoModel->getConteudos([], 'pagina');
        $data['paginas'] = $this->conteudoModel->getConteudos([], 'pagina');
        $data['msg']     = [];



        $limite = 8;
        $numRegistro = count($this->galeriaImagensModel->getImagens());
        $data['pageCurrent'] = $page;
        $data['numPages'] = ceil($numRegistro / $limite);
        $inicio = ($page * $limite) - $limite;
        
        $data['imagens'] = $this->galeriaImagensModel->getImagens($limite, $inicio);
        



        $this->verificaAction();

        return view('painel/galeria-add-imagens', $data);
    }

    private function salvar()
    {
        $extPermitida = ['png', 'jpg', 'jpeg', 'webp'];
        $dir =  './public/portal/img/galeria-imagens';
        $erro = [];
        if($imgs = $this->request->getFileMultiple('imagens'))
        {
            foreach($imgs as $img )
            {
                $ext = $img->getExtension();
                $img_title = $this->request->getPost('titulo');
                $img_uri = url_title($img_title);
                $novoNome = $img_uri.'.'.$ext;
                if(in_array($ext, $extPermitida))
                {
                    if($img->move($dir, $novoNome,true))
                    {
                        $this->galeriaImagensModel->insert(
                            [
                                'imagem_nome'=> $novoNome,
                                'imagem_title'=> $img_title,
                                'imagem_uri' => $img_uri
                                ]);   
                    }
                    else
                    {
                        $erro[] = $img->erro;
                    }
                }
            }

            echo '<script>window.alert("Imagens salvas com sucesso");window.location=window.location</script>';die();
        }
    }

    public function deletar()
    {
        if ($this->request->getMethod() == 'post') {
            $dir =  './public/portal/img/galeria-imagens';
            unlink($dir . '/' . $this->request->getPost('img'));
            return $this->galeriaImagensModel->delete($this->request->getPost('id'));
        }
    }

    private function verificaAction()
    {
        if ($action = $this->request->getPost('action')) {
            switch ($action) {
                case 'Adicionar imagens':
                    return $this->salvar();
                    break;
            }
        }
    }

    private function arquivos($pagina)
    {
        switch ($pagina) {
            default:
                $arquivos['css'] = ['public/painel/css/galeria-imagens.css'];
                $arquivos['js']  = ['public/painel/js/galeria-imagens.js'];
                $arquivos['jsOut']  = [];
                return $arquivos;
                break;
        }
    }
}
