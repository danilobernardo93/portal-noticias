<?php namespace App\Controllers;

use App\Models\ConteudoModel;

class Revista extends Inicio
{

    protected $conteudoModel;
    protected $revistaModel;

    public function __construct()
    {
        $this->sessao = session();
        $this->conteudoModel  = new \App\Models\ConteudoModel();
        $this->revistaModel  = new \App\Models\RevistaModel();
    }

    public function index()
    {
        nivelAcessoLogin(getNivelAcesso(),[1],base_url('painel'));
        $data['links'] = $this->arquivos('revista');
        $data['title'] = 'Galeria de imagens (capa de revista)';
        $data['description'] = 'Galeria de imagens (capa de revista)';
        $data['paginas'] = $this->conteudoModel->getConteudos([],'pagina');
        $data['imagens'] = $this->revistaModel->findAll();
        $data['msg'] = [];

        $this->verificaAction();

        return view('painel/revista', $data);
    }

    private function salvar()
    {
        $extPermitida = ['png', 'jpg', 'jpeg','webp'];
        $dir =  './public/portal/img/capa-revista';
        $erro = [];
        if($imgs = $this->request->getFileMultiple('imagens'))
        {
            foreach($imgs as $img )
            {
                $ext = $img->getExtension();
                $novoNome = sha1(date("d-m-Y H:i:s").rand(0, 100)).'.'.$ext;
                if(in_array($ext, $extPermitida))
                {
                    if($img->move($dir, $novoNome,true))
                    {
                        $this->revistaModel->insert(['revista_img'=>$novoNome]);   
                    }
                    else
                    {
                        $erro[] = $img->erro;
                    }
                }
            }

            echo '<script>window.alert("Imagens salvas com sucesso");window.location=window.location</script>';die();
        }
    }

    public function deletar()
    {
        if($this->request->getMethod() == 'post')
        {
            $dir =  './public/portal/img/capa-revista';
            unlink($dir.'/'.$this->request->getPost('img'));
            return $this->revistaModel->delete($this->request->getPost('id'));
        }
    }

    private function verificaAction()
    {
        if ($action = $this->request->getPost('action')) {
            switch ($action) {
                case 'Adicionar imagens':
                    return $this->salvar();
                break;
            }
        }
    }

    private function arquivos($pagina)
	{
		switch ($pagina) {
			default:
            $arquivos['css'] = ['public/painel/css/revista.css'];
            $arquivos['js']  = ['public/painel/js/revista.js'];
                $arquivos['jsOut']  = [];
				return $arquivos;
				break;
		}
	}


}