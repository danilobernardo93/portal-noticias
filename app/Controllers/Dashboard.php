<?php namespace App\Controllers;

class Dashboard extends Inicio
{
	protected $comentarioModel;
	protected $conteudoModel;
	protected $postModel;


	public function __construct()
    {
		$this->postModel = new \App\Models\PostModel();
		$this->comentarioModel = new \App\Models\ComentarioModel();
		$this->conteudoModel = new \App\Models\ConteudoModel();
        $this->sessao = session();
	}

	public function index()
	{	
		nivelAcessoLogin(getNivelAcesso(),['1','2','3']);
        $data['title'] = 'Dashboard';
        $data['description'] = 'Dashboard';
        $data['links'] = $this->arquivos('');

		$data['numComentarios'] = $this->comentarioModel->numComentarios();
		$data['numPosts'] = count($this->postModel->numRegistro(['post_user_id'=>$_SESSION['user']['user_id']]));
		$data['paginas'] = $this->conteudoModel->getConteudos([],'pagina');
		return view('painel/dashboard', $data);
    }

	private function arquivos($pagina)
	{
		switch ($pagina) {
			default:
				$arquivos['css'] = [];
                $arquivos['js']  = [];
                $arquivos['jsOut'] = [];
				return $arquivos;
			break;
		}
	}
	
	//--------------------------------------------------------------------

}
