<?php namespace App\Controllers;

class Login extends Inicio
{
	protected $loginModel;

	public function __construct()
	{
		$this->loginModel = new \App\Models\LoginModel();
		$this->sessao = session();  
	}

	public function index()
	{		
        $data['title'] = 'Tela de login';
        $data['description'] = 'Tela de login';
        $data['ocultaMenu'] = 'Responsavel de tirar o menu';
		$data['links'] = $this->arquivos('');
		$data['msg'] = '';
		
		if($this->request->getMethod() == 'post')
		{
			$dados['user_email'] = $this->request->getPost('email');
			$dados['user_senha'] = sha1($this->request->getPost('senha'));
			$dados['user_ativo'] = 1;
			
			if($user = $this->loginModel->login($dados))
			{
				$_SESSION['user']['logged'] = true; 
				$_SESSION['user']['nivel_acesso'] = $user->user_nivel_id;
				$_SESSION['user']['user_id'] = $user->user_id;
				$_SESSION['user']['user_nome'] = $user->user_nome;

				return redirect()->to(base_url('painel'));
			}

			$data['msg'] = 'Dados incorretos';

		}

		return view('painel/login', $data);
    }

    public function logout()
	{		
		session_destroy();
		return redirect()->to(base_url('login'));
    }

	private function arquivos($pagina)
	{
		switch ($pagina) {
			default:
				$arquivos['css'] = [];
                $arquivos['js']  = [];
                $arquivos['jsOut'] = [];
				return $arquivos;
			break;
		}
	}
	
	//--------------------------------------------------------------------

}
