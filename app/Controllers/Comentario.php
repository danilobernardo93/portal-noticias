<?php namespace App\Controllers;

class Comentario extends Inicio
{
	protected $comentarioModel;
	protected $conteudoModel;

    public function __construct()
    {
        $this->comentarioModel = new \App\Models\ComentarioModel();
		$this->categoriaModel = new \App\Models\CategoriaModel();
		$this->conteudoModel = new \App\Models\ConteudoModel();
        $this->sessao = session();
	}
	
	public function index($page=1)
	{	
		nivelAcessoLogin(getNivelAcesso(),[1],base_url('painel'));
        $limite = 10;
        $numRegistro = $this->comentarioModel->countAllResults();
		$numPages    = ceil($numRegistro/$limite);
		$data['pageCurrent'] = $page;
        $data['numPages'] = $numPages;
		$inicio = ($page * $limite)-$limite;
		
        $data['title'] = 'Lista de comentários';
        $data['description'] = 'Lista de comentários';
		$data['links'] = $this->arquivos('');		
		$data['comentarios'] = $this->comentarioModel->getComentarios([],$limite,$inicio);
		$data['paginas'] = $this->conteudoModel->getConteudos([],'pagina');

		return view('painel/comentarios', $data);
    }

    public function editar($id)
	{		
		nivelAcessoLogin(getNivelAcesso(),[1],base_url('painel'));
        $data['title'] = 'Editar comentario';
        $data['description'] = 'Editar comentario';
		$data['links'] = $this->arquivos('');
		$data['paginas'] = $this->conteudoModel->getConteudos([],'pagina');
		
		if(!$data['comentario'] = $this->comentarioModel->find($id))
		{
			return redirect()->to(base_url('painel/comentario'));
		}

		$this->verificaAction($id);

		return view('painel/comentario-editar', $data);
	}

	private function editarComentario($id)
	{
		$data['comentario_nome'] = $this->request->getPost('comentario_nome');
		$data['comentario_email'] = $this->request->getPost('comentario_email');
		$data['comentario_assunto'] = $this->request->getPost('comentario_assunto');
		$data['comentario_texto'] = $this->request->getPost('comentario_texto');
		$data['comentario_ativo'] = $this->request->getPost('comentario_ativo') ? 1:0;

		if($this->comentarioModel->update($id, $data))
		{
			header("Location:".base_url('painel/comentario/'.$id));die();
		}
	}
	
	private function verificaAction($id=null)
    {
        if ($action = $this->request->getPost('action')) {
            switch ($action) {
                case 'Editar comentário':
                    return $this->editarComentario($id);
                break;
            }
        }
    }

	private function arquivos($pagina)
	{
		switch ($pagina) {
			default:
				$arquivos['css'] = ['public/painel/css/comentarios.css'];
                $arquivos['js']  = [];
                $arquivos['jsOut'] = [];
				return $arquivos;
			break;
		}
	}
	
	//--------------------------------------------------------------------

}
