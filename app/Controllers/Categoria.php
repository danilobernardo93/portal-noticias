<?php

namespace App\Controllers;

class Categoria extends Inicio
{
	protected $categoriaModel;
	protected $conteudoModel;

	function __construct()
	{
		$this->categoriaModel = new \App\Models\CategoriaModel();
		$this->conteudoModel = new \App\Models\ConteudoModel();
		$this->sessao = session();
	}

	public function index()
	{
		nivelAcessoLogin(getNivelAcesso(), [1], base_url('painel'));

		$data['title'] = 'Categorias';
		$data['description'] = 'Categorias';
		$data['links'] = $this->arquivos('');
		$data['categorias'] = $this->categoriaModel->findAll();
		$data['paginas'] = $this->conteudoModel->getConteudos([], 'pagina');

		if ($this->verificaAction()) {
			return redirect()->to(base_url('painel/categoria'));
		}

		return view('painel/categorias', $data);
	}

	public function destaqueHome()
	{
		nivelAcessoLogin(getNivelAcesso(), [1], base_url('painel'));

		$data['title'] = 'Alterar o destaque da página inicial';
		$data['description'] = 'Alterar o destaque da página inicial';
		$data['links'] = $this->arquivos('');
		$data['categoriasDestaque'] = json_decode($this->categoriaModel->getCategoriasDesstaque(['destaque_id' => 1])->categorias);
		$data['categorias'] = $this->categoriaModel->findAll();
		$data['paginas'] = $this->conteudoModel->getConteudos([], 'pagina');

		if ($this->request->getPost('action') == 'Atualizar Destaques') {
			$pagina = "Página Inicial";
			$categorias['categoria1'] = $this->request->getPost('categoria1');
			$categorias['categoria2'] = $this->request->getPost('categoria2');
			$categorias['categoria3'] = $this->request->getPost('categoria3');
			$categorias['categoria4'] = $this->request->getPost('categoria4');
			$categorias['categoria5'] = $this->request->getPost('categoria5');
			$categorias['categoria6'] = $this->request->getPost('categoria6');
			$categorias['categoria7'] = $this->request->getPost('categoria7');
			$categorias['categoria8'] = $this->request->getPost('categoria8');
			$data = 
			[
				'pagina' => $pagina,
				'categorias' => json_encode($categorias)
			];

			$this->categoriaModel->updateCategoriasDesstaque(1,$data);
			return redirect()->to(base_url('painel/categoria/destaque-home'));
		}

		return view('painel/categorias-destaque-home', $data);
	}

	protected function salvar()
	{
		$dados['categoria_nome'] = $this->request->getPost('categoria_nome');
		$dados['categoria_uri'] = mb_url_title($this->request->getPost('categoria_nome'));

		return $this->categoriaModel->insert($dados);
	}

	protected function atualizar()
	{
		$id = $this->request->getPost('categoria_id');
		$dados['menu'] = $this->request->getPost('menu');
		$dados['rodape'] = $this->request->getPost('rodape');
		$dados['categoria_uri'] = $this->request->getPost('categoria_uri');
		$dados['categoria_nome'] = $this->request->getPost('categoria_nome');

		return $this->categoriaModel->update($id, $dados);
	}

	public function deletar()
	{
		if ($this->request->getMethod() == 'post') {
			return $this->categoriaModel->delete($this->request->getPost('id'));
		}
	}

	private function verificaAction($id = null)
	{
		if ($action = $this->request->getPost('action')) {
			switch ($action) {
				case 'Salvar':
					return $this->salvar();
					break;

				case 'Atualizar':
					return $this->atualizar();
					break;

				case 'Deletar':
					return $this->deletar();
					break;
			}
		}
	}

	private function arquivos($pagina)
	{
		switch ($pagina) {
			default:
				$arquivos['css'] = ['public/painel/css/categoria.css'];
				$arquivos['js']  = ['public/painel/js/categoria.js'];
				$arquivos['jsOut'] = [];
				return $arquivos;
				break;
		}
	}
}
