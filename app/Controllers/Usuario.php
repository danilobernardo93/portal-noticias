<?php

namespace App\Controllers;

class Usuario extends Inicio
{
	protected $userModel;
	protected $conteudoModel;

	public function __construct()
	{
		$this->userModel = new \App\Models\UserModel();
		$this->conteudoModel = new \App\Models\ConteudoModel();
		$this->sessao = session();
	}

	public function index()
	{
		nivelAcessoLogin(getNivelAcesso(), [1], base_url('painel'));
		$data['paginas'] 	 = $this->conteudoModel->getConteudos([], 'pagina');
		$data['title'] 		 = 'Lista de usuários';
		$data['description'] = 'Lista de usuários';
		$data['users'] 		 = $this->userModel->getUsuarios();
		$data['links'] 		 = $this->arquivos('');

		return view('painel/usuarios', $data);
	}

	public function novo()
	{
		nivelAcessoLogin(getNivelAcesso(), [1], base_url('painel'));
		$data['paginas'] 	  = $this->conteudoModel->getConteudos([], 'pagina');
		$data['title'] 		  = 'Adicionar novo usuário';
		$data['description']  = 'Adicionar novo usuário';
		$data['links'] 		  = $this->arquivos('');
		$data['niveisAcesso'] = $this->userModel->getNiveisAcesso();
		$data['msg'] 		  = $this->verificaAction();


		return view('painel/usuario-novo', $data);
	}

	public function editar($id = 0)
	{
		nivelAcessoLogin(getNivelAcesso(), [1], base_url('painel'));
		if (!$data['user'] = $this->userModel->find($id)) {
			return redirect()->to(base_url('painel/usuario'));
		}
		$data['paginas']      = $this->conteudoModel->getConteudos([], 'pagina');
		$data['title'] 	   	  = 'Editar usuário';
		$data['description']  = 'Editar usuário';
		$data['links'] 		  = $this->arquivos('');
		$data['niveisAcesso'] = $this->userModel->getNiveisAcesso();
		$data['msg'] 		  = '';

		$this->verificaAction($id);

		return view('painel/usuario-editar', $data);
	}

	public function perfil()
	{
		nivelAcessoLogin(getNivelAcesso(), [1, 2, 3], base_url('painel'));
		if (!$data['user'] = $this->userModel->find($_SESSION['user']['user_id'])) {
			return redirect()->to(base_url('painel/usuario'));
		}
		$data['paginas'] = $this->conteudoModel->getConteudos([], 'pagina');

		$data['title'] = 'Editar perfil';
		$data['description'] = 'Editar perfil';
		$data['links'] = $this->arquivos('');
		$data['msg'] = '';

		if ($this->request->getPost('action') == 'Editar perfil') {

			if (!empty($_FILES['user_foto']['name'])) {
				if ($file = $this->request->getFile('user_foto')) {
					$dir          = './public/portal/img/usuarios';
					$ext          = $file->getExtension();
					$imgName      =  'usuario_' . $_SESSION['user']['user_id'] . '.' . $ext;

					if ($file->move($dir, $imgName, true)) {
						$dados['user_foto'] = $imgName;
					}
				}
			}

			$email = trim($this->request->getPost('user_email'));
			if ($senha = $this->request->getPost('user_senha')) {
				$dados['user_senha'] = sha1($senha);
			}
			if ($data['user']->user_email != $email) {
				$dados['user_email'] = $email;
			}

			$dados['user_nome'] = trim($this->request->getPost('user_nome'));
			$dados['user_texto'] = trim($this->request->getPost('user_texto'));
			$dados['user_coluna'] = trim($this->request->getPost('user_coluna'));
			if ($this->userModel->update($_SESSION['user']['user_id'], $dados)) {
				return redirect()->to(base_url('painel/perfil'));
			} else {
				$data['msg'] = 'Email existente, tente outro';
			}
		}

		return view('painel/perfil', $data);
	}

	private function salvar($acao, $id = null)
	{

		if ($senha = $this->request->getPost('user_senha')) {
			$dados['user_senha'] = sha1($senha);
		}

		$dados['user_nome'] = $this->request->getPost('user_nome');
		$dados['user_uri'] = mb_url_title($this->request->getPost('user_nome'));
		$dados['user_email'] = $this->request->getPost('user_email');
		$dados['user_texto'] = trim($this->request->getPost('user_texto'));
		$dados['user_ativo'] = $this->request->getPost('user_ativo') ? 1 : 0;
		$dados['user_coluna'] = $this->request->getPost('user_coluna');
		$dados['user_nivel_id'] = $this->request->getPost('user_nivel') ? $this->request->getPost('user_nivel') : getNivelAcesso();

	
		switch ($acao) {
			case 'insert':
				if ($ID = $this->userModel->insert($dados)) {
					header("Location:" . base_url('painel/usuario/' . $ID));
					die();
				}
				return 'Erro ao adicionar usuário';
				break;

			case 'update':
				if ($this->userModel->update($id, $dados)) {
					header("Location:" . base_url('painel/usuario/' . $id));
					die();
				}
				return 'Erro ao atualizar';

				break;
		}
	}

	public function deletar()
	{
		if ($this->request->getMethod() == 'post') {
			return $this->userModel->delete($this->request->getPost('id'));
		}
	}

	private function verificaAction($id = null)
	{
		if ($action = $this->request->getPost('action')) {
			switch ($action) {
				case 'Adicionar usuário':
					return $this->salvar('insert');
					break;

				case 'Editar perfil':
					return $this->salvar('update', $id);
					break;

				case 'Deletar':
					return $this->deletar();
					break;
			}
		}
	}

	private function arquivos($pagina)
	{
		switch ($pagina) {
			default:
				$arquivos['css']   = ['public/painel/css/usuarios.css'];
				$arquivos['js']    = ['public/painel/js/usuario.js'];
				$arquivos['jsOut'] = [];
				return $arquivos;
				break;
		}
	}
}
