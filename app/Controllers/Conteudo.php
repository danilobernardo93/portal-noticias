<?php

namespace App\Controllers;

class Conteudo extends Inicio
{
    protected $conteudoModel;

    public function __construct()
    {
        $this->sessao = session();
        $this->conteudoModel = new \App\Models\ConteudoModel();
    }

    public function index($pagina)
    {
        nivelAcessoLogin(getNivelAcesso(), [1], base_url('painel'));
        if (!$itens = $this->conteudoModel->getConteudos(['pagina' => $pagina])) {
            return redirect()->to(base_url('painel'));
        }
        $data['title'] = 'Gerenciar página ' . $pagina;
        $data['links'] = $this->arquivos('');
        $data['description'] = '';
        $data['pagina'] = $pagina;
        $data['paginas'] = $this->conteudoModel->getConteudos([], 'pagina');

        $data['itens'] = $itens;

        return view('painel/pagina', $data);
    }

    public function editar($id)
    {
        nivelAcessoLogin(getNivelAcesso(), [1], base_url('painel'));
        if (!$item = $this->conteudoModel->find($id)) {
            return redirect()->to(base_url('painel'));
        }
        $data['title'] = 'Editar conteúdo';
        $data['links'] = $this->arquivos('');
        $data['description'] = '';
        $data['titulo'] = 'Editar ' . $item->descricao;
        $data['paginas'] = $this->conteudoModel->getConteudos([], 'pagina');

        $data['pagina'] = $item;

        $this->verificaAction($id);

        return view('painel/pagina-editar', $data);
    }

    private function salvar($id)
    {
       
        unset($_POST['action']);


        if ($file = $this->request->getFile('file_imgem_sobre_a_lorena')) {
            
            
            $dir          = './public/portal/img/conteudo';
            $ext          = $file->getExtension();
            $imgName      =  'conteudo' . $id . '.' . $ext;
            $extPermitida = ['png', 'jpg', 'jpeg', 'webp'];
            
            
            
            if (in_array($file->getExtension(), $extPermitida)) {

                if ($file->move($dir, $imgName, true)) {
                    $_POST['file_imgem_sobre_a_lorena'] = $imgName;
                }
            }
        }

        $dados['conteudo'] = json_encode($_POST);

        if ($this->conteudoModel->update($id, $dados)) {
            echo '<script>window.location=window.location</script>';
        }


    }

    private function verificaAction($id = null)
    {
        if ($action = $this->request->getPost('action')) {
            switch ($action) {
                case 'Salvar':
                    return $this->salvar($id);
                    break;
            }
        }
    }

    private function arquivos($pagina)
    {
        switch ($pagina) {
            default:
                $arquivos['css'] = [];
                $arquivos['js']  = [];
                $arquivos['jsOut']  = [];
                return $arquivos;
                break;
        }
    }
}
