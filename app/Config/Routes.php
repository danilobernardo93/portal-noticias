<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Pagina');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('politicas-de-privacidade', 'Pagina::politicaDePrivacidade');
$routes->get('termos-de-uso', 'Pagina::termosDeUso');
$routes->get('/', 'Pagina::index');
$routes->get('sobre', 'Pagina::sobre');
$routes->add('contato', 'Pagina::contato');
$routes->get('busca/(:any)', 'Pagina::busca/$1');

// Colunistas -> Lista de colunistas
$routes->get('colunistas', 'Pagina::colunistas');
$routes->get('colunistas/(:num)', 'Pagina::colunistas/$1');

// Colunista -> Perfil e Postagens
$routes->get('colunista/(:any)', 'Pagina::colunista/$1');
$routes->get('colunista/(:any)/(:num)', 'Pagina::colunista/$1/$2');

// Categorias -> Lista de categorias
$routes->add('categoria/(:any)', 'Pagina::categoria/$1');
$routes->add('categoria/(:any)/(:num)', 'Pagina::categoria/$1/$2');  

// SubCategorias -> Lista de subCategorias
$routes->get('subcategoria/(:any)', 'Pagina::subCategoria/$1');
$routes->add('subcategoria/(:any)/(:num)', 'Pagina::subCategoria/$1/$2'); 

// Postagens rescentes
$routes->get('recentes/(:num)', 'Pagina::recentes/$1');
$routes->get('recentes', 'Pagina::recentes');

$routes->post('grava-email-news', 'Pagina::salvaEmailNews');

$routes->get('rss', 'Pagina::rss');

$routes->add('enquete', 'Pagina::enquete');

$routes->add('/preview/(:any)','Pagina::postagemPreview/$1');




// ______________________________________________________________

//TESTES TESTE TESTES TESTES 
$routes->get('lerfeed','Pagina::lerFeed');




// ______________________________________________________________

// PAINEL
$routes->add('painel','Dashboard::index');
// __________________________________________________________
$routes->add('painel/perfil','Usuario::perfil');
// __________________________________________________________
$routes->add('logout','Login::logout');
// __________________________________________________________
$routes->add('painel/usuario','Usuario::index');
$routes->add('painel/usuario/novo','Usuario::novo');
$routes->add('painel/usuario/(:num)','Usuario::editar/$1');
$routes->post('painel/usuario/deletar','Usuario::deletar');
// __________________________________________________________
$routes->add('painel/post','Post::index');
$routes->add('painel/post/rascunho','Post::rascunho');
$routes->add('painel/post/novo','Post::novo');
$routes->add('painel/post/(:num)','Post::editar/$1');
$routes->get('painel/post/page/(:num)','Post::index/$1');
$routes->add('painel/post/deletar','Post::deletar');
// __________________________________________________________
$routes->add('painel/categoria','Categoria::index');
$routes->add('painel/categoria/nova','Categoria::nova');
$routes->add('painel/categoria/deletar','Categoria::deletar');
$routes->add('painel/categoria/destaque-home','Categoria::destaqueHome');
// __________________________________________________________
$routes->add('painel/galeria-imagens','GaleriaImagens::index');
$routes->add('painel/galeria-imagens/(:num)','GaleriaImagens::index/$1');
$routes->post('painel/galeria-imagens/deletar','GaleriaImagens::deletar');
// __________________________________________________________
$routes->add('painel/comentario','Comentario::index');
$routes->add('painel/comentario/(:num)','Comentario::editar/$1');
$routes->get('painel/comentario/page/(:num)','Comentario::index/$1');
// __________________________________________________________
$routes->get('painel/conteudo/pagina/(:any)','Conteudo::index/$1');
$routes->add('painel/conteudo/(:any)/(:num)','Conteudo::editar/$2');
// __________________________________________________________
$routes->add('painel/revista','Revista::index');
$routes->post('painel/revista/deletar','Revista::deletar');



$routes->add('sitemap','Post::createSiteMap');
//Post -> Coloca o nome do Jornalista/Colunista
$routes->add('(:any)/(:any)', 'Pagina::postagem/$2');
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
