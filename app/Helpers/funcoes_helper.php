<?php
function geraXML()
{
    header("content-type:text/xml; charset=iso-8859-1");
    echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
    echo '<rss version="2.0">' . PHP_EOL;
    echo '<channel>' . PHP_EOL;
    echo '<title>Feed Lorena Bueri</title>' . PHP_EOL;
    echo '<link>http://localhost/lorena</link>' . PHP_EOL;
    echo '<description>Teste</description>' . PHP_EOL;
    echo '<item>' . PHP_EOL;
    echo '<guid>https://localhost/lorena/link-unico</guid>' . PHP_EOL;
    echo '<pubDate>Mon, 12 Nov 2018 13:40:14 +0000</pubDate>' . PHP_EOL;
    // echo '<author>danilo.bernardo1993@gmail.com</author>' . PHP_EOL;
    echo '<category>' . PHP_EOL;
    echo '<![CDATA[  nome da categoria ]]>' . PHP_EOL;
    echo '</category>' . PHP_EOL;
    echo '<description>' . PHP_EOL;
    echo 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ultrices mauris lacinia rhoncus viverra. Nullam ut metus accumsan, tincidunt augue nec, congue odio. Phasellus dignissim efficitur metus vel posuere. Fusce et magna dignissim, convallis sem at, rutrum ante. Mauris dictum eget odio id pretium. Quisque mi elit, placerat sed porta sed, cursus id tellus. Nulla vel bibendum mi. Nam consequat, ligula sed suscipit facilisis, neque magna consectetur lectus, ac rhoncus nisl velit id nisi. Phasellus quis viverra lacus. Donec in pulvinar arcu. Nulla turpis nulla, consectetur ornare semper quis, egestas nec dui. Curabitur tristique sollicitudin semper.    ' . PHP_EOL;
    echo '</description>' . PHP_EOL;
    echo '</item>' . PHP_EOL;
    echo '</channel>' . PHP_EOL;
    echo '</rss>';
}

function listaCategoria($categoriasPost, $listaCetgorias)
{
    $categoriasPost = explode(';', $categoriasPost);
    $categorias = '';
    foreach ($categoriasPost as $valor) {
        if (array_key_exists($valor, $listaCetgorias)) {
            $categorias .= ' | <a href="' . base_url('categoria/' . $listaCetgorias[$valor]['uri']) . '"><span class="preto">' . $listaCetgorias[$valor]['nome'] . '</small></a>';
        }
    }

    return $categorias;
}

function validaCheckbox($array, $search)
{
    if (in_array($search, $array)) {
        return '<input type="checkbox" name="categorias[]" value="' . $search . '" checked />';
    }
    return '<input type="checkbox" name="categorias[]" value="' . $search . '"  />';
}

function setCampos($name, $value)
{
    $tipo = explode("_", $name);
    $tipoCampo = $tipo[0];
    unset($tipo[0]);
    $placeholder = implode("_", $tipo);
    $placeholder = str_replace("_", ' ', $placeholder);
    switch ($tipoCampo) {
        case 'input':
            return '<input type="text" name="' . $name . '" placeholder="' . $placeholder . '" value="' . $value . '" class="form-control mb-1">';
            break;

        case 'text':
            return '<textarea name="' . $name . '" class="form-control mb-1" rows="10">' . $value . '</textarea>';
            break;

        case 'file':
            return '<label>' . $placeholder . '</label><br/>
            <input type="file" name="' . $name . '" class="form-control mb-1"><br/>
            <input type="hidden" name="' . $name . '" value="' . $value . '" class="form-control mb-1"><br/>
            <img src="' . base_url('/public/portal/img/conteudo/' . $value) . '" width="300px">';
            break;
    }
}


function getImg($arquivo = 'vazio', $dir)
{
    $file = 'public/portal/img/' . $dir . '/' . $arquivo;
    if ($arquivo == 'vazio' || $arquivo == '') {
        return '';
    }
    return file_exists($file) ? base_url($file) . '?' . rand(0, 10) : '';
}

function limitarTexto($texto, $limite)
{
    $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
    return $texto;
}


function paginacao($numPage, $Pagecurrent, $link,$maxLinks = 4)
{
    $itens = '';
    $linksLatereais = ceil($maxLinks / 2);
    $comeco = $Pagecurrent - $linksLatereais;
    $inicio = $comeco < 0 ? 1 : $comeco;
    $limite = $Pagecurrent + $linksLatereais;
    $numPage;
    $previous = $next = '';
    for ($i = $inicio; $i <= $limite; $i++) {
        if ($i <= $numPage) {
            $i        = $i == 0 ? 1 : $i;
            $active   = $Pagecurrent == $i ? ' active ' : '';
            $url      = $Pagecurrent == $i ? '#' : base_url($link . '/' . $i);
            $previous = ($Pagecurrent - 1) > 0 ? base_url($link . '/' . ($Pagecurrent - 1)) : 'javascript:void()';
            $next     = ($Pagecurrent + 1) <= $numPage ? base_url($link . '/' . ($Pagecurrent + 1)) : 'javascript:void()';
            $itens   .= '<li class=" ' . $active . '"><a href="' . $url . '">' . $i . '</a></li>';
        }
    }

    if ($numPage > 1) {
        return
            '<ul class="pagination float--right">
                <li><a href="'.$previous.'"><i class="fa fa-long-arrow-left"></i></a></li>
                ' . $itens . '
                <li><a href="' . $next . '"><i class="fa fa-long-arrow-right"></i></a></li>
            </ul>';
    }
}

function setSelected($busca, $compara, $imprime = false)
{
    $txt = $imprime == true ? $imprime : $busca;
    return $busca == $compara ? "<option value='$busca' SELECTED >$txt</option>" : "<option value='$compara' >$txt</option>";
}

function setLink($value, $type = null,  $link = '#',  $class = '', $other = '')
{
    switch ($type) {
        case 'whats':
            $numero = str_replace(array('(', ')', '-', ' ', ''), "", $value);
            return "<a href='https://api.whatsapp.com/send?phone=55$numero' target='_blank' class='$class' $other ><i class='fa fa-whatsapp'></i> $value</a>";
            break;

        default:
            return "<a href='$link' class='$class' $other >$value</a>";
            break;
    }
}


function nivelAcessoLogin($nivel = null, $autorizado = ['1'], $urlRedirecionamento = null)
{
    $urlRedirecionamento = $urlRedirecionamento ? $urlRedirecionamento : base_url();
    if (!isset($_SESSION['user']['user_id'])) {
        header("location: " . $urlRedirecionamento);
        die();
    }

    if (!in_array($nivel, $autorizado)) {
        echo header("location: " . $urlRedirecionamento);
        die();
    }
}

function getNivelAcesso()
{
    if (isset($_SESSION['user']['nivel_acesso'])) {
        return $_SESSION['user']['nivel_acesso'];
    }
}


function formataDta($data, $formato)
{
    return strftime($formato, strtotime($data));
}
