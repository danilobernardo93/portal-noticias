<?php namespace App\Models;

use CodeIgniter\Model;

class CategoriaModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'categorias';
    protected $primaryKey = 'categoria_id';
    protected $useTimestamps = true;
    protected $updatedField = 'updated_at';
    protected $allowedFields = ['categoria_nome','categoria_uri','cor','menu','rodape','created_at','updated_at'];


    public function getCategoriaUri($uri)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->get();
        $result = $builder->like('categoria_uri',$uri,'before')->get(1);
        $DB->close();
        return $result->getRowObject();
    }

    public function getCategoriasMenu($array)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->get();
        $builder->orderBy('categoria_nome','ASC');
        $result = $builder->getWhere($array);
        $DB->close();
        return $result->getResult();
    }

    public function getCategoriasDesstaque($array)
    {
        $DB = db_connect();
        $builder = $DB->table('categorias_destaque');
        $result = $builder->getWhere($array);
        $DB->close();
        return $result->getRowObject();
    }

    public function updateCategoriasDesstaque($id, $dados)
    {
        $DB = db_connect();
        $builder = $DB->table('categorias_destaque');
        $builder->update($dados,['destaque_id'=>$id]);
        $result = $DB->affectedRows();
        $DB->close();
        return $result;
        
    }

    public function findSubcategorias()
    {
        $DB = db_connect();
        $builder = $DB->table('subCategorias');
        $result = $builder->get();
        $DB->close();
        return $result->getResult();   
    }

    public function getSubCategoriaUri($subcategoria)
    {
        $DB = db_connect();
        $builder = $DB->table('subCategorias');
        $builder->get();
        // $builder->where('subcategoria_categoria_id',$categoria);
        $result = $builder->like('subcategoria_uri',$subcategoria,'before')->get(1);
        $DB->close();
        return $result->getRowObject();
    }
    
}