<?php

namespace App\Models;

use App\Controllers\Inicio;
use CodeIgniter\Model;

class PostModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'post';
    protected $primaryKey = 'post_id';
    // protected $useTimestamps = true;
    // protected $updatedField  = 'post_updated_at';
    // protected $updatedField  = 'post_created_at';
    protected $allowedFields = [
        'post_titulo', 'post_texto', 'post_categoria', 'post_img', 'post_uri', 'post_rascunho',
        'post_views', 'post_user_id', 'post_ativo', 'post_destaque', 'post_resumo', 'keywords', 'comentarios', 'post_updated_at', 'post_created_at'
    ];


    public function postGetWhere($array, $limite = null, $inicio = 0)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->join('categorias', 'categorias.categoria_id = post.post_categoria');
        $result = $builder->getWhere($array, $limite, $inicio);
        $DB->close();
        return $result->getResult();
    }

    public function getPostADM($array, $limite = null, $inicio = 0)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->join('categorias', 'categorias.categoria_id = post.post_categoria');
        $result = $builder->getWhere($array, $limite, $inicio);
        $DB->close();
        return $result->getResult();
    }

    public function numRegistro($where, $like = false)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->join('categorias', 'categorias.categoria_id = post.post_categoria');
        $builder->join('usuarios', 'usuarios.user_id = post.post_user_id');
        $like ? $builder->like('post_titulo', $like) : '';
        $result = $builder->getWhere($where);
        $DB->close();
        return $result->getResult();
    }

    public function getPost($uri, $where)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->select();
        $builder->join('categorias', 'categorias.categoria_id = post.post_categoria');
        $builder->join('usuarios', 'usuarios.user_id = post.post_user_id');
        $builder->where($where);
        $builder->orderBy('post.post_updated_at', 'DESC');
        $result = $builder->like('post.post_uri', $uri, 'before')->get(1);
        $DB->close();
        return $result->getRowObject();
    }

    public function getPosts($where, $limite = null, $inicio = 0)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->select();
        $builder->join('categorias', 'categorias.categoria_id = post.post_categoria');
        $builder->orderBy('post.post_id', 'DESC');
        $result = $builder->getWhere($where, $limite, $inicio);
        $DB->close();
        return $result->getResult();
    }

    public function getComentarios($postId)
    {
        $DB = db_connect();
        $builder = $DB->table('comentarios');
        $result = $builder->getWhere(['comentario_post_id' => $postId, 'comentario_ativo' => 1]);
        $DB->close();
        return $result->getResult();
    }

    public function getPostsCategoria($where, $limite, $inicio = 0)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->select();
        $builder->join('categorias', 'categorias.categoria_id = post.post_categoria');
        $builder->join('subCategorias', 'subCategorias.subcategoria_categoria_id = categorias.categoria_id','left');
        $builder->join('usuarios', 'usuarios.user_id = post.post_user_id');
        $builder->orderBy('post.post_updated_at', 'DESC');
        $result = $builder->getWhere($where, $limite, $inicio);
        $DB->close();
        return $result->getResult();
    }

    public function getPostsResncentes($where, $limite, $inicio = 0)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->select();
        $builder->join('categorias', 'categorias.categoria_id = post.post_categoria');
        $builder->join('usuarios', 'usuarios.user_id = post.post_user_id');
        $builder->orderBy('post.post_updated_at', 'DESC');
        $result = $builder->getWhere($where, $limite, $inicio);
        $DB->close();
        return $result->getResult();
    }

    public function getBuscaPosts($like, $limite, $inicio = 0)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->select();
        $builder->join('categorias', 'categorias.categoria_id = post.post_categoria');
        $builder->like('post_titulo', $like);
        $builder->groupBy('post_id');
        $builder->orderBy('post.post_updated_at', 'DESC');
        $result = $builder->getWhere(['post_ativo' => 1, 'post_rascunho' => 0], $limite, $inicio);
        $DB->close();
        return $result->getResult();
    }

    public function getPostMaisVisto($where, $limite)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->select();
        $builder->join('categorias', 'categorias.categoria_id = post.post_categoria');
        $builder->join('usuarios', 'usuarios.user_id = post.post_user_id');
        $builder->orderBy('post.post_views', 'DESC');
        $result = $builder->getWhere($where, $limite);
        $DB->close();
        return $result->getResult();
    }
}
