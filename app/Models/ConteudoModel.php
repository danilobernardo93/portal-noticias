<?php namespace App\Models;

use CodeIgniter\Database\MySQLi\Builder;
use CodeIgniter\Model;

class ConteudoModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'conteudo';
    protected $primaryKey = 'conteudo_id';
    protected $allowedFields = ['conteudo'];

    public function getConteudos($where,$groupby='descricao')
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->orderBy($groupby,'ASC');
        $builder->groupBy($groupby);
        $result = $builder->getWhere($where);
        $DB->close();
        return $result->getResult();
    }
}