<?php namespace App\Models;

use CodeIgniter\Model;

class RevistaModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'revistas';
    protected $primaryKey = 'revista_id';

    protected $allowedFields = ['revista_id','revista_img','revista_titulo'];    


}