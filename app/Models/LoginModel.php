<?php namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'usuarios';
    protected $primaryKey = 'user_id';
    protected $useTimestamps = true;
    protected $updatedField  = 'updated_at';

    protected $allowedFields = ['user_nome','user_email','user_senha','user_nivel_id','user_ativo'];    

    public function login($dados)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $resultado = $builder->getWhere($dados);
        $DB->close();
        return $resultado->getRowObject();

    }
}