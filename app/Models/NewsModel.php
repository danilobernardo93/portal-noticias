<?php namespace App\Models;

use CodeIgniter\Model;

class NewsModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'newsletter';
    protected $primaryKey = 'newsletter_id';

    protected $allowedFields = ['newsletter_id','newsletter_email','newsletter_data'];    

}