<?php namespace App\Models;

use CodeIgniter\Model;

class ComentarioModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'comentarios';
    protected $primaryKey = 'comentario_id';
    protected $useTimestamps = true;
    protected $updatedField = 'updated_at';
    protected $allowedFields = ['comentario_nome','comentario_email','comentario_assunto','comentario_texto','comentario_ativo','comentario_post_id','created_at','updated_at'];

    public function getComentarios($where,$limite,$inicio=0)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->orderBy('comentario_ativo','ASC');
        $result = $builder->getWhere($where,$limite,$inicio);
        $DB->close();
        return $result->getResult();
    }

    public function numComentarios()
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $result = $builder->where('comentario_ativo',0);
        $DB->close();
        return $result->countAllResults(false);
    }
} 