<?php namespace App\Models;

use CodeIgniter\Model;

class EnqueteModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'enquete';
    protected $primaryKey = 'enquete_id';
    // protected $useTimestamps = true;
    // protected $updatedField  = 'updated_at';

    protected $allowedFields = ['enquete_id','enquete_texto','enquete_titulo','enquete_status'];    


    public function getEnquete($params)
    {
        $DB = db_connect();
        $result = $DB->table($this->table)->select()->join("enquete_alternativas","enquete_alternativas.enquete_id = $this->table.enquete_id")->getWhere($params);
        // $result = $builder->
        $DB->close();
        return $result->getResult();
    }

    public function getAlternativaEnquete($params)
    {
        $DB = db_connect();
        $result = $DB->table('enquete_alternativas')->select()->getWhere($params);
        $DB->close();
        return $result->getRowObject();
    }

    public function updateAlternativaEnquete($where,$params)
    {
        $DB = db_connect();
        $builder = $DB->table('enquete_alternativas')->update($params,$where);
        $result  = $DB->affectedRows();
        $DB->close();
        return $result;
    }
}