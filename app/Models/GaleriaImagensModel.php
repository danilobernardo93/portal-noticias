<?php namespace App\Models;

use CodeIgniter\Model;

class GaleriaImagensModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'galeriaImagens';
    protected $primaryKey = 'imagem_id';

    protected $allowedFields = ['imagem_id','imagem_uri','imagem_title','imagem_nome'];    

    public function getImagens($limite=null,$inicio=0)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->orderBy('imagem_id','DESC');
        $result = $builder->get($limite,$inicio);
        $DB->close();
        return $result->getResult();
    }
}