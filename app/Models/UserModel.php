<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $returnType = 'object';
    protected $table = 'usuarios';
    protected $primaryKey = 'user_id';
    // protected $useTimestamps = true;
    // protected $updatedField  = 'updated_at';

    protected $allowedFields = ['user_coluna','user_foto','user_nome','user_uri','user_email','user_senha','user_nivel_id','user_ativo','user_texto'];    


    public function getNiveisAcesso()
    {
        $DB = db_connect();
        $builder = $DB->table('niveis_acesso');
        $resultado = $builder->get();
        $DB->close();
        return $resultado->getResult();
    }

    public function getUsuarios($where = [],$limite=null,$inicio=0)
    {
        $DB = db_connect();
        $builder = $DB->table($this->table);
        $builder->join('niveis_acesso','nivel_id = user_nivel_id');
        $resultado = $builder->getWhere($where,$limite,$inicio);
        $DB->close();
        return $resultado->getResult();
    }
 
}