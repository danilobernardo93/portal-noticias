<?= $this->include('header') ?>
<div class="containnerr">
    <div class="container" id="bannerGrande_R7_1">
        <div class="row ">
            <div class="col-12">
                
            </div>
        </div>
    </div>
</div>
<div class="containnerr listra">
    <div class="container mt-4">
        <div class="row grupo-5">
            <div class="col-sx-12 col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">
                <div class="sobre clearfix">
                    <div class="img-sobre clearfix">
                        <img src="<?= base_url('public/portal/img/conteudo/'.$sobre->file_imgem_sobre_a_lorena) ?>" class="img-lorena" />
                    </div>
                    <div class="txt-sobre clearfix">
                        <h3 class="titulo-sobre"><?= $sobre->input_titulo ?></h3>
                        <div class="rede-social mb-3 mt-3">
                        <?php
                            foreach ($redesSociais as $key => $value) :
                                $key = explode('_', $key);
                                $rede = $key[1];
                            ?>
                                <a href="<?= $value ?>" class="preto" target="_blank"><i class="fa fa-<?= $rede ?> "></i></a>
                            <?php endforeach ?>
                        </div>
                        <?= $sobre->text_sobre ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="containnerr listra">
    <div class="container">

        <div class="row grupo-6 post-topo ">
            <h2 class="w-100">
                <center><?= $sobre2->input_titulo ?></center>
            </h2>
            <div id="descricao-carousel"><?= $sobre2->text_subtitulo ?></div>
            <div class="MultiCarousel" data-items="1,1,5,5" data-slide="1" id="MultiCarousel" data-interval="1000">
                <div class="MultiCarousel-inner">
                    <?php foreach ($imagens as $linha) : ?>
                        <div class="item">
                            <img src="<?= base_url('public/portal/img/capa-revista/' . $linha->revista_img) ?>" width="100%">
                        </div>
                    <?php endforeach ?>
                </div>
                <button class="btn btn-primary bg-5 leftLst">
                    <</button> <button class="btn btn-primary bg-5 rightLst">>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="containnerr">
    <?= $this->include('incl/news') ?>
</div>

<?= $this->include('footer') ?>