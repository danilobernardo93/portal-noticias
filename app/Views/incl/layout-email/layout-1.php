<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Email</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- IE Specific to remove tap highlight -->
    <meta name="msapplication-tap-highlight" content="no">
    <script src="https://use.fontawesome.com/d4eec62f7a.js"></script>
    <style>

    </style>
</head>

<body>
    <div class="container">
        <table style="margin:auto;max-width:600px;margin-top:50px;border-collapse:unset;border-spacing:0px;background: #f9f9f9;border-radius: 20px;">
            <tr>
                <td style="border-radius: 20px 20px 0 0;font-size: 30px;text-align: center;font-family: initial;background: #c39885;color: #fff;padding: 20px;border: none;">LORENA LOUISY</td>
            </tr>
            <tr>
                <td style="padding: 0;border:none" >
                    <img style="padding: 0;border: none;margin-top: -1px;    margin-bottom: -10px !important; width: 100%" src="https://evouc.com.br/lorena/public/portal/img/teste-99.jpeg">
                </td>
            </tr>
            <tr style="display: table;margin-top: -11px;">
                <td style="padding: 20px;background: #f9f9f9;text-align: center;color: #795548;">
                    <h3 style="font-size: 28px;"><?= $assunto ?></h3>
                </td>
            </tr>
            <tr>
                <td style="padding: 20px;text-align: center;background: #f9f9f9;">
                    <hr style="margin-bottom: 45px;">
                    <?= $mensagem ?>
                </td>
            </tr>
            <tr>
                <td style="padding: 20px;background: #f9f9f9;">
                    <hr style="margin-bottom: 45px;">
                    <p><i class="fa fa-address-card-o" aria-hidden="true"></i> <?= $nome ?></p>
                    <p><i class="fa fa-envelope-o" aria-hidden="true"></i> <?= $email ?></p>
                    <p><i class="fa fa-phone" aria-hidden="true"></i> <?= $telefone ?></p>
                </td>
            </tr>
            <tr>
                <td style="background: #c39885;color: #fff;padding: 20px;border: none;border-radius: 0 0 20px 20px;font-size: 11px;">
                    <a href="#" style="float: left;color: #fff;">
                        <i class="fa fa-instagram" aria-hidden="true"></i> LORENA LOUISY
                    </a>

                    <a href="https://danilobernardo.com.br" style=" float: right;color: #795548;">Desenvolvido por: <span>Danilo Augusto</span></a>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>