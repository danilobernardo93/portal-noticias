<div class="containnerr">
    <div class="container">
        <div class="row grupo-6 post-topo">
            <div class="MultiCarousel" data-items="1,1,2,2" data-slide="1" id="MultiCarousel" data-interval="1000">
                <div class="MultiCarousel-inner">
                    <?php foreach ($rescentes as $post) : ?>
                        <div class="item">
                            <img class="w-100" src="<?= base_url('public/portal/img/postagens/' . $post->post_img) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>">
                            <div class="dados-post">
                                <a href="<?= base_url('post/' . $post->post_uri) ?>">
                                    <h3 class="titulo-1-post"><?= $post->post_titulo ?></h3>
                                </a>
                                <a href="<?= base_url('categoria/' . $post->categoria_uri) ?>"><div class="categoria-home bg-3"  ><?= $post->categoria_nome ?></div></a>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
                <button class="btn btn-primary bg-5 leftLst">
                    <</button> <button class="btn btn-primary bg-5 rightLst">>
                </button>
            </div>
        </div>
    </div>
</div>