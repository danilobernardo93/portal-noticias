<div class="row grupo-3">
    <div class="col-md-6 col-lg-4 bd-r-1">
        <div class="titulo-secao mb-3">
            <h3><?= isset($categoriaDestaque2[0]->categoria_nome) ? $categoriaDestaque2[0]->categoria_nome : '' ?></h3>
        </div>
        <?php $i = 1;
        foreach ($categoriaDestaque2 as $post) :
            $urlImg = 'public/portal/img/postagens/thumb-' . $post->post_img;
            $imgPost = file_exists($urlImg) ? $urlImg :  'public/portal/img/postagens/' . $post->post_img; ?>
            <?php if ($i == 1) : ?>
                <div class="post-m mb-4 clearfix min-altura1">
                    <img class="w-100" src="<?= base_url($imgPost) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>" />
                    <div class="categoria bg-2"><?= $post->categoria_nome ?></div>
                    <h3 class="titulo-1-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= $post->post_titulo ?></a></h3>
                    <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>
                    <div class="comentario "><?= $post->comentarios ?> comentários</div>
                    <div class="w-100" style="display: flex;"><?= limitarTexto($post->post_resumo, 100) ?></div>
                </div>
            <?php else : ?>
                <hr />
                <div class="post-p clearfix min-altura2">
                    <div class="row">
                        <div class="col-md-5">
                            <img style="width: 100%;" src="<?= base_url($imgPost) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>" />
                        </div>
                        <div class="col-md-7">
                            <h4 class="titulo-2-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= limitarTexto($post->post_titulo, 67) ?></a></h4>
                            <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>
                        </div>
                    </div>
                    <div class="w-100" style="display: flex;"><?= limitarTexto($post->post_resumo, 100) ?></div>
                </div>
            <?php endif ?>
        <?php $i++;
        endforeach ?>
    </div>


    <div class="col-md-6 col-lg-4 bd-r-1">
        <div class="titulo-secao mb-3">
            <h3><?= isset($categoriaDestaque3[0]->categoria_nome) ? $categoriaDestaque3[0]->categoria_nome : '' ?></h3>
        </div>
        <?php $i = 1;
        foreach ($categoriaDestaque3 as $post) :
            $urlImg = 'public/portal/img/postagens/thumb-' . $post->post_img;
            $imgPost = file_exists($urlImg) ? $urlImg : $imgPost; ?>
            <?php if ($i == 1) : ?>
                <div class="post-m mb-4 clearfix min-altura1">
                    <img class="w-100" src="<?= base_url($imgPost) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>" />
                    <div class="categoria bg-2"><?= $post->categoria_nome ?></div>
                    <h3 class="titulo-1-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= $post->post_titulo ?></a></h3>
                    <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>
                    <div class="comentario "><?= $post->comentarios ?> comentários</div>
                    <div class="w-100" style="display: flex;"><?= limitarTexto($post->post_resumo, 100) ?></div>
                </div>
            <?php else : ?>
                <hr />
                <div class="post-p clearfix min-altura2">
                    <div class="row">
                        <div class="col-md-5">
                            <img style="width: 100%;" src="<?= base_url($imgPost) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>" />
                        </div>
                        <div class="col-md-7">
                            <h4 class="titulo-2-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= limitarTexto($post->post_titulo, 67) ?></a></h4>
                            <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>
                        </div>
                    </div>
                    <div class="w-100" style="display: flex;"><?= limitarTexto($post->post_resumo, 100) ?></div>
                </div>
            <?php endif ?>
        <?php $i++;
        endforeach ?>
    </div>

    <div class="col-md-12 col-lg-4 ">
        <div class="titulo-secao mb-3">
            <h3><?= isset($categoriaDestaque4[0]->categoria_nome) ? $categoriaDestaque4[0]->categoria_nome : '' ?></h3>
        </div>
        <?php $i = 1;
        foreach ($categoriaDestaque4 as $post) :
            $urlImg = 'public/portal/img/postagens/thumb-' . $post->post_img;
            $imgPost = file_exists($urlImg) ? $urlImg : 'public/portal/img/postagens/' . $post->post_img; ?>
            <?php if ($i == 1) : ?>
                <div class="post-m mb-4 clearfix min-altura1">
                    <img class="w-100" src="<?= base_url($imgPost) ?>" />
                    <div class="categoria bg-2"><?= $post->categoria_nome ?></div>
                    <h3 class="titulo-1-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= $post->post_titulo ?></a></h3>
                    <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>
                    <div class="comentario "><?= $post->comentarios ?> comentários</div>
                    <div class="w-100" style="display: flex;"><?= limitarTexto($post->post_resumo, 100) ?></div>
                </div>
            <?php else : ?>
                <div class="post-p clearfix min-altura2">
                    <hr />
                    <div class="row">
                        <div class="col-md-5">
                            <img style="width: 100%;" src="<?= base_url($imgPost) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>" />
                        </div>
                        <div class="col-md-7">
                            <h4 class="titulo-2-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= limitarTexto($post->post_titulo, 67) ?></a></h4>
                            <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>
                        </div>
                    </div>
                    <div class="w-100" style="display: flex;"><?= limitarTexto($post->post_resumo, 100) ?></div>
                </div>

            <?php endif ?>
        <?php $i++;
        endforeach ?>
    </div>
</div>

<style>
    .min-altura1 {
        min-height: 360px;
    }

    .min-altura2 {
        min-height: 166px;
    }
</style>