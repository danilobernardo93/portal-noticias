<!--Container-->
<div class="container">
    <!--Start code-->
    <div class="row">
        <div class="col-12 pb-5">
            <section class="row">
                <div class="col-12 col-md-6 pb-0 pb-md-3 pt-2 pr-md-1">
                    <div id="featured" class="carousel slide carousel" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php $action = 'active'; ?>
                            <?php foreach ($rescentes as $post) : ?>
                                <div class="carousel-item <?= $action ?>">
                                    <div class="card border-0 rounded-0 text-light overflow zoom">
                                        <div class="position-relative">
                                            <!--thumbnail img-->
                                            <div class="ratio_left-cover-1 image-wrapper">
                                                <a href="<?= base_url('post/' . $post->post_uri) ?>">
                                                    <img class="img-fluid w-100" src="<?= base_url('public/portal/img/postagens/' . $post->post_img) ?>" alt="<?= $post->post_titulo ?>">
                                                </a>
                                            </div>
                                            <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                                <!--title-->
                                                <a href="<?= base_url('post/' . $post->post_uri) ?>">
                                                    <h2 class="h3 post-title text-white my-1"><?= $post->post_titulo ?></h2>
                                                </a>
                                                <!-- meta title -->
                                                <div class="news-meta">
                                                    <span class="news-author">por <a class="text-white font-weight-bold" href="<?= base_url('colunista/' . $post->user_uri) ?>"><?= $post->user_nome ?></a></span>
                                                    <span class="news-date"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php $action = '';
                            endforeach ?>
                            <!--end item slider-->
                        </div>
                    </div>

                    <!--navigation-->
                    <a class="carousel-control-prev" href="#featured" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#featured" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <!--End slider news-->

                <!--Start box news-->
                <div class="col-12 col-md-6 pt-2 pl-md-1 mb-3 mb-lg-4">
                    <div class="row">

                        <div class="col-6 pb-1 pt-0 pr-1">
                            <div class="card border-0 rounded-0 text-white overflow zoom">
                                <div class="position-relative">
                                    <!--thumbnail img-->
                                    <div class="ratio_right-cover-2 image-wrapper">
                                        <a href="<?= base_url('post/' . $categoriaDestaque[0]->post_uri) ?>">
                                            <img class="img-fluid" src="<?= base_url('public/portal/img/postagens/' . $categoriaDestaque[0]->post_img) ?>" alt="<?= $categoriaDestaque[0]->post_titulo ?>">
                                        </a>
                                    </div>
                                    <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                        <!-- category -->
                                        <a class="p-1 badge badge-primary rounded-0" href="<?= base_url('post/' . $categoriaDestaque[0]->post_uri) ?>"><?= $categoriaDestaque[0]->categoria_nome ?></a>

                                        <!--title-->
                                        <a href="<?= base_url('post/' . $categoriaDestaque[0]->post_uri) ?>">
                                            <h2 class="h5 text-white my-1"><?= $categoriaDestaque[0]->post_titulo ?></h2>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--news box-->
                        <div class="col-6 pb-1 pl-1 pt-0">
                            <div class="card border-0 rounded-0 text-white overflow zoom">
                                <div class="position-relative">
                                    <!--thumbnail img-->
                                    <div class="ratio_right-cover-2 image-wrapper">
                                        <a href="<?= base_url('post/' . $categoriaDestaque1[0]->post_uri) ?>">
                                            <img class="img-fluid" src="<?= base_url('public/portal/img/postagens/' . $categoriaDestaque1[0]->post_img) ?>" alt="<?= $categoriaDestaque1[0]->post_titulo ?>">
                                        </a>
                                    </div>
                                    <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                        <!-- category -->
                                        <a class="p-1 badge badge-primary rounded-0" href="<?= base_url('post/' . $categoriaDestaque1[0]->post_uri) ?>"><?= $categoriaDestaque1[0]->categoria_nome ?></a>

                                        <!--title-->
                                        <a href="<?= base_url('post/' . $categoriaDestaque1[0]->post_uri) ?>">
                                            <h2 class="h5 text-white my-1"><?= $categoriaDestaque1[0]->post_titulo ?></h2>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--news box-->
                        <div class="col-6 pb-1 pr-1">
                            <div class="card border-0 rounded-0 text-white overflow zoom">
                                <div class="position-relative">
                                    <!--thumbnail img-->
                                    <div class="ratio_right-cover-2 image-wrapper">
                                        <a href="<?= base_url('post/' . $categoriaDestaque2[0]->post_uri) ?>">
                                            <img class="img-fluid" src="<?= base_url('public/portal/img/postagens/' . $categoriaDestaque2[0]->post_img) ?>" alt="<?= $categoriaDestaque2[0]->post_titulo ?>">
                                        </a>
                                    </div>
                                    <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                        <!-- category -->
                                        <a class="p-1 badge badge-primary rounded-0" href="<?= base_url('post/' . $categoriaDestaque2[0]->post_uri) ?>"><?= $categoriaDestaque2[0]->categoria_nome ?></a>

                                        <!--title-->
                                        <a href="<?= base_url('post/' . $categoriaDestaque2[0]->post_uri) ?>">
                                            <h2 class="h5 text-white my-1"><?= $categoriaDestaque2[0]->post_titulo ?></h2>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--news box-->
                        <div class="col-6 pb-1 pl-1">
                            <div class="card border-0 rounded-0 text-white overflow zoom">
                                <div class="position-relative">
                                    <!--thumbnail img-->
                                    <div class="ratio_right-cover-2 image-wrapper">
                                        <a href="<?= base_url('post/' . $categoriaDestaque3[0]->post_uri) ?>">
                                            <img class="img-fluid" src="<?= base_url('public/portal/img/postagens/' . $categoriaDestaque3[0]->post_img) ?>" alt="<?= $categoriaDestaque3[0]->post_titulo ?>">
                                        </a>
                                    </div>
                                    <div class="position-absolute p-2 p-lg-3 b-0 w-100 bg-shadow">
                                        <!-- category -->
                                        <a class="p-1 badge badge-primary rounded-0" href="<?= base_url('post/' . $categoriaDestaque3[0]->post_uri) ?>"><?= $categoriaDestaque3[0]->categoria_nome ?></a>

                                        <!--title-->
                                        <a href="<?= base_url('post/' . $categoriaDestaque3[0]->post_uri) ?>">
                                            <h2 class="h5 text-white my-1"><?= $categoriaDestaque3[0]->post_titulo ?></h2>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end news box-->
                    </div>
                </div>
                <!--End box news-->
            </section>
            <!--END SECTION-->
        </div>
    </div>
  
</div>
<style>
    .b-0 {
        bottom: 0;
    }

    .bg-shadow {
        background: rgba(76, 76, 76, 0);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(179, 171, 171, 0)), color-stop(49%, rgba(48, 48, 48, 0.37)), color-stop(100%, rgba(19, 19, 19, 0.8)));
        background: linear-gradient(to bottom, rgba(179, 171, 171, 0) 0%, rgba(48, 48, 48, 0.71) 49%, rgba(19, 19, 19, 0.8) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#4c4c4c', endColorstr='#131313', GradientType=0);
    }

    .top-indicator {
        right: 0;
        top: 1rem;
        bottom: inherit;
        left: inherit;
        margin-right: 1rem;
    }

    .overflow {
        position: relative;
        overflow: hidden;
    }

    .zoom img {
        transition: all 0.2s linear;
    }

    .zoom:hover img {
        -webkit-transform: scale(1.1);
        transform: scale(1.1);
    }

    @media (max-width: 450px) {
    .h5{
        font-size: 12px !important;
    }}
</style>