<div class="row grupo-4">

    <div class="col-md-12">
        <h3 class="titulo-1 rosa-3">Mais Lidas</h3>
    </div>
    <?php foreach ($maisLidas as $linha) :
        $urlImg = 'public/portal/img/postagens/thumb-' . $linha->post_img;
        $imgPost = file_exists($urlImg) ? $urlImg : 'public/portal/img/postagens/' . $linha->post_img; ?>
        <div class="post-p col-md-6 clearfix">
            <a href="<?= base_url('post/' . $linha->post_uri) ?>">
                <div class="row">
                    <div class="col-md-5">
                        <img src="<?= base_url($imgPost) ?>" title="<?= $linha->post_titulo ?>" alt="<?= $linha->post_titulo ?>" />
                    </div>
                    <div class="col-md-7">
                        <h3 class="titulo-1-post"><?= $linha->post_titulo ?></h3>
                        <p><?= limitarTexto($linha->post_resumo, 100) ?></p>
                       
                    </div>
                </div>

            </a>
        </div>
    <?php endforeach ?>

</div>