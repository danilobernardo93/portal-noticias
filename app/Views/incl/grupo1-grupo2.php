<div class="row grupo-2">
    <div class="col-md-6 bd-r-1 ultimas-celular">
        <div class="row">
            <div class="titulo-secao mb-3 w-100">
                <h3>Últimas</h3>
                <h4>Fique por dentro das últimas postagens </h4>
            </div>
            <?php foreach ($rescentes as $linha) :
                $urlImg = 'public/portal/img/postagens/thumb-' . $linha->post_img;
                $imgPost = file_exists($urlImg) ? $urlImg : 'public/portal/img/postagens/' . $linha->post_img;
            ?>
                <div class="post-m mb-4 col-lg-4 col-md-6">
                    <img src="<?= base_url($imgPost) ?>" title="<?= $linha->post_titulo ?>" alt="<?= $linha->post_titulo ?>" />
                    <h3 class="titulo-1-post"><a href="<?= base_url('post/' . $linha->post_uri) ?>"><?= $linha->post_titulo ?></a></h3>
                    <div class=" data"><?= formataDta($linha->post_updated_at, '%d de %b de %Y') ?></div>

                </div>
            <?php endforeach ?>

            <div class="col-md-12 center">
                <a class="" href="<?= base_url('recentes') ?>"><b>Ver mais</b></a>
            </div>

            <div class="col-12">
                <div id="bannerCelularR7_2" class="mb-6 mt-4"></div>
            </div>
        </div>
    </div>


    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6 ">
                <div class="titulo-secao mb-3">
                    <h3><?= isset($categoriaDestaque[0]->categoria_nome) ? $categoriaDestaque[0]->categoria_nome : '' ?></h3>
                </div>
                <?php $i = 1;
                foreach ($categoriaDestaque as $post) :
                    $urlImg = 'public/portal/img/postagens/thumb-' . $post->post_img;
                    $imgPost = file_exists($urlImg) ? $urlImg : 'public/portal/img/postagens/' . $post->post_img;
                ?>
                    <?php if ($i == 1) : ?>
                        <div class="post-m mb-4 clearfix">
                            <img class="w-100" src="<?= base_url($imgPost) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>" />
                            <div class="categoria bg-2"><?= $post->categoria_nome ?></div>
                            <h3 class="titulo-1-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= $post->post_titulo ?></a></h3>
                            <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>
                            <div class="comentario "><?= $post->comentarios ?> comentários</div>
                        </div>
                    <?php else : ?>
                        <div class="post-p clearfix">
                            <img src="<?= base_url($imgPost) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>" />
                            <div class="titulo-data">
                                <h4 class="titulo-2-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= $post->post_titulo ?></a></h4>
                                <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>

                            </div>
                        </div>
                        <hr />
                    <?php endif ?>
                <?php $i++;
                endforeach ?>
            </div>

            <div class="col-md-6 ">
                <div class="titulo-secao mb-3">
                    <h3><?= isset($categoriaDestaque1[0]->categoria_nome) ? $categoriaDestaque1[0]->categoria_nome : '' ?></h3>
                </div>
                <?php $i = 1;
                foreach ($categoriaDestaque1 as $post) : 
                    $urlImg = 'public/portal/img/postagens/thumb-' . $post->post_img;
                    $imgPost = file_exists($urlImg) ? $urlImg : 'public/portal/img/postagens/' . $post->post_img;
                    if ($i == 1) :?>
                        <div class="post-m mb-4 clearfix">
                            <img class="w-100" src="<?= base_url($imgPost) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>" />
                            <div class="categoria bg-2"><?= $post->categoria_nome ?></div>
                            <h3 class="titulo-1-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= $post->post_titulo ?></a></h3>
                            <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>
                            <div class="comentario "><?= $post->comentarios ?> comentários</div>
                        </div>
                    <?php else : ?>
                        <div class="post-p clearfix">
                            <img src="<?= base_url($imgPost) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>" />
                            <div class="titulo-data">
                                <h4 class="titulo-2-post"><a href="<?= base_url('post/' . $post->post_uri) ?>"><?= $post->post_titulo ?></a></h4>
                                <div class=" data"><?= formataDta($post->post_updated_at, '%d de %b de %Y') ?></div>

                            </div>
                        </div>
                        <hr />
                    <?php endif ?>
                <?php $i++;
                endforeach ?>
            </div>
        </div>
    </div>

</div>