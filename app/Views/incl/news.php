<div class="container">
    <div class="row grupo-7">
        <div class="col-sx-12 col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">
            <div class="news clearfix">
                <div class="titulo-news clearfix">
                    <h4 class="mt-3 mb-3 text-center w-100"><?= $news->input_titulo ?></h4>
                    <hr>
                </div>
                <div class="form text-center">
                    <p class="text-center"><?= $news->input_subtitulo ?></p>
                    <input type="email" id="emailNews" class="form-control input-news" placeholder="E-mail" />
                    <button class="btn btn-news" id="cadastrarNews">Cadastrar agora</button>
                    <div class="alert alert-info" id="msgNews" role="alert">
                        Email cadastrar com sucesso
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>