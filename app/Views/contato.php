<?= $this->include('header') ?>
<div class="containnerr">
    <div class="container" id="bannerGrande_R7_1">
        <div class="row ">
            <div class="col-12">
                
            </div>
        </div>
    </div>
</div>
<div class="container mt-5">
    <h1 class="titulo-pagina center mb-5 mt-5">Fale conosco</h1>
    <div class="row">
        <div class="col-md-8">
            <form method="POST">
                <div class="row">
                <div class="col-md-6 mb-3">
                        <input type="text" class="form-control" name="nome" placeholder="Nome" />
                    </div>
                    <div class="col-md-6 mb-3">
                        <input type="text" class="form-control" name="telefone" placeholder="Telefone com DD" />
                    </div>

                    <div class="col-md-6 mb-3">
                        <input type="email" class="form-control" name="email" placeholder="Email" />
                    </div>
                    <div class="col-md-6 mb-3">
                        <input type="text" class="form-control" name="assunto" placeholder="Assunto" />
                    </div>

                    <div class="col-md-12 mb-3">
                        <textarea name="mensagem" rows="5" class="form-control"></textarea>
                    </div>

                    <div class="col-md-12">
                        <input type="submit" class="btn btn-primary" name="action" value="Enviar Mensagem">
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-4">
            <p>Entre em contato conosco por e-mail também
            <h3>Comercial</h3>
            <?=$contato->input_email_comercial?>
            <br/>
            <h3>Editorial</h3>
            <?=$contato->input_email_editorial?>
        </div>
    </div>
</div>
<?= $this->include('footer') ?>