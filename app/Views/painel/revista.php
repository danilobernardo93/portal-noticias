<?= $this->include('painel/header') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $title ?></h1>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <label>Selecione até 20 itens para upload </label><br />
        <label><small>Extensões permitidas: 'png' , 'jpg' , 'jpeg' , 'webp'</small></label>
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <input type="file" required multiple="" accept=".jpg, .jpeg, .png, .webp" class="form-control" name="imagens[]" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" name="action" value="Adicionar imagens">
            </div>
        </form>

        <ul class="list-group">
            <?php if (count($msg) > 0) : foreach ($msg as $linha) : ?>
                    <li class="list-group-item"><?= $linha ?></li>
            <?php endforeach;
            endif ?>
        </ul>
    </div>
</div>

<div class="row">
    <?php foreach ($imagens as $linha) : ?>
        <div class="col-md-3">
            <i class="fa fa-trash" onclick="deletaIMG('<?=$linha->revista_id?>','<?=$linha->revista_img?>')" aria-hidden="true"></i>
            <img src="<?= base_url('public/portal/img/capa-revista/' . $linha->revista_img) ?>" width="100%">
        </div>
    <?php endforeach; ?>
</div>
<?= $this->include('painel/footer') ?>