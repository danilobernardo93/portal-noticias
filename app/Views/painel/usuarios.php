<?= $this->include('painel/header') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $title ?></h1>
    </div>
</div>

<div class="row">
    <ul class="list-group">
        <?php foreach ($users as $linha) : ?>
            <li class="list-group-item clearfix">
                <div class="info-usuario">
                    <small>Nome</small><br />
                    <?= $linha->user_nome ?>
                </div>
                <div class="info-usuario">
                    <small>Email</small><br />
                    <?= $linha->user_email ?>
                </div>
                <div class="info-usuario">
                    <small>Nivel de acesso</small><br />
                    <?= $linha->nivel_nome ?>
                </div>

                <div class="info-usuario2 txt-right">
                    <a class="icone-edit" href="<?= base_url('painel/usuario/' . $linha->user_id) ?>"><i class="fa fa-edit "></i></a>
                    <a class="icone-del" onclick="deletarUsuario(<?= $linha->user_id ?>,'<?= ($linha->user_nome) ?>')" href="javascript:void(0)"><i class="fa fa-trash "></i></a>
                </div>

            </li>
        <?php endforeach ?>
    </ul>
</div>
<?= $this->include('painel/footer') ?>