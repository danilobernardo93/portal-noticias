<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="url_painel" content="<?=base_url('painel')?>">

    <title>Painel de controler</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('public/painel/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?= base_url('public/painel/css/metisMenu.min.css') ?>" rel="stylesheet">

    <!-- Timeline CSS -->
    <!-- <link href="<?= base_url('public/painel/css/timeline.css') ?>" rel="stylesheet"> -->

    <!-- Custom CSS -->
    <link href="<?= base_url('public/painel/css/startmin.css') ?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?= base_url('public/painel/css/morris.css') ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= base_url('public/painel/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('public/painel/css/main.css') ?>" rel="stylesheet">
    <?php foreach ($links['css'] as $link) : ?>
        <link href="<?= base_url($link) ?>?2" rel="stylesheet">
    <?php endforeach ?>
    <?php foreach ($links['jsOut'] as $link) : ?>
        <script src="<?= base_url($link) ?>"></script>
    <?php endforeach ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>

    <div id="wrapper">
        <?
        if(!isset($ocultaMenu))
        {
            echo $this->include('painel/menu');
        }
    ?>