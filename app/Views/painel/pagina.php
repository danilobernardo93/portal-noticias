<?= $this->include('painel/header') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $title ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <ul class="list-group">
            <?php foreach ($itens as $item) : ?>
                <li class="list-group-item clearfix">
                    <?=$item->descricao?>
                    <a class="icone-edit right" href="<?= base_url('painel/conteudo/'.$pagina.'/' . $item->conteudo_id) ?>"><i class="fa fa-edit "></i></a>
                </li>
            <?php endforeach ?>
        </ul>

    </div>

</div>
<?= $this->include('painel/footer') ?>