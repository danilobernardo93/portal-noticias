<?= $this->include('painel/header') ?>
<link rel="stylesheet" href="<?= base_url('public/painel/recort/css/jquery.Jcrop.css') ?>" type="text/css" />

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar post
            <a href="<?= base_url('post/preview/' . $post->post_uri) ?>" target="_blank" class="btn btn-preview right">Ver postagem</a>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $msg ? $msg : ''; ?>
    </div>
</div>

<form method="POST" enctype="multipart/form-data">
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">Matéria</div>
        <div class="panel-body">
            <div class="col-md-6 ">
                <h3 class="titulo">Título</h3>
                <input type="text" name="post_titulo" value="<?= $post->post_titulo ?>" class="form-control" required>
            </div>

            <div class="col-md-6  clearfix">
                <h3 class="titulo">Categorias</h3>
                <select name="categoria" class="form-control">
                    <?php foreach ($categorias as $linha) : ?>
                        <?= setSelected($post->post_categoria, $linha->categoria_id, $linha->categoria_nome) ?>
                    <?php endforeach ?>
                </select>
            </div>

            <div class="col-md-12 ">
                <hr>
                <h3 class="titulo">Conteúdo</h3>
                <textarea name="conteudo" class="form-control editor-conteudo"><?= $post->post_texto ?></textarea>
                <hr>
            </div>

            <div class="col-md-6 ">
                <h3 class="titulo">Ativar / Desativar</h3>
                <input type="checkbox" name="post_ativo" <?= $post->post_ativo == 1 ? 'checked' : '' ?> class="ativar">
                <small>Selecionepara mostrar a postagem no site</small>
            </div>

            <div class="col-md-6 ">
                <h3 class="titulo">Rascunho ?</h3>
                <input type="checkbox" name="post_rascunho" <?= $post->post_rascunho == 1 ? 'checked' : '' ?> class="ativar">
                <small>Selecionepara deixar a postagem no RASCUNHO</small>
            </div>

            <div class="col-md-12 ">
                <hr>
                <h3 class="titulo">Imagem <small>(Largura 1080px X Altura 624px)</small></h3>
                <input type="file" name="imagem" id="img_post" class="form-control mb-3">
            </div>
            <div class="col-md-6">
                <div id="preview">
                    <img src="<?= getImg($post->post_img, 'postagens') ?>" alt="Imagem upload preview do recorte" class="img-recorte" width="270px">
                </div>
            </div>
        </div>
        <div class="panel-footer clearfix">
            <button type="button" class="btn btn-danger" id="btn-recortar-imagem" data-toggle="modal" data-target="#myModal">
                Ajustar imagem
            </button>
            <input type="hidden" name="input_x1" id="input_x1">
            <input type="hidden" name="input_y1" id="input_y1">
            <input type="hidden" name="input_x2" id="input_x2">
            <input type="hidden" name="input_y2" id="input_y2">
            <input type="hidden" name="input_w" id="input_w">
            <input type="hidden" name="input_h" id="input_h">
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">SEO</div>

        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="titulo apagaKey">Palavras Chaves</h3>
            </div>
            <div class="col-md-12  mb-1" id="keywordPrincipal">
                <input type="text" id="keywords" value="<?= $post->keywords ?>" name="keywords" class="form-control  mb-1" placeholder="Separar por vírgula">
                <small id="listKeywords">
                    <?php foreach (explode(",", $post->keywords) as $keyword) : ?>
                        <span class="itemKeyword" data-value="<?= $keyword ?>"><?= $keyword ?></span>
                    <?php endforeach ?>
                </small>
            </div>
            <div class="col-md-12  mb-1">
                <hr>
                <h3 class="titulo">Descrição</h3>
                <textarea name="resumo" id="description" rows="6" class="form-control editor  mb-1"><?= $post->post_resumo ?></textarea>
                <small id="numberCaracters"><b><?=strlen(strip_tags(trim($post->post_resumo)));?> caractere. O ideal é 160 caracteres</b></small>
                <small id="verificarDescripition" class="right">Verificar</small>
                <div class="clearfix" id="listKeywordInDescription"></div>
            </div>
        </div>
        <div class="panel-footer clearfix">
            <a href="<?= base_url('painel/post') ?>" class="btn btn-danger left">Voltar para lista</a>
            <input type="submit" name="action" value="Editar postagem" class="btn btn-primary right">
        </div>
    </div>
</form>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajustar imagem para aparecer na capa da matéria (Recomendado Largura 1080px x Altura 624px)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4" id="result">
                <img src="<?= getImg($post->post_img, 'postagens') ?>" alt="Imagem upload " id="imagem" class="img-recorte">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Finalizar Alteração</button>
            </div>
        </div>
    </div>
</div>
<?= $this->include('painel/footer') ?>
<script src="<?= base_url('public/painel/recort/js/jquery.Jcrop.min.js') ?>"></script>