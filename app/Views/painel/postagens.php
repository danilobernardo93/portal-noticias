<?= $this->include('painel/header') ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Matérias</h1>
    </div>
</div>
<div class="row">
    <?php if ($postagens) : ?>
        <ul class="list-group">
            <?php foreach ($postagens as $post) : ?>
                <li class="list-group-item clearfix">
                    <div class="left titulo-post ">
                        <?= $post->post_titulo ?>
                    </div>
                    <div class="right info-post">
                        <div class="data-post">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            <smal class=""><?= formataDta($post->post_created_at, "%d de %b, %Y") ?></smal>
                        </div>
                        <div class="btn-post clearfix">
                            <a class="icone-edit" href="<?= base_url('painel/post/' . $post->post_id) ?>"><i class="fa fa-edit "></i></a>
                            <a class="icone-del" onclick="deletarPost(<?= $post->post_id ?>,'<?= ($post->post_titulo) ?>')" href="javascript:void(0)"><i class="fa fa-trash "></i></a>
                        </div>

                    </div>
                </li>
            <?php endforeach ?>
        </ul>
        <hr>
    <?php else : ?>
        <h4 style="margin-top: 70vh;text-align:center">Você ainda não possui nenhuma matéria</h4>
    <?php endif ?>
    <?= paginacao($numPages, $pageCurrent, base_url('painel/post/page')) ?>
</div>
<?= $this->include('painel/footer') ?>