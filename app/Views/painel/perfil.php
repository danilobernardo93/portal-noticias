<?= $this->include('painel/header') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $title ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= !empty($msg) ? '<div class="alert alert-danger" role="alert">' . $msg . '</div>' : '' ?>
    </div>
    <form method="POST" enctype="multipart/form-data">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12 ">
                    <h4 class="titulo">Nome</h4>
                    <input type="text" name="user_nome" value="<?= $user->user_nome ?>" class="form-control" required>
                </div>
                <div class="col-md-12 ">
                    <h4 class="titulo">Coluna</h4>
                    <input type="text" name="user_coluna" value="<?= $user->user_coluna ?>" class="form-control" required>
                </div>

                <div class="col-md-12 ">
                    <h4 class="titulo">Texto</h4>
                    <textarea name="user_texto" rows="10" class="form-control"><?= $user->user_texto ?></textarea>
                </div>
                <div class="col-md-12 ">
                    <h4 class="titulo">Email</h4>
                    <input type="text" name="user_email" value="<?= $user->user_email ?>" class="form-control" required>
                </div>
                <div class="col-md-12 ">
                    <h4 class="titulo">Senha</h4>
                    <input type="password" name="user_senha" placeholder="Apenas se for mudar a senha" class="form-control">
                    <input type="hidden" name="user_ativo" value='1'>
                </div>

            </div>
        </div>
        <div class="col-md-6 ">
            <h4 class="titulo">Foto de perfil <small>(512px x 512px) ou proporcional</small></h4>
            <input type="file" name="user_foto" id="img_user" class="form-control" />
            <img src="<?= base_url('public/portal/img/usuarios/' . $user->user_foto) ?>" id="previewIMG" width="100%">
        </div>

        <div class="col-md-12 mb-3 clearfix">
            <hr>
            <input type="submit" name="action" value="Editar perfil" class="btn btn-primary right">
        </div>

    </form>
</div>
<?= $this->include('painel/footer') ?>