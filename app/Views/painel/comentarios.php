<?= $this->include('painel/header') ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Lista de comentarios</h1>
    </div>
</div>
<div class="row">
    <ul class="list-group">
        <?php foreach ($comentarios as $linha) : ?>
            <li class="list-group-item clearfix">
                <div class="left titulo-comentario">
                    <?=substr($linha->comentario_texto,0,120)?>
                </div>
                <div class="right info-comentario">
                    <div class="data-comentario">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <smal class=""><?= formataDta($linha->created_at, "%d de %b, %Y") ?></smal>
                    </div>
                    <div class="btn-comentario clearfix">
                        <?=$linha->comentario_ativo==1?'<i class="fa fa-eye icone-views" title="Comentário ativo"></i>':'<i class="fa fa-eye-slash icone-views" title="Comentário desativado"></i>';?>
                        <a class="icone-edit" href="<?= base_url('painel/comentario/' . $linha->comentario_id) ?>"><i class="fa fa-edit "></i></a>
                        <!-- <a class="icone-del" onclick="deletarcomentario()" href="javascript:void(0)"><i class="fa fa-trash "></i></a> -->
                    </div>
                </div>
            </li>
        <?php endforeach ?>
    </ul>
    <hr>
    <?= paginacao($numPages, $pageCurrent,base_url('painel/comentario/page')) ?>
</div>
<?= $this->include('painel/footer') ?>