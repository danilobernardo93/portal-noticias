<?= $this->include('painel/header') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $title ?></h1>
    </div>
</div>

<div class="row">
    <form method="POST" enctype="multipart/form-data">
        <div class="col-md-4 ">
            <h4 class="titulo">Nome</h4>
            <input type="text" name="user_nome" class="form-control" required>
        </div>

        <div class="col-md-4 ">
            <h4 class="titulo">Coluna</h4>
            <input type="text" name="user_coluna" class="form-control" required >
        </div>
        <div class="col-md-4  clearfix">
            <h4 class="titulo">Tipo de usuário</h4>
            <select class="form-control" name="user_nivel">
                <?php foreach ($niveisAcesso as $linha) : ?>
                    <option value="<?= $linha->nivel_id ?>"><?= $linha->nivel_nome ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-4 ">

            <h4 class="titulo">Email</h4>
            <input type="text" name="user_email" class="form-control" required>
        </div>
        <div class="col-md-4 ">
            <h4 class="titulo">Senha</h4>
            <input type="password" name="user_senha" class="form-control" required>
        </div>
        <div class="col-md-4  clearfix">
            <h4 class="titulo">Ativo / Desativar</h4>
            <input type="checkbox" name="user_ativo">
        </div>

        <div class="col-md-12 ">
            <hr>
            <h4 class="titulo">Texto</h4>
            <textarea name="user_texto" class="form-control" required></textarea>
        </div>

        <div class="col-md-12 mb-3 clearfix">
            <hr>
            <input type="submit" name="action" value="Adicionar usuário" class="btn btn-primary right">
        </div>

    </form>
</div>
<?= $this->include('painel/footer') ?>