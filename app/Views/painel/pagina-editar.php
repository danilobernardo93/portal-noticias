<?= $this->include('painel/header') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $titulo ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <form method="POST" enctype="multipart/form-data">
            <?php
            $pagina = json_decode($pagina->conteudo);
            foreach ($pagina as $key => $value) {
                echo setCampos($key, $value);
            }
            ?>

            <input type="submit" class="form-control btn btn-primary" name="action" value="Salvar">

        </form>
    </div>
</div>
<?= $this->include('painel/footer') ?>