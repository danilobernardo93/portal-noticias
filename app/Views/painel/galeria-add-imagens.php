<?= $this->include('painel/header') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $title ?></h1>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <input type="text" name="titulo" required="required" class="form-control mb-2" placeholder="titulo da imagem">
                <label><small>Extensões permitidas: 'png' , 'jpg' , 'jpeg' , 'webp'</small></label>
                <input type="file" required="required" accept=".jpg, .jpeg, .png, .webp" class="form-control" name="imagens[]" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" name="action" value="Adicionar imagens">
            </div>
        </form>

        <ul class="list-group">
            <?php if (count($msg) > 0) : foreach ($msg as $linha) : ?>
                    <li class="list-group-item"><?= $linha ?></li>
            <?php endforeach;
            endif ?>
        </ul>
    </div>
</div>

<div class="row">
    <?php foreach ($imagens as $linha) : ?>
        <div class="col-md-3 col-sm-3">
            <input type="text" value="<?= base_url('public/portal/img/galeria-imagens/' . $linha->imagem_nome) ?>" id="linkIMG-<?= $linha->imagem_id ?>" style="opacity: 0;" />
            <i class="fa fa-eye btnCopiar" onclick="copiarLinkIMG('linkIMG-<?=$linha->imagem_id?>')" ></i>
            <i class="fa fa-trash btnApagar" onclick="deletaIMG('<?= $linha->imagem_id ?>','<?= $linha->imagem_nome ?>')" aria-hidden="true"></i>
            <img src="<?= base_url('public/portal/img/galeria-imagens/' . $linha->imagem_nome) ?>" width="100%">
        </div>
    <?php endforeach; ?>

    <?= paginacao($numPages, $pageCurrent,base_url('painel/galeria-imagens')) ?>
</div>
<?= $this->include('painel/footer') ?>