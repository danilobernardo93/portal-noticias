<?= $this->include('painel/header') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Alterar categorias de destaque da página Inicial</h1>
    </div>
</div>

<div class="row">

    <form method="POST" enctype="multipart/form-data">
        <?php $i= 1;?>
        <?php foreach ($categoriasDestaque as $key => $value) : ?>
            <div class="col-md-3  clearfix text-center">
                <h4 class="titulo"><?=str_replace($key,"Categoria ".$i, $key)?></h4>
                <select name="<?=$key?>" class="form-control">
                    <?php foreach ($categorias as $linha) : ?>
                        <?= setSelected($categoriasDestaque->$key, $linha->categoria_id, $linha->categoria_nome) ?>
                    <?php endforeach ?>
                </select>
                <hr>
            </div>
        <?php $i++; endforeach ?>
        <div class="col-md-12 mb-3 clearfix text-center">
            <hr>
            <input type="submit" name="action" value="Atualizar Destaques" class="btn btn-primary right">
        </div>

    </form>
</div>

<?= $this->include('painel/footer') ?>