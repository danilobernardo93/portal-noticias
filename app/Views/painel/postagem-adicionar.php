<?= $this->include('painel/header') ?>
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->

<link rel="stylesheet" href="<?= base_url('public/painel/recort/css/jquery.Jcrop.css') ?>" type="text/css" />
<style>
    h1 {
        text-align: center;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Adicionar novo post</h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $msg ? $msg : ''; ?>
    </div>
</div>

<form method="POST" enctype="multipart/form-data">
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">Matéria</div>
        <div class="panel-body">
            <div class="col-md-8 ">
                <h3 class="titulo">Título</h3>
                <input type="text" name="post_titulo" class="form-control" required>
                <small id="valitationTitulo"></small>
            </div>
            <div class="col-md-4  clearfix">
                <h3 class="titulo">Categorias</h3>
                <select name="categoria" class="form-control">
                    <?php foreach ($categorias as $linha) : ?>
                        <option value="<?= $linha->categoria_id ?>"><?= $linha->categoria_nome ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <div class="col-md-12 ">
                <hr>
                <h3 class="titulo">Matéria</h3>
                <textarea name="conteudo" class="form-control editor-conteudo"></textarea>
                <hr>
            </div>

            <div class="col-md-6 ">
                <h3 class="titulo">Ativar / Desativar</h3>
                <input type="checkbox" name="post_ativo" class="ativar">
                <small>Selecione para mostrar a postagem no site</small>
            </div>

            <div class="col-md-6 ">
                <h3 class="titulo">Rascunho ?</h3>
                <input type="checkbox" name="post_rascunho" class="ativar">
                <small>Selecione para deixar a postagem no RASCUNHO</small>
            </div>

            <div class="col-md-12 ">
                <hr>
                <h3 class="titulo">Imagem <small>(Largura 1080px X Altura 624px)</small></h3>
                <input type="file" name="imagem" id="img_post" class="form-control mb-3">
            </div>
            <div class="col-md-6">
                <div id="preview">
                    <img src="" alt="Selecione uma imagem" class="img-recorte">
                </div>
            </div>
        </div>
        <div class="panel-footer clearfix">
            <button type="button" class="btn btn-danger" id="btn-recortar-imagem" data-toggle="modal" data-target="#myModal">
                Ajustar imagem
            </button>
            <input type="hidden" name="input_x1" id="input_x1">
            <input type="hidden" name="input_y1" id="input_y1">
            <input type="hidden" name="input_x2" id="input_x2">
            <input type="hidden" name="input_y2" id="input_y2">
            <input type="hidden" name="input_w" id="input_w">
            <input type="hidden" name="input_h" id="input_h">
        </div>
    </div>

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">SEO</div>

        <div class="panel-body">
            <div class="col-md-12">
                <h3 class="titulo apagaKey">Palavras Chaves</h3>
            </div>
            <div class="col-md-12  mb-1" id="keywordPrincipal">
                <input type="text" id="keywords" name="keywords" class="form-control  mb-1" placeholder="Separar por vírgula">
                <small id="listKeywords"></small>
            </div>
            <div class="col-md-12 ">
                <hr>
                <h3 class="titulo">Descrição</h3>
                <textarea name="resumo" id="description" rows="6" class="form-control editor mb-1"></textarea>
                <small id="numberCaracters"><b>0 caractere. O ideal é 160 caracteres</b></small>
                <small id="verificarDescripition" class="right">Verificar</small>
                <div class="clearfix" id="listKeywordInDescription"></div>
            </div>
        </div>
        <div class="panel-footer clearfix">
            <input type="submit" name="action" value="Adicionar postagem" class="btn btn-primary right">
        </div>
    </div>
</form>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajustar imagem para aparecer na capa da matéria (Recomendado Largura 1080px x Altura 624px)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4" id="result">
                <img src="" alt="Imagem upload " id="imagem" class="img-recorte">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Finalizar Alteração</button>
            </div>
        </div>
    </div>
</div>

<?= $this->include('painel/footer') ?>

<script src="<?= base_url('public/painel/recort/js/jquery.Jcrop.min.js') ?>"></script>