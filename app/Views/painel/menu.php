<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <ul class="nav navbar-nav navbar-left navbar-top-links">
        <li><a href="#"><i class="fa fa-home fa-fw"></i> Dashoboard</a></li>
    </ul>

    <ul class="nav navbar-right navbar-top-links">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <?= $_SESSION['user']['user_nome'] ?> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                    <a href="<?= base_url('painel/perfil') ?>"><i class="fa fa-user fa-fw"></i> Perfil</a>
                </li>
            </ul>
        </li>
        <li class="divider"></li>
        <li>
            <a href="<?= base_url('logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
        </li>
    </ul>
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="<?= base_url('painel/') ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>


                <li>
                    <a href="#"><i class="fa fa-file-text fa-fw"></i> Postagens<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?= base_url('painel/post/novo') ?>">Nova postagem</a>
                        </li>
                        <li>
                            <a href="<?= base_url('painel/post') ?>">Lista de Postagens</a>
                        </li>
                        <li>
                            <a href="<?= base_url('painel/post/rascunho') ?>">Lista de Rascunhos</a>
                        </li>
                        <?php if (getNivelAcesso() == 1) : ?>
                            <li>
                                <a href="<?= base_url('painel/categoria') ?>">Categorias</a>
                            </li>
                        <?php endif ?>
                    </ul>
                </li>

                <?php if (getNivelAcesso() == 1) : ?>
                    <li>
                        <a href="<?= base_url('painel/categoria/destaque-home') ?>">Categorias de destaque da Página Inicial</a>
                    </li>
                <?php endif ?>
                <li>
                    <a href="<?= base_url('painel/galeria-imagens') ?>">Upload de Imagens</a>
                </li>



                <?php if (getNivelAcesso() == 1) : ?>
                    <li>
                        <a href="<?= base_url('painel/comentario') ?>"><i class="fa fa-comments fa-fw"></i> Comentários</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-users fa-fw"></i> Usuários<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?= base_url('painel/usuario/novo') ?>">Novo usuário</a>
                            </li>
                            <li>
                                <a href="<?= base_url('painel/usuario') ?>">Lista de usuários</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?= base_url('painel/perfil') ?>"><i class="fa fa-user fa-fw"></i> Perfil</a>
                    </li>

                    <li>
                        <a href="<?= base_url('painel/revista') ?>"><i class="fa fa-address-book"></i> Capa da Revista</a>
                    </li>


                    <li>
                        <a href="#"><i class="fa fa-archive fa-fw"></i> Páginas<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php foreach ($paginas as $linha) : ?>
                                <li>
                                    <a href="<?= base_url('painel/conteudo/pagina/' . $linha->pagina) ?>"><?= str_replace("-", " ", $linha->pagina) ?></a>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</nav>

<div id="page-wrapper">
    <div class="container-fluid">