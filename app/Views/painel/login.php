<?= $this->include('painel/header') ?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Painel de controler</h3>
            </div>
            <div class="panel-body">
                <form method="post">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Senha" name="senha" type="password" value="">
                        </div>
                        <div class="checkbox">
                            <label><?php echo($msg) ?></label>
                        </div>
                        <button type="submit" class="btn btn-lg btn-success btn-block">Login</butto>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<?= $this->include('painel/footer') ?>