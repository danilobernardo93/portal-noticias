<?= $this->include('painel/header') ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar comentario</h1>
    </div>
</div>

<div class="row">
    <form method="POST" enctype="multipart/form-data">
    <div class="col-md-12 ">
            <h3 class="titulo">Nome</h3>
            <input type="text" name="comentario_nome" value="<?= $comentario->comentario_nome ?>" class="form-control" required>
        </div>

        <div class="col-md-12 ">
            <h3 class="titulo">E-mail</h3>
            <input type="text" name="comentario_email" value="<?= $comentario->comentario_email ?>" class="form-control" required>
        </div>

        <div class="col-md-12 ">
            <h3 class="titulo">Assunto</h3>
            <input type="text" name="comentario_assunto" value="<?= $comentario->comentario_assunto ?>" class="form-control" required>
        </div>

        <div class="col-md-12 ">
            <hr>
            <h3 class="titulo">Comentário</h3>
            <textarea name="comentario_texto" rows='6' class="form-control"><?= $comentario->comentario_texto ?></textarea>
        </div>

        <div class="col-md-12 ">
            <hr>
            <h3 class="titulo">Ativar / Desativar</h3>
            <input type="checkbox" name="comentario_ativo" <?=$comentario->comentario_ativo ==1 ? 'checked' : ''?> class="ativar">
            <small>Selecione o checkbox a esquerda para mostrar o comentário no site</small>
        </div>

        

        <div class="col-md-12 mb-3 clearfix">
            <hr>
            <a href="<?=base_url('painel/comentario')?>" class="btn btn-danger left">Voltar para lista</a>
            <input type="submit" name="action" value="Editar comentário" class="btn btn-primary right">
        </div>

    </form>
</div>
<?= $this->include('painel/footer') ?>