<?= $this->include('painel/header') ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Categorias (ADD, EDIT)</h1>
    </div>
</div>
<div class="row">

    <form method="POST">
        <div class="col-md-12 ">
            <h3 class="titulo">Nome da categoria</h3>
        </div>
        <div class="col-sm-8 col-xs-8 ">
            <input type="text" name="categoria_nome" class="form-control" required>
        </div>
        <div class="col-sm-4 col-xs-4 ">
            <input type="submit" name="action" value="Salvar" class="btn btn-primary">
        </div>

    </form>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Categoria</th>
                    <th scope="col">URI</th>
                    <th scope="col">Aparece no menu</th>
                    <th scope="col">Aparece no rodape</th>
                    <th scope="col">Atualizar</th>
                    <th scope="col">Apagar</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($categorias as $linha) : ?>
                    <tr>
                        <td><input value="<?= $linha->categoria_nome ?>" class="form-control " id="nome-categoria-<?= $linha->categoria_id ?>"></td>
                        <td><input value="<?= $linha->categoria_uri ?>" class="form-control" id="uri-categoria-<?= $linha->categoria_id ?>"></td>
                        <td><input type="checkbox" onclick="atualizarCategoria(<?= $linha->categoria_id ?>)" <?=$linha->menu==1? 'checked' : ''?> id="menu-categoria-<?= $linha->categoria_id ?>"></td>
                        <td><input type="checkbox" onclick="atualizarCategoria(<?= $linha->categoria_id ?>)" <?=$linha->rodape==1? 'checked' : ''?> id="rodape-categoria-<?= $linha->categoria_id ?>"></td>
                        <td><i class="fa fa-floppy-o btn-salvar" onclick="atualizarCategoria(<?= $linha->categoria_id ?>)" aria-hidden="true"></i></td>
                        <td><i class="fa fa-trash btn-apagar" onclick="deletarCategoria(<?= $linha->categoria_id ?>)" aria-hidden="true"></i></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<?= $this->include('painel/footer') ?>