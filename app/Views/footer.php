<div id="ad-tag-sticky" style="display:none; width:auto;height:auto;position: fixed;bottom: 0;left: 50%;transform: translate(-50%);z-index: 9999998;cursor: pointer;">
    <button onclick="javascript:document.getElementById('ad-tag-sticky').style.display = 'none'" style="position: absolute; top: -24px; right: 0; height: 25px; width: 25px; cursor: pointer; z-index: 9999999999; background: #fff; border: 1px solid #ccc; padding: 0 0 0 1px; color: #666;">X</button>
    <div id="stickyAdR7">
        <script>
            window.googletag = window.googletag || {
                cmd: []
            };
            googletag.cmd.push(function() {
                googletag.pubads().addEventListener('slotRenderEnded', function(event) {
                    var div = document.getElementById(event.slot.getSlotElementId()).parentNode;
                    if (!event.isEmpty && event.slotContentChanged && div.id == "stickyAdR7") {
                        div = div.parentNode;
                        div.style.width = event.size[0] + 'px';
                        div.style.height = event.size[1] + 'px';
                        div.style.display = 'block';
                    }
                });
            });
            (function() {
                var ie = /msie/i.test(navigator.userAgent),
                    ieBox = ie && (document.compatMode == null || document.compatMode == "BackCompat"),
                    canvasEl = ieBox ? document.body : document.documentElement,
                    wd = window.innerWidth || canvasEl.clientWidth,
                    sz = "";
                if (wd < 710) {
                    sz = '[[320,50],[300,50]]';
                    dp = 'mobilefixo';
                } else if (wd < 960) {
                    sz = '[[728,90]]';
                } else {
                    sz = '[[970,90],[728,90]]';
                    dp = 'sticky_ad';
                }
                document.write('<scr' + 'ipt data-sizes="' + sz + '" data-vendor="r7.com" data-pos="' + dp + '" data-context="principal" src="https://sc.r7.com/r7/js/adPartner.min.js"></scr' + 'ipt>');
            })();
        </script>
    </div>
</div>
<footer class="page-footer font-small indigo bg-4">
    <img src="<?= base_url('public/portal/img/triangulo-branco.png') ?>" class="triangulo-rodape " />
    <!-- Footer Links -->
    <div class="container text-center text-md-left">
        <div class="row">
            <div class="col-md-3 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Sobre</h5>
                <?= $sobreRodape->text_sobre ?>
                <img src="<?= base_url('public/portal/img/logotipo.png') ?>" class="w-100" style="display: none" />
            </div>

            <hr class="clearfix w-100 d-md-none">

            <div class="col-md-3 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Principais Assuntos</h5>
                <ul class="lista-rodape">
                    <?php foreach ($menu['rodape'] as $linha) : ?>
                        <li>
                            <a href="<?= base_url('categoria/' . $linha->categoria_uri) ?>"><?= $linha->categoria_nome ?></a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>

            <hr class="clearfix w-100 d-md-none">

            <div class="col-md-3 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Últimos Posts</h5>
                <ul class="list-unstyled">
                    <?php $i = 1;
                    foreach ($rescentes as $linha) : ?>
                        <?php if ($i <= 3) : ?>
                            <li class="post-footer">
                                <a href="<?= base_url('post/' . $linha->post_uri) ?>"><?= $linha->post_titulo ?></a><br />
                                <?= formataDta($linha->post_updated_at, '%d de %b de %Y') ?>
                            </li>
                        <?php endif ?>
                    <?php $i++;
                    endforeach ?>
                </ul>
            </div>

            <hr class="clearfix w-100 d-md-none">

            <div class="col-md-3 mx-auto">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Instagram</h5>

                <div class="img-insta clearfix" id="instagram-feed1"></div>
                <div class="rede-social clearfix">
                    <?php
                    foreach ($redesSociais as $key => $value) :
                        $key = explode('_', $key);
                        $rede = $key[1];
                    ?>
                        <a href="<?= $value ?>" class="preto" target="_blank"><i class="fa fa-<?= $rede ?> "></i></a>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright  py-3" style="margin-bottom: -25px">
        <div class="container">
            © <?= date("Y") ?> Todos os direitos reservados
            <a style="color:#fff;float: right;font-size: 15px;" target="_blank" href="https://rifaslocal.com.br/?codIndicacao=1">Desenvolvido por: <b>Danilo Augusto</b></a>
        </div>
    </div>
</footer>
<!-- TailTarget Tag Manager TT-9964-3/CT-23 -->
<script>
    (function(i) {
        var ts = document.createElement('script');
        ts.type = 'text/javascript';
        ts.async = true;
        ts.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'tags.t.tailtarget.com/t3m.js?i=' + i;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ts, s);
    })('TT-9964-3/CT-23');
</script>
<!-- End TailTarget Tag Manager -->
</body>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="<?= base_url('public/assets/dist/js/bootstrap.bundle.js') ?>"></script>
<script src="<?= base_url('public/assets/js/main.js') ?>"></script>
<?php foreach ($links['js'] as $link) : ?>
    <script src="<?= base_url($link) ?>"></script>
<?php endforeach; ?>


<script src="https://www.sowecms.com/demos/statics/prism.js"></script>
<script src="<?= base_url('public/assets/js/jquery.instagramFeed.min.js') ?>"></script>

<script>
    $.instagramFeed({
        'username': 'lorena.r7',
        'container': "#instagram-feed1",
        'display_profile': true,
        'display_biography': true,
        'display_gallery': true,
        'callback': null,
        'styling': true,
        'items': 6,
        'items_per_row': 3,
        'margin': 1,
        'lazy_load': true,
        'on_error': console.error
    });
</script>

</html>