<?= $this->include('header') ?>
<div class="containnerr">
    <div class="container" id="bannerGrande_R7_1">
        <div class="row ">
            <div class="col-12">

            </div>
        </div>
    </div>
</div>
<div class="container mt-5">
    <div class="row mt-5 grupo-3">
        <div class="col-md-4 center">
            <div class="img-colunista2" ><img src="<?= empty($colunista->user_foto) ? base_url('public/portal/img/usuarios/perfil.png') : base_url('public/portal/img/usuarios/' . $colunista->user_foto) ?> " /></div>
        </div>
        <div class="col-md-8">
            <h1><?= $colunista->user_nome ?></h1>
            <div class="sobre"><?= $colunista->user_texto ?></div>
        </div>
        <div class="col-md-12 mt-4">
            <hr>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-xl-9 col-md-12">
            <div class="row">
                <?php foreach ($posts as $linha) : ?>
                    <div class="col-md-3 col-xl-2">
                        <div  class="img-colunista2"><img src="<?= empty($colunista->user_foto) ? base_url('public/portal/img/usuarios/perfil.png') : base_url('public/portal/img/usuarios/' . $colunista->user_foto) ?> " /></div>
                        <label class="nomeColunista"><?= $colunista->user_nome ?></label>
                        <div class="data"><?= formataDta($linha->post_created_at, '%d/%b/%Y %H:%m') ?></div>
                        <label class="labelAtualiza"><b>Atualizado</b></label>
                        <div class="data"><?= formataDta($linha->post_updated_at, '%d/%b/%Y %H:%m') ?></div>
                        
                    </div>
                    <div class="col-md-5 col-xl-4">
                        <img class="w-100" src="<?= base_url('public/portal/img/postagens/' . $linha->post_img) ?>" title="<?= $linha->post_titulo ?>" alt="<?= $linha->post_titulo ?>" />
                        <a href="<?= base_url('categoria/' . $linha->categoria_uri) ?>">
                            <div class="categoria bg-2"><?= $linha->categoria_nome ?></div>
                        </a>
                    </div>
                    <div class="col-md-4 col-xl-6">
                        <h3 class="titulo-1-post"><?= $linha->post_titulo ?></h3>
                        <div class="btn-ler-materia"><a href="<?= base_url('post/' . $linha->post_uri) ?>">LER MATÉRIA <i class="fa fa-caret-right" aria-hidden="true"></i></a></div>
                        <div class="comentario "><?= $linha->comentarios ?> comentários</div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>

    <div class="row paginacao">
        <hr>
        <?= paginacao($numPages, $pageCurrent, base_url('colunista/' . $colunista->user_uri . '/')) ?>
    </div>
</div>
<?= $this->include('footer') ?>