<?= $this->include('header') ?>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item banner-home  active" style="background-image: url(<?= base_url('public/portal/img/banner-4.jpg') ?>);"></div>
        <div class="carousel-item banner-home  " style="background-image: url(<?= base_url('public/portal/img/banner-3.jpg') ?>);"></div>
        <div class="carousel-item banner-home  " style="background-image: url(<?= base_url('public/portal/img/banner-2.jpg') ?>);"></div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<div class="containnerr">
    <div class="container" id="bannerGrande_R7_1">
        <div class="row ">
            <div class="col-12">

            </div>
        </div>
    </div>
</div>

<?= $this->include('incl/destaque-2'); ?>


<div class="containnerr listra">
    <div class="container ">
        <?= $this->include('incl/grupo1-grupo2') ?>
    </div>
</div>

<div class="containnerr">
    <div class="container">
        <div class="row ">
            <div class="col-12">
                <div id="bannerCelularR7_3"></div>
            </div>
        </div>
    </div>
</div>

<div class="containnerr listra">
    <div class="container ">
        <?= $this->include('incl/grupo3') ?>
    </div>
</div>

<div class="containnerr">
    <div class="container">
        <?= $this->include('incl/mais-lidas') ?>
    </div>
</div>
<div class="containnerr">
    <div class="container">
        <div class="row ">
            <div class="col-12">
                <div id="bannerCelularR7_4"></div>
            </div>
        </div>
    </div>
</div>
<div class="containnerr listra">
    <div class="container">
        <div class="row grupo-5">
            <div class="col-sx-12 col-sm-12 col-md-10 col-lg-8 offset-md-1 offset-lg-2">
                <div class="sobre clearfix">
                    <div class="img-sobre clearfix">
                        <img src="<?= base_url('public/portal/img/conteudo/'.$sobre->file_imgem_sobre_a_lorena) ?>" class="img-lorena" />
                    </div>
                    <div class="txt-sobre clearfix">
                        <h3 class="titulo-sobre"><?= $sobre->input_titulo ?></h3>
                        <div class="rede-social mb-3 mt-3">
                            <?php
                            foreach ($redesSociais as $key => $value) :
                                $key = explode('_', $key);
                                $rede = $key[1];
                            ?>
                                <a href="<?= $value ?>" class="preto" target="_blank"><i class="fa fa-<?= $rede ?> "></i></a>
                            <?php endforeach ?>
                        </div>
                        <?= $sobre->text_sobre ?><br />
                        <small class=" text-center"><a href="<?= base_url('sobre') ?>" class="ler-mais">Ler mais</a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="containnerr listra">
    <?= $this->include('incl/news') ?>
</div>


<?= $this->include('footer') ?>