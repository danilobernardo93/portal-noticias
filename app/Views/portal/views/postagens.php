<?= $this->include('portal/views/header') ?>
<div class="main-content--section pbottom--30">
    <div class="container">
        <div class="row">
            <!-- Main Content Start -->
            <div class="main--content col-md-8 col-sm-7" data-sticky-content="true">
                <div class="sticky-content-inner">
                    <div class="post--items post--items-5 pd--30-0">
                        <ul class="nav">
                            <?php foreach ($posts as $linha) : ?>
                                <?php
                                $nameCat = $linha->subcategoria_nome;
                                $urlLink = 'subcategoria/' . $linha->subcategoria_uri . '/';
                                if (isset($categoria->categoria_uri)) {
                                    $urlLink = 'categoria/' . $linha->categoria_uri . '/';
                                    $nameCat = $linha->categoria_nome;
                                }
                                ?>
                                <li>
                                    <div class="post--item post--title-larger">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12 col-xs-4 col-xxs-12">
                                                <div class="post--img">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>
                                                    <a href="<?= base_url($urlLink) ?>" class="cat"><?= $nameCat ?></a>
                                                </div>
                                            </div>

                                            <div class="col-md-8 col-sm-12 col-xs-8 col-xxs-12">
                                                <div class="post--info">
                                                    <ul class="nav meta">
                                                        <?php $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->created_at : $linha->post_updated_at; ?>
                                                        <li><a href="javascript:void(0)"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                        <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                    </ul>

                                                    <div class="title">
                                                        <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= $linha->post_titulo ?></a></h3>
                                                    </div>
                                                </div>

                                                <div class="post--content">
                                                    <p><?= $linha->post_resumo ?></p>
                                                </div>

                                                <div class="post--action">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>">Continuar lendo...</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>

                    <div class="pagination--wrapper clearfix bdtop--1 bd--color-2 ptop--60 pbottom--30">
                        <p class="pagination-hint float--left">Pagina <?= $pageCurrent ?> de <?= $numPages == 0 ? $numPages + 1 : $numPages ?></p>
                        <?php $urlPage = isset($categoria->categoria_uri) ? 'categoria/' . $categoria->categoria_uri : 'subcategoria/' . $subcategoria->subcategoria_uri . '/'; ?>
                        <?= paginacao($numPages, $pageCurrent, base_url($urlPage)) ?>
                    </div>
                </div>
            </div>
            <?= $this->include('portal/views/sidebar-main') ?>
        </div>
    </div>
</div>
<?= $this->include('portal/views/footer') ?>