<?= $this->include('portal/views/header') ?>

<div class="main-content--section pbottom--30">
    <div class="container">
        <div class="main--content">
            <div class="post--items post--items-1 pd--30-0">
                <div class="row gutter--15">
                <?php if($rescentes):?>
                    <div class="col-md-6">
                        <div class="post--item post--layout-1 post--title-larger" style="margin-top:12px">
                            <div class="post--img">
                                <a href="<?= base_url($rescentes[0]->categoria_uri.'/'.$rescentes[0]->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $rescentes[0]->post_img) ?>" alt=""></a>
                                <a href="<?= base_url('categoria/' . $rescentes[0]->categoria_uri) ?>" class="cat"><?= $rescentes[0]->categoria_nome ?></a>

                                <div class="post--info">
                                    <ul class="nav meta">
                                        <?php $data = $rescentes[0]->post_updated_at == '0000-00-00 00:00:00' ? $rescentes[0]->post_created_at : $rescentes[0]->post_updated_at; ?>
                                        <li><a href="<?= base_url('colunista/' . $rescentes[0]->user_uri) ?>"><?= explode(" ", $rescentes[0]->user_nome)[0] ?></a></li>
                                        <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                    </ul>

                                    <div class="title">
                                        <h2 class="h4"><a href="<?= base_url($rescentes[0]->categoria_uri.'/'.$rescentes[0]->post_uri) ?>" class="btn-link"><?= $rescentes[0]->post_titulo ?></a></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif?>

                    <div class="col-md-6">
                        <div class="row gutter--15">
                            <?php $i = 0;
                            $ii = 1 ?>
                            <?php foreach ($rescentes as $linha) : ?>
                                <?php if ($i > 0) : ?>
                                    <div class="col-xs-6 col-xss-12">
                                        <div class="post--item post--layout-1 post--title-large " style="margin-top:10px">
                                            <div class="post--img">
                                                <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>
                                                <a href="<?= base_url('categoria/' . $linha->categoria_uri) ?>" class="cat"><?= $linha->categoria_nome ?></a>

                                                <div class="post--info">
                                                    <ul class="nav meta">
                                                        <?php $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->post_created_at : $linha->post_updated_at; ?>
                                                        <li><a href="<?= base_url('colunista/' . $linha->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                        <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                    </ul>

                                                    <div class="title">
                                                        <h2 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= word_limiter($linha->post_titulo, 10) ?></a></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $ii++;
                                endif ?>
                            <?php $i++;
                            endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Post Items End -->
        </div>
        <!-- Main Content End -->

        <div class="row">
            <!-- Main Content Start -->
            <div class="main--content col-md-8 col-sm-7" data-sticky-content="true">
                <div class="sticky-content-inner">
                    <div class="row">
                        <div class="col-md-6 ptop--30 pbottom--30">
                            <div class="post--items-title" data-ajax="tab">
                                <h2 class="h4"><?= $categoriaDestaque1 ? $categoriaDestaque1[0]->categoria_nome : ''; ?></h2>
                            </div>

                            <div class="post--items post--items-2" data-ajax-content="outer">
                                <ul class="nav row gutter--15" data-ajax-content="inner">
                                    <?php $i = 0;
                                    $coluna = 'col-xs-12';
                                    $layoutStyle = 'post--layout-1';

                                    foreach ($categoriaDestaque1 as $linha) :
                                        $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->post_created_at : $linha->post_updated_at;
                                        if ($i != 0) {
                                            $coluna = 'col-xs-6';
                                            $layoutStyle = 'post--layout-2';
                                        }
                                    ?>
                                        <li class="<?= $coluna ?>">
                                            <div class="post--item <?= $layoutStyle ?>">
                                                <div class="post--img">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>

                                                    <div class="post--info">
                                                        <ul class="nav meta">
                                                            <li><a href="<?= base_url('colunista/' . $linha->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                            <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                        </ul>

                                                        <div class="title">
                                                            <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= word_limiter($linha->post_titulo, 10) ?></a></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <?php if (($i % 2) == 0) : ?>
                                            <li class="col-xs-12">
                                                <hr class="divider">
                                            </li>
                                    <?php endif;
                                        $i++;
                                    endforeach; ?>
                                </ul>

                                <div class="preloader bg--color-0--b" data-preloader="1">
                                    <div class="preloader--inner"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 ptop--30 pbottom--30">
                            <div class="post--items-title" data-ajax="tab">
                                <h2 class="h4"><?= $categoriaDestaque2 ? $categoriaDestaque2[0]->categoria_nome : '';?></h2>
                            </div>

                            <div class="post--items post--items-3" data-ajax-content="outer">
                                <ul class="nav" data-ajax-content="inner">
                                    <?php
                                    $ii = 0;
                                    $layoutStyle = 'post--layout-1';

                                    foreach ($categoriaDestaque2 as $linha) :
                                        $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->post_created_at : $linha->post_updated_at;
                                        if ($ii != 0) {
                                            $layoutStyle = 'post--layout-3';
                                        }
                                    ?>
                                        <li>
                                            <div class="post--item <?= $layoutStyle ?>">
                                                <div class="post--img">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>

                                                    <div class="post--info">
                                                        <ul class="nav meta">
                                                            <li><a href="<?= base_url('colunista/' . $linha->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                            <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                        </ul>

                                                        <div class="title">
                                                            <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= word_limiter($linha->post_titulo, 13)  ?></a></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php
                                        $ii++;
                                    endforeach;
                                    ?>
                                </ul>

                                <!-- Preloader Start -->
                                <div class="preloader bg--color-0--b" data-preloader="1">
                                    <div class="preloader--inner"></div>
                                </div>
                                <!-- Preloader End -->
                            </div>
                        </div>

                        <div class="col-md-12 ptop--30 pbottom--30">
                            <div class="ad--space">
                                <a href="#">
                                    <img src="<?= $propagandas['body01']['image'] ?>" alt="<?= $propagandas['body01']['title'] ?>" class="center-block">
                                </a>
                            </div>
                        </div>

                        <div class="col-md-12 ptop--30 pbottom--30">
                            <!-- Post Items Title Start -->
                            <div class="post--items-title" data-ajax="tab">
                                <h2 class="h4"><?= $categoriaDestaque3 ? $categoriaDestaque3[0]->categoria_nome : '';?></h2>
                            </div>

                            <div class="post--items post--items-2" data-ajax-content="outer">
                                <ul class="nav row" data-ajax-content="inner">
                                <?php if($categoriaDestaque3):?>
                                    <li class="col-md-6">
                                        <div class="post--item post--layout-2">
                                            <div class="post--img">
                                                <a href="<?= base_url($categoriaDestaque3[0]->user_uri.'/'.$categoriaDestaque3[0]->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $categoriaDestaque3[0]->post_img) ?>" alt=""></a>
                                                <a href="<?= base_url('categoria/' . $categoriaDestaque3[0]->categoria_uri) ?>" class="cat"><?= $categoriaDestaque3[0]->categoria_nome ?></a>
                                                <a href="#" class="icon"><i class="fa fa-star-o"></i></a>

                                                <div class="post--info">
                                                    <ul class="nav meta">
                                                        <li><a href="<?= base_url('colunista/' . $categoriaDestaque3[0]->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                        <li><a href="#"><?= formataDta($categoriaDestaque3[0]->post_updated_at, '%d/%m/%Y') ?></a></li>
                                                    </ul>

                                                    <div class="title">
                                                        <h3 class="h4"><a href="<?= base_url($categoriaDestaque3[0]->user_uri.'/'.$categoriaDestaque3[0]->post_uri) ?>" class="btn-link"><?= $categoriaDestaque3[0]->post_titulo ?></a></h3>
                                                    </div>
                                                    <p><?= $categoriaDestaque3[0]->post_resumo ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php endif;?>
                                    <li class="col-md-6">
                                        <ul class="nav row">
                                            <li class="col-xs-12 hidden-md hidden-lg">
                                                <hr class="divider">
                                            </li>
                                            <?php $i = 0;
                                            foreach ($categoriaDestaque2 as $linha) : ?>
                                                <?php $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->post_created_at : $linha->post_updated_at; ?>
                                                <?php if ($i > 0) : ?>
                                                    <li class="col-xs-6">
                                                        <div class="post--item post--layout-2">
                                                            <div class="post--img">
                                                                <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>

                                                                <div class="post--info">
                                                                    <ul class="nav meta">
                                                                        <li><a href="<?= base_url('colunista/' . $linha->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                                        <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                                    </ul>

                                                                    <div class="title">
                                                                        <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= $linha->post_titulo ?></a></h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php if (($i % 2) == 0) : ?>
                                                        <li class="col-xs-12">
                                                            <!-- Divider Start -->
                                                            <hr class="divider">
                                                            <!-- Divider End -->
                                                        </li>
                                                    <?php endif ?>
                                                <?php endif ?>
                                            <?php $i++;
                                            endforeach; ?>
                                        </ul>
                                    </li>
                                </ul>

                                <!-- Preloader Start -->
                                <div class="preloader bg--color-0--b" data-preloader="1">
                                    <div class="preloader--inner"></div>
                                </div>
                                <!-- Preloader End -->
                            </div>
                        </div>

                        <div class="col-md-6 ptop--30 pbottom--30">
                            <div class="post--items-title" data-ajax="tab">
                                <h2 class="h4"><?= $categoriaDestaque4? $categoriaDestaque4[0]->categoria_nome : '';?></h2>
                            </div>

                            <div class="post--items post--items-2" data-ajax-content="outer">
                                <ul class="nav row gutter--15" data-ajax-content="inner">
                                    <?php $i = 0;
                                    $coluna = 'col-xs-12';
                                    $layoutStyle = 'post--layout-1';

                                    foreach ($categoriaDestaque4 as $linha) :
                                        $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->post_created_at : $linha->post_updated_at;
                                        if ($i != 0) {
                                            $coluna = 'col-xs-6';
                                            $layoutStyle = 'post--layout-2';
                                        }
                                    ?>
                                        <li class="<?= $coluna ?>">
                                            <div class="post--item <?= $layoutStyle ?>">
                                                <div class="post--img">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>

                                                    <div class="post--info">
                                                        <ul class="nav meta">
                                                            <li><a href="<?= base_url('colunista/' . $linha->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                            <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                        </ul>

                                                        <div class="title">
                                                            <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= word_limiter($linha->post_titulo, 10) ?></a></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <?php if (($i % 2) == 0) : ?>
                                            <li class="col-xs-12">
                                                <hr class="divider">
                                            </li>
                                    <?php endif;
                                        $i++;
                                    endforeach; ?>
                                </ul>

                                <div class="preloader bg--color-0--b" data-preloader="1">
                                    <div class="preloader--inner"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 ptop--30 pbottom--30">
                            <div class="post--items-title" data-ajax="tab">
                                <h2 class="h4"><?= $categoriaDestaque5 ? $categoriaDestaque5[0]->categoria_nome : ''; ?></h2>
                            </div>

                            <div class="post--items post--items-3" data-ajax-content="outer">
                                <ul class="nav" data-ajax-content="inner">
                                    <?php
                                    $ii = 0;
                                    $layoutStyle = 'post--layout-1';

                                    foreach ($categoriaDestaque5 as $linha) :
                                        $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->post_created_at : $linha->post_updated_at;
                                        if ($ii != 0) {
                                            $layoutStyle = 'post--layout-3';
                                        }
                                    ?>
                                        <li>
                                            <div class="post--item <?= $layoutStyle ?>">
                                                <div class="post--img">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>

                                                    <div class="post--info">
                                                        <ul class="nav meta">
                                                            <li><a href="<?= base_url('colunista/' . $linha->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                            <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                        </ul>

                                                        <div class="title">
                                                            <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= word_limiter($linha->post_titulo, 13)  ?></a></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php
                                        $ii++;
                                    endforeach;
                                    ?>
                                </ul>

                                <!-- Preloader Start -->
                                <div class="preloader bg--color-0--b" data-preloader="1">
                                    <div class="preloader--inner"></div>
                                </div>
                                <!-- Preloader End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->include('portal/views/sidebar-main') ?>
        </div>

        <!-- Advertisement Start -->
        <div class="ad--space pd--30-0">
            <a href="#">
                <img src="<?= $propagandas['body02']['image'] ?>" alt="<?= $propagandas['body02']['title'] ?>" class="center-block">
            </a>
        </div>
        <!-- Advertisement End -->

        <div class="row">
            <!-- Main Content Start -->
            <div class="main--content col-md-8 col-sm-7" data-sticky-content="true">
                <div class="sticky-content-inner">
                    <div class="row">
                        <div class="col-md-6 ptop--30 pbottom--30">
                            <div class="post--items-title" data-ajax="tab">
                                <h2 class="h4"><?= $categoriaDestaque6 ? $categoriaDestaque6[0]->categoria_nome : ''; ?></h2>
                            </div>

                            <div class="post--items post--items-3" data-ajax-content="outer">
                                <ul class="nav" data-ajax-content="inner">
                                    <?php
                                    $ii = 0;
                                    $layoutStyle = 'post--layout-1';

                                    foreach ($categoriaDestaque6 as $linha) :
                                        $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->post_created_at : $linha->post_updated_at;
                                        if ($ii != 0) {
                                            $layoutStyle = 'post--layout-3';
                                        }
                                    ?>
                                        <li>
                                            <div class="post--item <?= $layoutStyle ?>">
                                                <div class="post--img">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>

                                                    <div class="post--info">
                                                        <ul class="nav meta">
                                                            <li><a href="<?= base_url('colunista/' . $linha->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                            <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                        </ul>

                                                        <div class="title">
                                                            <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= word_limiter($linha->post_titulo, 13)  ?></a></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php
                                        $ii++;
                                    endforeach;
                                    ?>
                                </ul>

                                <!-- Preloader Start -->
                                <div class="preloader bg--color-0--b" data-preloader="1">
                                    <div class="preloader--inner"></div>
                                </div>
                                <!-- Preloader End -->
                            </div>
                        </div>

                        <div class="col-md-6 ptop--30 pbottom--30">
                            <div class="post--items-title" data-ajax="tab">
                                <h2 class="h4"><?= $categoriaDestaque7 ? $categoriaDestaque7[0]->categoria_nome : ''; ?></h2>
                            </div>

                            <div class="post--items post--items-3" data-ajax-content="outer">
                                <ul class="nav" data-ajax-content="inner">
                                    <?php
                                    $ii = 0;
                                    $layoutStyle = 'post--layout-1';

                                    foreach ($categoriaDestaque7 as $linha) :
                                        $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->post_created_at : $linha->post_updated_at;
                                        if ($ii != 0) {
                                            $layoutStyle = 'post--layout-3';
                                        }
                                    ?>
                                        <li>
                                            <div class="post--item <?= $layoutStyle ?>">
                                                <div class="post--img">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>

                                                    <div class="post--info">
                                                        <ul class="nav meta">
                                                            <li><a href="<?= base_url('colunista/' . $linha->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                            <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                        </ul>

                                                        <div class="title">
                                                            <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= word_limiter($linha->post_titulo, 13)  ?></a></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php
                                        $ii++;
                                    endforeach;
                                    ?>
                                </ul>

                                <!-- Preloader Start -->
                                <div class="preloader bg--color-0--b" data-preloader="1">
                                    <div class="preloader--inner"></div>
                                </div>
                                <!-- Preloader End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main--sidebar col-md-4 col-sm-5 ptop--30 pbottom--30" data-sticky-content="true">
                <div class="sticky-content-inner">
                    <?php if ($enquete != null) : ?>
                        <div class="widget">
                            <div class="widget--title" data-ajax="tab">
                                <h2 class="h4">Enquete</h2>
                            </div>
                            <div class="poll--widget" data-ajax-content="outer">
                                <ul class="nav" data-ajax-content="inner">
                                    <li class="title">
                                        <h3 class="h4"><?= $enquete[0]->enquete_texto ?></h3>
                                        <input type="hidden" id="enquete" value="enquete<?= $enquete[0]->enquete_id ?>"/>
                                    </li>

                                    <li class="options">
                                        <!-- <form> -->
                                            <?php foreach ($enquete as $linha11) : ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="radio" name="radio" class="alternativaEnquete" value="<?= $linha11->enquete_alternativa_id ?>">
                                                        <span><?= $linha11->enquete_alternativa_texto ?></span>
                                                    </label>
                                                    <!-- <p>65%<span style="width: 65%;"></span></p> -->
                                                </div>
                                            <?php endforeach ?>
                                            <button id="btnEnquete" class="btn btn-primary">Responder</button>
                                        <!-- </form> -->
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->include('portal/views/footer') ?>