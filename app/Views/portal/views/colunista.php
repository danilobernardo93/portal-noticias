<?= $this->include('portal/views/header') ?>
<div class="main-content--section pbottom--30">
    <div class="container">
        <div class="row">
            <!-- Main Content Start -->
            <div class="main--content col-md-8 col-sm-7" data-sticky-content="true">
                <div class="sticky-content-inner">
                    <!-- Post Author Info Start -->
                    <div class="post--author-info clearfix">
                        <div class="img">
                            <div class="vc--parent">
                                <div class="vc--child">
                                    <img src="<?= empty($colunista->user_foto) ? base_url('public/portal/img/usuarios/perfil.png') : base_url('public/portal/img/usuarios/' . $colunista->user_foto) ?> " />
                                    <p class="name"><?= $colunista->user_nome ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="info">
                            <h2 class="h4">Sobre</h2>

                            <div class="content">
                                <p><?= $colunista->user_texto ?></p>
                            </div>

                            <ul class="social nav">
                                <li><a href="<?= $colunista->facebook ?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?= $colunista->twitter ?>"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="<?= $colunista->instagram ?>"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="<?= $colunista->youtube ?>"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="post--items post--items-5 pd--30-0">
                        <ul class="nav">
                            <?php foreach ($posts as $linha) : ?>
                                <li>
                                    <div class="post--item post--title-larger">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12 col-xs-4 col-xxs-12">
                                                <div class="post--img">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>
                                                    <a href="<?= base_url('categoria/' . $linha->categoria_uri) ?>" class="cat"><?= $linha->categoria_nome ?></a>
                                                </div>
                                            </div>

                                            <div class="col-md-8 col-sm-12 col-xs-8 col-xxs-12">
                                                <div class="post--info">
                                                    <ul class="nav meta">
                                                        <?php $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->created_at : $linha->post_updated_at; ?>
                                                        <li><a href="javascript:void(0)"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                        <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                                    </ul>

                                                    <div class="title">
                                                        <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= $linha->post_titulo ?></a></h3>
                                                    </div>
                                                </div>

                                                <div class="post--content">
                                                    <p><?= $linha->post_resumo ?></p>
                                                </div>

                                                <div class="post--action">
                                                    <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>">Continuar lendo...</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                    <!-- Pagination Start -->
                    <div class="pagination--wrapper clearfix bdtop--1 bd--color-2 ptop--60 pbottom--30">
                        <p class="pagination-hint float--left">Pagina <?= $pageCurrent ?> de <?= $numPages == 0 ? $numPages + 1 : $numPages ?></p>
                        <?= paginacao($numPages, $pageCurrent, base_url('colunista/' . $colunista->user_uri . '/')) ?>
                    </div>
                    <!-- Pagination End -->
                </div>
            </div>
            <!-- Main Content End -->

            <!-- Main Sidebar Start -->
            <?= $this->include('portal/views/sidebar-main') ?>
        </div>
    </div>
</div>
<?= $this->include('portal/views/footer') ?>