<!-- Footer Section Start -->
<footer class="footer--section">
    <!-- Footer Widgets Start -->
    <div class="footer--widgets pd--30-0 bg--color-2">
        <div class="container">
            <div class="row AdjustRow">
                <div class="col-md-3 col-xs-6 col-xxs-12 ptop--30 pbottom--30">
                    <!-- Widget Start -->
                    <div class="widget">
                        <div class="widget--title">
                        </div>
                        <!-- About Widget Start -->
                        <div class="about--widget">
                            <div class="content">
                                <img src="<?= base_url('public/portal/img/logotipo-branco.png') ?>" width="200px" alt="Vale Finanças">
                            </div>
                        </div>
                        <!-- About Widget End -->
                    </div>
                    <!-- Widget End -->
                </div>

                <div class="col-md-3 col-xs-6 col-xxs-12 ptop--30 pbottom--30">
                    <!-- Widget Start -->
                    <div class="widget">
                        <div class="widget--title">
                            <h2 class="h4">Sobre</h2>

                            <i class="icon fa fa-exclamation"></i>
                        </div>

                        <!-- About Widget Start -->
                        <div class="about--widget">
                            <div class="content">
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium laborum et dolorum fuga.</p>
                            </div>

                            <div class="action">
                                <!-- <a href="#" class="btn-link">Read More<i class="fa flm fa-angle-double-right"></i></a> -->
                            </div>

                            <ul class="nav">
                                <li>
                                    <i class="fa fa-envelope-o"></i>
                                    <a href="mailto:contato@valefinancas.com.br">contato@valefinancas.com.br</a>
                                </li>
                            </ul>
                        </div>
                        <!-- About Widget End -->
                    </div>
                    <!-- Widget End -->
                </div>



                <div class="col-md-3 col-xs-6 col-xxs-12 ptop--30 pbottom--30">
                    <!-- Widget Start -->
                    <div class="widget">
                        <div class="widget--title">
                            <h2 class="h4">Links Importantes</h2>

                            <i class="icon fa fa-bullhorn"></i>
                        </div>

                        <!-- Links Widget Start -->
                        <div class="links--widget">
                            <ul class="nav">
                                <!-- <li><a href="#" class="fa-angle-right">RSS</a></li> -->
                                <li><a href="#" class="fa-angle-right">Contato</a></li>
                                <li><a href="#" class="fa-angle-right">Colunistas</a></li>
                                <li><a href="#" class="fa-angle-right">Rescentes</a></li>
                                <li><a href="#" class="fa-angle-right">Anunciar</a></li>
                                <li><a href="<?=base_url('termos-de-uso')?>" class="fa-angle-right">Termos de uso</a></li>
                                <li><a href="<?=base_url('politicas-de-privacidade')?>" class="fa-angle-right">Políticas de privacidade</a></li>
                            </ul>
                        </div>
                        <!-- Links Widget End -->
                    </div>
                    <!-- Widget End -->
                </div>

                <div class="col-md-3 col-xs-6 col-xxs-12 ptop--30 pbottom--30">
                    <!-- Widget Start -->
                    <div class="widget">
                        <div class="widget--title">
                            <h2 class="h4">Categorias</h2>

                            <i class="icon fa fa-expand"></i>
                        </div>

                        <!-- Links Widget Start -->
                        <div class="links--widget">
                            <ul class="nav">
                                <?php foreach ($menu['rodape'] as $linha) : ?>
                                    <?php if (count($linha['subMenu']) == 0) : ?>
                                        <li><a href="<?= base_url('categoria/' . $linha['itemMenu']->categoria_uri) ?>"><?= $linha['itemMenu']->categoria_nome ?></a></li>
                                    <?php endif; ?>

                                    <?php if (count($linha['subMenu']) > 0) : ?>
                                        <li class="">
                                            <a href="#">
                                                <?= $linha['itemMenu']->categoria_nome ?><i class="fa flm fa-angle-down"></i>
                                            </a>

                                            <ul>
                                                <?php foreach ($linha['subMenu'] as $subMenu) : ?>
                                                    <li>
                                                        <a href="<?= base_url('subcategoria/' . $subMenu->subcategoria_uri) ?>"><?= $subMenu->subcategoria_nome ?></a>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach ?>
                            </ul>
                        </div>
                        <!-- Links Widget End -->
                    </div>
                    <!-- Widget End -->
                </div>

            </div>
        </div>
    </div>
    <!-- Footer Widgets End -->

    <!-- Footer Copyright Start -->
    <div class="footer--copyright bg--color-3">
        <div class="social--bg bg--color-1"></div>

        <div class="container">
            <p class="text float--left">&copy; <?=date("Y")?> <a href="#">Vale Finanças.</a></p>

            <ul class="nav social float--right">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
            </ul>

            <!-- <ul class="nav links float--right">
                <li><a href="#">Home</a></li>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Support</a></li>
            </ul> -->
        </div>
    </div>
    <!-- Footer Copyright End -->
</footer>
<!-- Footer Section End -->
</div>
<!-- Wrapper End -->

<!-- Sticky Social Start -->
<div id="stickySocial" class="sticky--right" style="display: none;">
    <ul class="nav">
        <li>
            <a href="#">
                <i class="fa fa-facebook"></i>
                <span>Follow Us On Facebook</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-twitter"></i>
                <span>Follow Us On Twitter</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-google-plus"></i>
                <span>Follow Us On Google Plus</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-rss"></i>
                <span>Follow Us On RSS</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-vimeo"></i>
                <span>Follow Us On Vimeo</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-youtube-play"></i>
                <span>Follow Us On Youtube Play</span>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-linkedin"></i>
                <span>Follow Us On LinkedIn</span>
            </a>
        </li>
    </ul>
</div>
<!-- Sticky Social End -->

<!-- Back To Top Button Start -->
<div id="backToTop">
    <a href="#"><i class="fa fa-angle-double-up"></i></a>
</div>
<!-- Back To Top Button End -->

<!-- ==== jQuery Library ==== -->
<script src="<?= base_url('public/portal/js/jquery-3.2.1.min.js') ?>"></script>

<!-- ==== Bootstrap Framework ==== -->
<script src="<?= base_url('public/portal/js/bootstrap.min.js') ?>"></script>

<!-- ==== StickyJS Plugin ==== -->
<script src="<?= base_url('public/portal/js/jquery.sticky.min.js') ?>"></script>

<!-- ==== HoverIntent Plugin ==== -->
<script src="<?= base_url('public/portal/js/jquery.hoverIntent.min.js') ?>"></script>

<!-- ==== Marquee Plugin ==== -->
<script src="<?= base_url('public/portal/js/jquery.marquee.min.js') ?>"></script>


<!-- ==== Isotope Plugin ==== -->
<script src="<?= base_url('public/portal/js/isotope.min.js') ?>"></script>

<!-- ==== Resize Sensor Plugin ==== -->
<script src="<?= base_url('public/portal/js/resizesensor.min.js') ?>"></script>

<!-- ==== Sticky Sidebar Plugin ==== -->
<script src="<?= base_url('public/portal/js/theia-sticky-sidebar.min.js') ?>"></script>

<!-- ==== Zoom Plugin ==== -->
<script src="<?= base_url('public/portal/js/jquery.zoom.min.js') ?>"></script>

<!-- ==== Bar Rating Plugin ==== -->
<script src="<?= base_url('public/portal/js/jquery.barrating.min.js') ?>"></script>

<!-- ==== Countdown Plugin ==== -->
<script src="<?= base_url('public/portal/js/jquery.countdown.min.js') ?>"></script>

<!-- ==== RetinaJS Plugin ==== -->
<script src="<?= base_url('public/portal/js/retina.min.js') ?>"></script>

<!-- ==== Google Map API ==== -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK9f7sXWmqQ1E-ufRXV3VpXOn_ifKsDuc"></script> -->

<!-- ==== Main JavaScript ==== -->
<script src="<?= base_url('public/portal/js/main.js') ?>"></script>
<script>
    bloqueiaVotoEnquete();

    //verificar se existe a enquete no cache, se existir colocar o testo como respondido
    $("#btnEnquete").click(function() {
        $("#btnEnquete").text("Respondido");
        var enquete = $("#enquete").val();
        var alternativas = $(".alternativaEnquete");
        var link = $("meta[name=url_site]").attr("content");
        link = link + '/enquete'


        for (let index = 0; index < alternativas.length; index++) {
            if (alternativas[index].checked == true) {
                if (getCookie(enquete) == null) {
                    var valor = alternativas[index].defaultValue
                    $.ajax({
                        url: link,
                        type: "post",
                        data: {
                            'action': 'formEnquete',
                            'alternativa': valor
                        },
                        success: function(res) {
                            setCookie(enquete, true, 7)
                        }
                    })
                }
            }
        }
    });

    function bloqueiaVotoEnquete() {
        var enquete = $("#enquete").val();
        console.log(enquete)
        if (getCookie(enquete) != null) {
            $("#btnEnquete").text("Respondido");
            $("#btnEnquete").attr("id", "btnEnqueteRespondido");
            $("#btnEnqueteRespondido").css("background", "#1d1d1d");
            $("#btnEnqueteRespondido").css("border-color", "#1d1d1d");
        }
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return null;
    }
</script>

</body>

</html>