<!doctype html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-1YL61PTZT9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-1YL61PTZT9');
    </script>
    <script data-ad-client="ca-pub-9006983587168496" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <meta charset="utf-8">
    <meta name="image" content="<?= $seo['ogIMG'] ?>" />
    <meta name="title" content="<?= $seo['title'] ?>" />
    <meta name="urlPost" content="<?= base_url($_SERVER["REQUEST_URI"]) ?>" />

    <meta name="url_site" content="<?= base_url() ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= str_replace('"', "'", $seo['description']) ?>">
    <meta name="keywords" content="<?= $seo['keywords'] ?>">
    <meta property="og:image" content="<?= $seo['ogIMG'] ?>" />
    <meta property="og:title" content="<?= $seo['title'] ?>" />
    <meta property="og:description" content="<?= str_replace('"', "'", $seo['description']) ?>" />
    <meta property="og:url" content="<?= base_url($_SERVER["REQUEST_URI"]) ?>" />

    <!-- TWITTER -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@eulorenar7" />
    <meta name="twitter:creator" content="@eulorenar7" />
    <meta property="og:url" content="<?= base_url($_SERVER["REQUEST_URI"]) ?>" />
    <meta property="og:title" content="<?= $seo['title'] ?>" />
    <meta property="og:description" content="<?= str_replace('"', "'", $seo['description']) ?>" />
    <meta property="og:image" content="<?= $seo['ogIMG'] ?>" />
    <!-- TWITTER -->

    <title><?= $seo['title'] ?></title>
    <meta name="author" content="Danilo Bernardo danilo.bernardo1993@gmail.com">

    <!-- ==== Favicons ==== -->
    <link rel="icon" href="<?= base_url('public/portal/img/favicon.ico') ?>">
    <!-- ==== Google Font ==== -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700">

    <!-- ==== Font Awesome ==== -->
    <link rel="stylesheet" href="<?= base_url('public/portal/css/font-awesome.min.css') ?>">

    <!-- ==== Bootstrap Framework ==== -->
    <link rel="stylesheet" href="<?= base_url('public/portal/css/bootstrap.min.css') ?>">

    <!-- ==== Main Stylesheet ==== -->
    <link rel="stylesheet" href="<?= base_url('public/portal/css/style.min.css') ?>">

    <!-- ==== Responsive Stylesheet ==== -->
    <link rel="stylesheet" href="<?= base_url('public/portal/css/responsive-style.min.css') ?>">

    <!-- ==== Theme Color Stylesheet ==== -->
    <link rel="stylesheet" href="<?= base_url('public/portal/css/colors/theme-color-1.css') ?>" id="changeColorScheme">

    <!-- ==== HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries ==== -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">

</head>

<body>
    <div id="preloader">
        <div class="preloader bg--color-1--b" data-preloader="1">
            <div class="preloader--inner"></div>
        </div>
    </div>
    <!-- Preloader End -->

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- Header Section Start -->
        <header class="header--section header--style-1">
            <!-- Header Topbar Start -->
            <div class="header--topbar bg--color-2">
                <div class="container">
                    <div class="float--left float--xs-none text-xs-center">
                        <!-- Header Topbar Info Start -->
                        <ul class="header--topbar-info nav">
                            <!-- <li><i class="fa fm fa-map-marker"></i>New York</li> -->
                            <!-- <li><i class="fa fm fa-mixcloud"></i>21<sup>0</sup> C</li> -->
                            <li><i class="fa fm fa-calendar"></i><?= formataDta(date("d-m-Y"), "%d de %B de %Y") ?></li>
                        </ul>
                        <!-- Header Topbar Info End -->
                    </div>

                    <div class="float--right float--xs-none text-xs-center">
                        <ul class="header--topbar-social nav hidden-sm hidden-xxs">
                            <!-- <li><a href="#"><i class="fa fa-facebook"></i></a></li> -->
                            <li><a href="https://www.instagram.com/vale.financas/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <!-- <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
                            <!-- <li><a href="#"><i class="fa fa-youtube-play"></i></a></li> -->
                        </ul>
                    </div>
                </div>
            </div>

            <div class="header--mainbar">
                <div class="container">
                    <div class="header--logo float--left float--sm-none text-sm-center">
                        <h1 class="h1">
                            <a href="<?= base_url() ?>" class=" btn-link">
                                <img src="<?= base_url('public/portal/img/logotipo-preto-vermelho.png') ?>" width="200px" alt="Vale Finanças">
                                <span class="hidden">Vale Finanças</span>
                            </a>
                        </h1>
                    </div>

                    <div class="header--ad float--right float--sm-none hidden-xs">
                        <a href="<?= $propagandas['header01']['link'] ?>">
                            <img src="<?= $propagandas['header01']['image'] ?>" alt="<?= $propagandas['header01']['title'] ?>" style="margin-top: 18px;">
                        </a>
                    </div>
                </div>
            </div>

            <!-- Header Navbar Start -->
            <div class="header--navbar style--1 navbar bd--color-1 bg--color-1" data-trigger="sticky">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#headerNav" aria-expanded="false" aria-controls="headerNav">
                            <span class="sr-only">Menu</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div id="headerNav" class="navbar-collapse collapse float--left">
                        <ul class="header--menu-links nav navbar-nav" data-trigger="hoverIntent">
                            <?php foreach ($menu['topo'] as $linha) : ?>
                                <?php if (count($linha['subMenu']) == 0) : ?>
                                    <li class="nav-item">
                                    <li><a href="<?= base_url('categoria/' . $linha['itemMenu']->categoria_uri) ?>"><?= $linha['itemMenu']->categoria_nome ?></a></li>
                                    </li>
                                <?php endif; ?>

                                <?php if (count($linha['subMenu']) > 0) : ?>
                                    <li class="dropdown active">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $linha['itemMenu']->categoria_nome ?><i class="fa flm fa-angle-down"></i></a>

                                        <ul class="dropdown-menu">
                                            <?php foreach ($linha['subMenu'] as $subMenu) : ?>
                                                <li class="active">
                                                    <a href="<?= base_url('subcategoria/' . $subMenu->subcategoria_uri) ?>"><?= $subMenu->subcategoria_nome ?></a>
                                                </li>
                                            <?php endforeach ?>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach ?>

                        </ul>
                    </div>

                    <!-- <form action="#" class="header--search-form float--right">
                        <input type="search" name="search" placeholder="Search..." class="header--search-control form-control" required>

                        <button type="submit" class="header--search-btn btn"><i class="header--search-icon fa fa-search"></i></button>
                    </form> -->
                </div>
            </div>
        </header>