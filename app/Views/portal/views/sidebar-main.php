<div class="main--sidebar col-md-4 col-sm-5 ptop--30 pbottom--30" data-sticky-content="true">
    <div class="sticky-content-inner">
        <div class="widget">
            <div class="ad--widget">
                <a href="<?=$propagandas['sideBar01']['link'] ?>">
                    <img src="<?= $propagandas['sideBar01']['image'] ?>" alt="<?=$propagandas['sideBar01']['title'] ?>">
                </a>
            </div>
        </div>

        <div class="widget">
            <div class="widget--title">
                <h2 class="h4">Newsletter</h2>
                <i class="icon fa fa-envelope-open-o"></i>
            </div>

            <!-- Subscribe Widget Start -->
            <div class="subscribe--widget">
                <div class="content">
                    <p>Assine nosso boletim informativo para receber as últimas notícias, notícias populares e atualizações exclusivas.</p>
                </div>

                <form action="https://themelooks.us13.list-manage.com/subscribe/post?u=79f0b132ec25ee223bb41835f&id=f4e0e93d1d" method="post" name="mc-embedded-subscribe-form" target="_blank" data-form="mailchimpAjax">
                    <div class="input-group">
                        <input type="email" name="EMAIL" placeholder="E-mail address" class="form-control" autocomplete="off" required>

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-lg btn-default active"><i class="fa fa-paper-plane-o"></i></button>
                        </div>
                    </div>

                    <div class="status"></div>
                </form>
            </div>
            <!-- Subscribe Widget End -->
        </div>
        <!-- Widget End -->

        <!-- Widget Start -->
        <div class="widget">
            <div class="widget--title">
                <h2 class="h4">Mais Lidas</h2>
                <i class="icon fa fa-newspaper-o"></i>
            </div>

            <!-- List Widgets Start -->
            <div class="list--widget list--widget-1">

                <!-- Post Items Start -->
                <div class="post--items post--items-3" data-ajax-content="outer">
                    <ul class="nav" data-ajax-content="inner">
                        <?php foreach ($maisLidas as $linha) : ?>
                            <?php $data = $linha->post_updated_at == '0000-00-00 00:00:00' ? $linha->created_at : $linha->post_updated_at; ?>
                            <li>
                                <div class="post--item post--layout-3">
                                    <div class="post--img">
                                        <a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="thumb"><img src="<?= base_url('public/portal/img/postagens/thumb-' . $linha->post_img) ?>" alt=""></a>

                                        <div class="post--info">
                                            <ul class="nav meta">
                                                <li><a href="<?= base_url('colunista/' . $linha->user_uri) ?>"><?= explode(" ", $linha->user_nome)[0] ?></a></li>
                                                <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                            </ul>

                                            <div class="title" title="<?= $linha->post_titulo ?>">
                                                <h3 class="h4"><a href="<?= base_url($linha->categoria_uri.'/'.$linha->post_uri) ?>" class="btn-link"><?= word_limiter($linha->post_titulo, 9)  ?></a></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach ?>
                    </ul>

                    <div class="preloader bg--color-0--b" data-preloader="1">
                        <div class="preloader--inner"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget">
            <div class="widget--title">
                <h2 class="h4">Anuncio</h2>
                <i class="icon fa fa-bullhorn"></i>
            </div>

            <div class="ad--widget">
                <a href="<?=$propagandas['sideBar01']['link'] ?>">
                    <img src="<?= $propagandas['sideBar01']['image'] ?>" alt="<?=$propagandas['sideBar01']['title'] ?>">
                </a>
            </div>
        </div>
    </div>
</div>