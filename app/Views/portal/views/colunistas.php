<?= $this->include('portal/views/header') ?>

<div class="main-content--section pbottom--30">
    <div class="container">
        <!-- Main Content Start -->
        <div class="main--content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- Page Title Start -->
                    <div class="page--title pd--30-0 text-center">
                        <h2 class="h2">Nossos Contribuidores</h2>

                        <div class="content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="contributor--items ptop--30">
                <ul class="nav row AdjustRow">
                    <?php foreach ($colunistas as $linha) : ?>
                        <li class="col-md-3 col-xs-6 col-xxs-12 pbottom--30">
                            <div class="contributor--item style--1">
                                <div class="img">
                                <img src="<?= empty($linha->user_foto) ? base_url('public/portal/img/usuarios/perfil.png') : base_url('public/portal/img/usuarios/' . $linha->user_foto) ?> " />
                                </div>

                                <div class="info bg--color-1 bd--color-1">
                                    <div class="name">
                                        <h3 class="h4"><?=$linha->user_nome?></h3>
                                    </div>

                                    <div class="desc">
                                        <p><?=limitarTexto($linha->user_texto, 50)?></p>
                                    </div>

                                    <ul class="social nav">
                                        <li><a href="<?=$linha->facebook?>"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="<?=$linha->twitter?>"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="<?=$linha->instagram?>"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="<?=$linha->youtube?>"><i class="fa fa-youtube"></i></a></li>
                                    </ul>

                                    <div class="action">
                                        <a href="<?= base_url('colunista/' . $linha->user_uri) ?>" class="btn btn-default">Ver postagens</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?= $this->include('portal/views/footer') ?>