<?= $this->include('portal/views/header') ?>
<div class="main-content--section pbottom--30">
    <div class="container">
        <div class="row">
            <!-- Main Content Start -->
            <div class="main--content col-md-8" data-sticky-content="true">
                <div class="sticky-content-inner">
                    <!-- Post Item Start -->
                    <div class="post--item post--single post--title-largest pd--30-0">
                        <div class="post--img">
                            <a href="#" class="thumb"><img class="imgPostagem" src="<?= base_url('public/portal/img/postagens/' . $post->post_img) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>"></a>
                        </div>

                        <div class="post--cats">
                            <ul class="nav">
                                <li><span><i class="fa fa-tags"></i></span></li>
                                <?php $tags = explode(",", $post->keywords); ?>
                                <?php foreach ($tags as $tag) : ?>
                                    <!-- Colocar um linkn para pegar pelas tags as postagens -->
                                    <li><a href="javascript:void(0)"><?= trim($tag) ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        </div>

                        <div class="post--info">
                            <ul class="nav meta">
                                <?php $data = $post->post_updated_at == '0000-00-00 00:00:00' ? $post->created_at : $post->post_updated_at; ?>
                                <li><a href="<?= base_url('colunista/' . $post->user_uri) ?>"><?= explode(" ", $post->user_nome)[0] ?></a></li>
                                <li><a href="#"><?= formataDta($data, '%d/%m/%Y') ?></a></li>
                                <li><span><i class="fa fm fa-eye"></i><?= $post->post_views ?></span></li>
                                <li><a href="#"><i class="fa fm fa-comments-o"></i><?= $post->comentarios ?></a></li>
                            </ul>

                            <div class="title">
                                <h2 class="h4"><?= $post->post_titulo ?></h2>
                            </div>
                        </div>

                        <div class="post--content container-postagem"><?= str_replace("../../", base_url() . '/', $post->post_texto) ?></div>
                    </div>

                    <div class="post--social pbottom--30" style="display: none;">
                        <span class="title"><i class="fa fa-share-alt"></i></span>
                        <div class="social--widget style--4">
                            <ul class="nav">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Post Social End -->

                    <!-- Post Author Info Start -->
                    <div class="post--author-info clearfix">
                        <div class="img">
                            <div class="vc--parent">
                                <div class="vc--child">
                                    <a href="<?= base_url('colunista/' . $post->user_uri) ?>" class="btn-link">
                                        <img src="img/news-single-img/author.jpg" alt="">
                                        <p class="name"><?= explode(" ", $post->user_nome)[0] ?></p>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="info">
                            <h2 class="h4">Sobre o Autor</h2>

                            <div class="content">
                                <?= word_limiter($post->user_texto, 40) ?>
                            </div>

                            <ul class="social nav">
                                <li><a href="<?= $post->instagram ?>"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="<?= $post->facebook ?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?= $post->twitter ?>"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="<?= $post->youtube ?>"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="comment--list pd--30-0">
                        <div class="post--items-title">
                            <h2 class="h4"><?= $post->comentarios ?> Comentarios</h2>
                            <i class="icon fa fa-comments-o"></i>
                        </div>

                        <ul class="comment--items nav">
                            <?php foreach ($comentarios as $linha) : ?>
                                <li>
                                    <div class="comment--item clearfix">
                                        <div class="comment--img float--left">
                                            <img src="img/news-single-img/comment-avatar-01.jpg" alt="">
                                        </div>

                                        <div class="comment--info">
                                            <div class="comment--header clearfix">
                                                <p class="name"><?= $linha->comentario_nome ?></p>
                                                <p class="date"><?= formataDta($linha->created_at, '%d/%m/%Y') ?></p>
                                            </div>

                                            <div class="comment--content">
                                                <p><?= $linha->comentario_texto ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                    <div class="comment--form pd--30-0">
                        <div class="post--items-title">
                            <h2 class="h4">Deixe um comentário</h2>
                            <i class="icon fa fa-pencil-square-o"></i>
                        </div>

                        <div class="comment-respond">
                            <form method="post">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>
                                            <span>Comentario *</span>
                                            <textarea name="comentario" class="form-control" required></textarea>
                                        </label>
                                    </div>

                                    <div class="col-sm-6">
                                        <label>
                                            <span>Assunto</span>
                                            <input type="text" name="assunto" class="form-control">
                                        </label>
                                        <label>
                                            <span>Nome *</span>
                                            <input type="text" name="nome" class="form-control" required>
                                        </label>

                                        <label>
                                            <span>Email *</span>
                                            <input type="email" name="email" class="form-control" required>
                                        </label>
                                    </div>

                                    <div class="col-md-12">
                                        <input type="hidden" name="action" value="Comentar" />
                                        <button type="submit" class="btn btn-primary">Comentar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->include('portal/views/sidebar-main') ?>
        </div>
    </div>
</div>

<?= $this->include('portal/views/footer') ?>
<script>
    $(window).on("load", function() {
        // var imgs = document.getElementsByTagName("meta");
        // var imgSrcs = [];

        // for (var i = 0; i < imgs.length; i++) {
        //     imgSrcs.push(imgs[i]);
        // }

        var d = document.getElementsByClassName("container-postagem");
        var img = [];
        var alt = [];
        var link = [];
        var item = [];
        if (d.length > 0) {
            d = d[0].getElementsByTagName("a");
            d[0].parentNode.insertAdjacentHTML('afterBegin', '<div id="saibaMais">LEIA MAIS</div>');

            if (d.length >= 1) {
                for (let index = 0; index < d.length; index++) {
                    item[index] = d[index];
                    titulo = item[index].innerHTML;
                    link[index] = item[index].href;
                    $.ajax({
                        type: "get",
                        url: link[index],
                        data: {
                            s: index
                        },
                        cache: false,
                        success: function(data) {
                            // console.log(item[index])
                            parser = new DOMParser();
                            docNode = parser.parseFromString(data, "text/html");
                            
                            var metas = docNode.getElementsByTagName("meta");
                            var imgSrc = title = urlPost = '';

                            for (var i = 0; i < metas.length; i++) {
                                if(metas[i].name == "image"){
                                    imgSrc = metas[i].content;
                                }

                                // if(metas[i].name == "title"){
                                //     title = metas[i].content;
                                // }

                                // if(metas[i].name == "urlPost"){
                                //     urlPost = metas[i].content;
                                // }
                            }

                            if(imgSrc == ''){
                                comsole.log(docNode.getElementsByTagName("img"))
                            }

                            item[index].innerHTML = '<div class="row"><a href="' + link[index] + '" class="listaLeiaMais linkLeiaMais"><div class="col-md-3 col-lg-4 left "></div><div class="col-6 col-md-4 col-lg-3 left "><img src="' + imgSrc + '" title="' + titulo + '" alt="' + titulo + '"/></div><div class="col-6 col-md-5 col-lg-5 left" style="padding: 17px 0;">' + titulo + '</div></a></div>';
                        }
                    });
                }
            }
        }
    })
</script>