<!doctype html>
<html lang="en">

<head>
    <!-- <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex"> -->
    <meta charset="utf-8">
    <meta name="url_site" content="<?= base_url() ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?= str_replace('"',"'",$description) ?>">
    <meta name="keywords" content="<?= $keywords ?>">
    <meta property="og:image" content="<?= $ogIMG ?>" />
    <meta property="og:title" content="<?= $title ?>" />
    <meta property="og:description" content="<?= str_replace('"',"'",$description) ?>" />
    <meta property="og:url" content="<?= base_url($_SERVER["REQUEST_URI"]) ?>" />






    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@eulorenar7" />
    <meta name="twitter:creator" content="@eulorenar7" />
    <meta property="og:url" content="<?= base_url($_SERVER["REQUEST_URI"]) ?>" />
    <meta property="og:title" content="<?= $title ?>" />
    <meta property="og:description" content="<?= str_replace('"',"'",$description) ?>" />
    <meta property="og:image" content="<?= $ogIMG ?>" />



    <link rel="icon" href="<?= base_url('public/portal/img/favicon-32x32.ico') ?>">
    <title><?= $title ?></title>
    <meta name="author" content="Danilo Bernardo danilo.bernardo1993@gmail.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <link href="<?= base_url('public/assets/dist/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?= base_url('public/assets/css/main.min.css') ?>" rel="stylesheet" type="text/css">
    <?php foreach ($links['css'] as $link) : ?>
        <link href="<?= base_url($link) ?>" rel="stylesheet" type="text/css">
    <?php endforeach; ?>


    <!-- SCRIPTS R7 -->
    <!-- Begin comScore Tag -->
    <script>
        var _comscore = _comscore || [];
        _comscore.push({
            c1: "2",
            c2: "14194541"
        });
        (function() {
            var s = document.createElement("script"),
                el = document.getElementsByTagName("script")[0];
            s.async = true;
            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
            el.parentNode.insertBefore(s, el);
        })();
    </script>
    <noscript>
        <img src="https://sb.scorecardresearch.com/p?c1=2&c2=14194541&cv=2.0&cj=1" />
    </noscript>
    <!-- End comScore Tag -->


    <!-- SCRIPTS R7 -->
</head>

<body>
    <div id="barraTopoR7"></div>
    <nav class="navbar navbar-expand-lg navbar-light bg-4">
        <div class="container">
            <a class="navbar-brand" href="<?= base_url() ?>">LORENA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <?php foreach ($menu['topo'] as $linha) : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= base_url('categoria/' . $linha->categoria_uri) ?>"><?= $linha->categoria_nome ?></a>
                        </li>
                    <?php endforeach ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('colunistas') ?>">Colunistas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('sobre') ?>">Sobre</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('contato') ?>">Contato</a>
                    </li>
                    <li class="nav-item">
                        <i class="fa fa-search icone-busca"></i>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-busca">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-4">
                    <h5 class="modal-title">Faça sua busca</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="get">
                    <div class="modal-body">
                        <input type="text" name="busca" class="form-control" required placeholder="Digite sua busca">
                    </div>
                    <div class="modal-footer ">
                        <input type="submit" name="action" value="Buscar" class="btn btn-primary bg-4">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php

    if (isset($_GET['busca'])) {
        header("Location:" . base_url('busca/' . mb_url_title($_GET['busca'])));
        die();
    }
    ?>