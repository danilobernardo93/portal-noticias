<?= $this->include('header') ?>
<div class="containnerr">
    <div class="container" id="bannerGrande_R7_1">
        <div class="row ">
            <div class="col-12">

            </div>
        </div>
    </div>
</div>
<div class="container mt-5">
    <div class="row mt-5 grupo-3">
        <div class="col-md-12 center">
            <h1>Colunistas</h1>
        </div>
    </div>
    <div class="row mt-3 grupo-3">
        <?php foreach ($colunistas as $linha) : ?>
            <div class="col-md-6 col-lg-4 clearfix mb-3">
                <a href="<?= base_url('colunista/' . $linha->user_uri) ?>">
                    <div class="img-colunista">
                        <img src="<?= empty($linha->user_foto) ? base_url('public/portal/img/usuarios/perfil.png') : base_url('public/portal/img/usuarios/' . $linha->user_foto) ?> " />
                    </div>
                    <div class="info-colunista">
                        <label class="nomeDaColuna"><?= $linha->user_coluna ?></label>
                        <label class="nomeColunista1"><span>Por</span> <?= $linha->user_nome ?></label>
                        <?= limitarTexto($linha->user_texto, 60) ?>
                    </div>
                </a>
            </div>
        <?php endforeach ?>
    </div>

    <div class="row paginacao">
        <hr>
        <?= paginacao($numPages, $pageCurrent, base_url('colunistas/')) ?>
    </div>
</div>
<?= $this->include('footer') ?>