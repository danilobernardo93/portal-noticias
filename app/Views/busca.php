<?= $this->include('header') ?>
<div class="container mt-5">
    <div class="row  mt-5 ">
        <h3 class="center">Resultado da sua pesquisa "<?=$busca?>"</h3>
    </div>
    <div class="row mt-4 grupo-3">
        <?php $i=1; 
            foreach ($posts as $linha) :
            $class = $i % 3 == 0 ? ' ' : 'bd-r-1';
            ?>
            <div class="col-md-4 <?=$class?>">
                <div class="post-m mb-4 clearfix">
                    <img class="w-100" src="<?= base_url('public/portal/img/postagens/' . $linha->post_img) ?>" title="<?= $linha->post_titulo ?>" alt="<?= $linha->post_titulo ?>"/>
                    <div class="categoria bg-2"><?=  $linha->categoria_nome ?></div>
                    <h3 class="titulo-1-post"><a href="<?= base_url('post/' . $linha->post_uri) ?>"><?= $linha->post_titulo ?></a></h3>
                    <div class=" data"><?= formataDta($linha->post_created_at, '%d de %b de %Y') ?></div>
                    <div class="comentario "><?= $linha->comentarios ?> comentários</div>
                </div>
            </div>
        <?php $i++; endforeach ?>
    </div>

    <div class="row paginacao">
        <hr>
        <?= paginacao($numPages, $pageCurrent, base_url('busca/'.$busca.'/')) ?>
    </div>
</div>
<?= $this->include('footer') ?>