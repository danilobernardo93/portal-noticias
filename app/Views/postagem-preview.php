<?= $this->include('header') ?>
<div class="containnerr">
    <div class="container" id="bannerGrande_R7_1">
        <div class="row ">
            <div class="col-12">

            </div>
        </div>
    </div>
</div>
<div class="container mt-4">
    <div class="row">
        <div class="col-md-12 mb-2">
            <img class="w-100" src="<?= base_url('public/portal/img/postagens/' . $post->post_img) ?>" title="<?= $post->post_titulo ?>" alt="<?= $post->post_titulo ?>"/>
            <div class="categoria mt-2">
                <b>Categoria:</b> <?= listaCategoria($post->post_categoria, $categorias); ?>
            </div>
        </div>
        <div class="col-md-8 postagem">
            <h1><?= $post->post_titulo ?></h1>
            <div class="w-100 clearfix mb-3">
                <div class="autor float-left mr-5">
                    <b><?=$post->user_nome?> - </b>
                    <small>Publicado <?= formataDta($post->post_updated_at, '%d de %b de %Y às %H:%m') ?></small>
                </div>
                <div class="comentario float-right mr-5"><?= $post->comentarios ?> Comentarios</div>

            </div>
            <?= str_replace("../../", base_url() . '/', $post->post_texto) ?>


            <div class="form-comentario mt-5 mb-6">
                <h4>Deixe um comentário</h4>
                <?php if (isset($msg)) : ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?= $msg ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endif ?>
                <form method="POST">
                    <input class="form-control mb-3" type="text" name="nome" required placeholder="Nome" />
                    <input class="form-control mb-3" type="email" name="email" required placeholder="E-mail" />
                    <input class="form-control mb-3" type="text" name="assunto" required placeholder="Assunto" />
                    <textarea class="form-control mb-3" rows="5" name="comentario" required placeholder="Mensagem"></textarea>
                    <input type="submit" name="action" class="form-control btn rosa-3" value="Comentar">
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div id="bannerPequenoR7-1">
                <script data-sizes="[300, 250]" data-vendor="r7.com" data-pos="island" data-context="principal" src="https://sc.r7.com/r7/js/adPartner.min.js"></script>
            </div>
            <h3 class="mt-3 titulo-h3">Mais Lidas</h3>
            <div class="mais-lidas mb-3">
                <?php foreach ($maisLidas as $linha) : ?>
                    <div class="post">
                        <a href="<?= base_url('post/' . $linha->post_uri) ?>">
                            <div class="posicao bg-1" style="background-image: url(<?= base_url('public/portal/img/postagens/' . $linha->post_img) ?>);"></div>
                            <div class="titulo-post"><?= $linha->post_titulo ?></div>
                        </a>
                        <div class="data w-100 "><small><?= formataDta($linha->post_updated_at, '%d de %b de %Y') ?></small></div>
                    </div>
                <?php endforeach ?>
            </div>

            <h3 class="titulo-h3">Recentes</h3>
            <div class="rescentes">
                <?php foreach ($rescentes as $linha) : ?>
                    <div class="post mb-5">
                        <a href="<?= base_url('post/' . $linha->post_uri) ?>">
                            <img src="<?= base_url('public/portal/img/postagens/' . $linha->post_img) ?>" class="img-rescente w-100">
                            <div class="titulo-post mb-2"><?= $linha->post_titulo ?></div>
                        </a>
                        <div class="data"><?= formataDta($linha->post_updated_at, '%d de %b de %Y') ?></div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<?= $this->include('footer') ?>