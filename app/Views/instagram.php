<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <title>jquery.instagramFeed</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://www.sowecms.com/demos/statics/prism.js"></script>
    <link rel="stylesheet" type="text/css" href="https://www.sowecms.com/demos/statics/prism.css">

</head>

<body>

<div id="instagram-feed1" class="instagram_feed"></div>

    <script src="<?=base_url('public/assets/js/jquery.instagramFeed.min.js')?>"></script>
    <script>
        (function($) {
            $(window).on('load', function() {
                $.instagramFeed({
                    'username': 'instagram',
                    'container': "#instagram-feed1",
                    'display_profile': true,
                    'display_biography': true,
                    'display_gallery': true,
                    'callback': null,
                    'styling': true,
                    'items': 6,
                    'items_per_row': 3,
                    'margin': 1,
                    'lazy_load': true,
                    'on_error': console.error
                });

            });
        })(jQuery);
    </script>
</body>

</html>