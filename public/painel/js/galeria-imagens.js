function deletaIMG(id, img) {
    if (confirm("Deseja deletar a imagem ?")) {

        var link = $("meta[name=url_painel]").attr("content");
        link = link + '/galeria-imagens/deletar'
        var id = parseInt(id)

        $.ajax({
            url: link,
            data: { id: id, img: img },
            type: 'post',
            success: function(res) {
                alert('Deletado com sucesso')
                window.location = window.location
            }
        })
    }


    // $("#link-convite").click(function() {
    //     var textoCopiado = document.getElementById("linkAfiliado");
    //     textoCopiado.select();
    //     document.execCommand("Copy");
    //     alert("Link de afiliado copiado");
    // })
}

function copiarLinkIMG(urlIMG) {
    var textoCopiado = document.getElementById(urlIMG);
    console.log(urlIMG)
    textoCopiado.select();
    document.execCommand("Copy");
    alert("Link de afiliado copiado");
}