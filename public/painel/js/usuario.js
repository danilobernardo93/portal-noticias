function deletarUsuario(id, nome) {
    var link = $("meta[name=url_painel]").attr("content");

    link = link + '/usuario/deletar'

    if (confirm('Deseja deletar o usuário ' + nome + ' ?')) {
        $.ajax({
            url: link,
            type: 'post',
            data: {
                id: id,
                action: 'Deletar'
            },
            success: function(res) {
                console.log(res)
                window.location = window.location
            },
            error: function(a, b, c) {
                console.log(a)
                console.log(b)
                console.log(c)
            }
        })
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#previewIMG').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}

$("#img_user").change(function() {
    readURL(this);
});