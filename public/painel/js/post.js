tinymce.init({
    selector: '.editor-conteudo',
    plugins: 'advlist autolink lists link image media charmap print preview hr anchor pagebreak wordcount',
    toolbar_mode: 'floating',
});

$(document).ready(function() {
    if ($("#imagem").attr('src') != "") {
        $("#preview img").css('opacity', 1);

        $("#btn-recortar-imagem").css('opacity', 1);
        $('#imagem').Jcrop({
            addClass: 'jcrop-centered',
            onSelect: show_preview,
            onChange: mostrar_coordenadas,
            aspectRatio: 1.73076923 / 1,
            setSelect: [1080, 624, 0, 0]

        });
    } else {
        $("#preview img").css('opacity', 0);
        $("#btn-recortar-imagem").css('opacity', 0);
    }
})

$(".editor-conteudo").css('height', 460)

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $(".img-recorte").attr('src', e.target.result);
            $(".jcrop-holder img").attr('src', e.target.result);
            $('#imagem').Jcrop({
                addClass: 'jcrop-centered',
                onSelect: show_preview,
                onChange: mostrar_coordenadas,
                aspectRatio: 1.73076923 / 1,
                setSelect: [1080, 624, 0, 0]

            });
        }

        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}

var html_padrao = '<div class="modal-body p-4" id="result"><img src="" alt="Imagem upload " id="imagem" class="img-recorte" ></div>';

$("#img_post").change(function() {
    $("#result").html(html_padrao)
    readURL(this);
    $("#preview img").css('opacity', 1);
    $("#btn-recortar-imagem").css('opacity', 1);
});

function deletarPost(id, titulo) {
    var link = $("meta[name=url_painel]").attr("content");
    link = link + '/post/deletar'
    var id = parseInt(id)

    if (confirm("Deseja deletar o post ? \n\n " + titulo)) {
        $.ajax({
            url: link,
            type: 'post',
            data: { id: id },
            success: function(res) {
                window.location = window.location
            }
        })
    }

}

// RECORTAR IMAGEM
function mostrar_coordenadas(coord) {
    $('#input_x1').val(coord.x);
    $('#input_y1').val(coord.y);
    $('#input_x2').val(coord.x2);
    $('#input_y2').val(coord.y2);
    $('#input_w').val(coord.w);
    $('#input_h').val(coord.h);
}

function show_preview(coords) {
    rx = 540 / coords.w; //tamanho da preview (css) da imagem se alterar lá tem que alterar aqui e virse-versa
    ry = 315 / coords.h;

    $('#preview img').css({
        width: Math.round(rx * $("#imagem").width()) + 'px',
        height: Math.round(ry * $("#imagem").height()) + 'px',
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px',
    });

}
// RECORTAR IMAGEM

// Parte de SEO
$("#keywords").keyup(function(e) {
    var list = this.value.split(',');
    var items = '';

    for (let index = 0; index < list.length; index++) {
        var palavra = list[index].trim();
        if (palavra != "") {
            items += '<span class="itemKeyword" data-value="' + palavra + '">' + palavra + '</span>';
        }
        $("#listKeywords").html(items);
    }
});

$("#verificarDescripition").click(() => {
    $("#listKeywordInDescription").html('<p><b>Palavras chaves usadas no descrição da página</p>');
    var keyWords = $(".itemKeyword");

    for (let index = 0; index < keyWords.length; index++) {
        if ($("#description").val().indexOf(keyWords[index].dataset.value) > -1) {
            $("#listKeywordInDescription").append("<span class='itemKeyword text-green'>" + keyWords[index].dataset.value + "</span>");
        } else {
            $("#listKeywordInDescription").append("<span class='itemKeyword text-red'>" + keyWords[index].dataset.value + "</span>");
        }
    }
})

$("#description").keyup(function(e) {
    var description = $("#description").val().split('');

    // INICIO Quantidade de cacacteres
    var count = 0;
    for (let index = 0; index < description.length; index++) {
        if (description[index].trim() != "") {
            count++;
        }
    }

    if (description.length < 150) {
        $("#numberCaracters").html("<b>" + description.length + " caracteres. O ideal é 160 caractéres</b>")
        $("#numberCaracters").addClass("box-red");
    } else {
        $("#numberCaracters").html("<b>" + description.length + " caracteres. O ideal é 160 caractéres</b>");
        $("#numberCaracters").removeClass("box-red");
    }
    //FIM Quantidade de cacacteres
});
// Parte de SEO