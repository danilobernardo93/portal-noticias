function atualizarCategoria(id) {
    var uri = $("#uri-categoria-" + id).val();
    var link = $("meta[name=url_painel]").attr("content");
    var nome = $("#nome-categoria-" + id).val();
    var menu = $("#menu-categoria-" + id).prop("checked") ? 1 : 0;
    var rodape = $("#rodape-categoria-" + id).prop("checked") ? 1 : 0;
    link = link + '/categoria'

    $.ajax({
        url: link,
        type: 'post',
        data: {
            menu: menu,
            rodape: rodape,
            categoria_id: id,
            categoria_uri: uri,
            categoria_nome: nome,
            action: 'Atualizar'
        },
        success: function(res) {
            $("#nome-categoria-" + id).css('border-color', 'green');
            $("#uri-categoria-" + id).css('border-color', 'green');
            setTimeout(function() {
                $("#nome-categoria-" + id).css('border-color', '#ced4da');
                $("#uri-categoria-" + id).css('border-color', '#ced4da');
            }, 2000)
        }
    })
}

function deletarCategoria(id) {
    var link = $("meta[name=url_painel]").attr("content");

    link = link + '/categoria/deletar'

    if (confirm('Deseja deletar a categoria ?')) {
        $.ajax({
            url: link,
            type: 'post',
            data: {
                id: id,
                action: 'Deletar'
            },
            success: function(res) {
                console.log(res)
                window.location = window.location
            },
            error: function(a, b, c) {
                console.log(a)
                console.log(b)
                console.log(c)
            }
        })
    }
}