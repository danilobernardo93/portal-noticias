function deletaIMG(id, img) {
    if (confirm("Deseja deletar a imagem ?")) {

        var link = $("meta[name=url_painel]").attr("content");
        link = link + '/revista/deletar'
        var id = parseInt(id)

        $.ajax({
            url: link,
            data: { id: id, img: img },
            type: 'post',
            success: function(res) {
                alert('Deletado com sucesso')
                window.location = window.location
            }
        })
    }
}